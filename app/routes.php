<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/hi',function() {
	Mail::send([],[],function($message){
		$message->to('giarsyani.nuli@gmail.com','Nuli Giarsyani')->subject('New Account');
	});
});
// Route::get('/hi','Program@getPrint');
Route::get('/','HalamanDepan@index');

Route::get('register','HalamanDepan@register');
Route::get('activate/{code?}','HalamanDepan@activate');
Route::post('register','HalamanDepan@postRegister');

Route::get('auth','HalamanDepan@auth');
Route::post('auth','HalamanDepan@postAuth');

Route::group(array('before'=>'panel_kta'),function(){
	Route::group(array('prefix'=>'panel'),function(){
		Route::controller('home','HalamanDepan');
		Route::controller('profile','ProfileKPA');

		Route::group(array('before'=>'complete_profile'),function(){
			Route::controller('program','Program');
		});
		// Route::controller('kegiatan','Kegiatan');
		// Route::controller('kegiatan','Kegiatan');

		// Route::get('program/create','HalamanDepan@panelCreateProgram');
		// Route::post('program','HalamanDepan@panelPostCreateProgram');

		Route::get('program/{id?}/{kegiatan?}','HalamanDepan@panelCreateKegiatan');
		Route::post('program/{id?}/{kegiatan?}','HalamanDepan@panelPostCreateKegiatan');
		
		Route::get('logout','HalamanDepan@logout');
	});
});


Route::get('404','HalamanDepan@not');


Route::controller('login','HomeController');
Route::group(array('before'=>'auth_admin'),function(){

	Route::group(array('prefix' => 'admin'), function(){

		Route::get('/',function(){
			return Redirect::to('/admin/home');
		});
		// Route::get('/home','Dashboard@index');
		// Route::post('/home','Dashboard@postIndex');
		Route::controller('/home','Dashboard');
		Route::controller('/profile','Profile');
		//ACL
		Route::group(array('before' => 'acl'), function(){

			Route::controller('/program','Programs');
			Route::controller('/pengaduan','Pengaduan');
			Route::controller('/tahapan','Tahapan');
			Route::controller('/kegiatan','Kegiatans');
			Route::controller('/kpa','KpaUsers');
  
			Route::controller('/clients','Clients');
			Route::controller('/about','About');
 
			Route::controller('/statistics','Statistics');
			Route::controller('/report','Report');
			Route::controller('/messages','Messages');
			
		});
		Route::group(array('before' => 'acl_admin'), function(){

			Route::controller('/users','Users');
			Route::controller('/deleted-staff','DeletedUsers');
			Route::controller('/group','Groups');
			Route::controller('/permission','Permissions');

		});
	});
	Route::get('/logout','Dashboard@logout');
	
});
// App::error(function($exception, $code) { 
// 	if(Request::is('admin/*')){
// 		switch ($code) {
// 			case 404:
// 				$errors = 'Page not found';
// 				$note   = 'We could not find the page you were looking for';
// 				break;
// 			default:
// 				$errors = 'Something went wrong';
// 				$note   = 'We will work on fixing that right away';
// 				break;
// 		}
// 		View::share('title',$code);
// 		View::share('path',$errors);
// 		$data['code']  = $code;
// 		$data['error'] = $errors;
// 		$data['note']  = $note;
// 		return View::make('backend.errors',$data);
// 	}
// });

// App::error(function($exception, $code) { 
// 	if(!Request::is('admin/*')){		
// 		return 'errors';
// 	}
// });