<?php

class ItemKegiatanModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'item_kegiatan';

	public function kegiatan()
	{
		return $this->belongsTo('KegiatanModel','id_kegiatan');
	}

	public function fisik()
	{
		return $this->belongsTo('FisikKegiatanModel','id_kegiatan','id_item_kegiatan');
	}

	public function spj()
	{
		return $this->belongsTo('AnggaranSPJModel','id_kegiatan','id_item_kegiatan');
	}

	public function sppd()
	{
		return $this->belongsTo('AnggaranSP2DModel','id_kegiatan','id_item_kegiatan');
	}

	public function tahap()
	{
		return $this->belongsTo('DetailTahapModel','id_detail_tahap');
	}

	public function user()
	{
		return $this->belongsTo('KPAModel','id_user');
	}
}
