<?php

class PengaduanModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'complains';

	public function program()
	{
		return $this->belongsTo('ProgramModel','program_id');
	}

	public function kegiatan()
	{
		return $this->belongsTo('KegiatanModel','kegiatan_id');
	}

	public function userkpa()
	{
		return $this->belongsTo('KPAModel','skpd_id');
	}

}
