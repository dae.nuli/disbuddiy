<?php

class KegiatanModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'kegiatan';

	public function program()
	{
		return $this->belongsTo('ProgramModel','id_program');
	}

	public function userkegiatan()
	{
		return $this->belongsTo('UserKegiatanModel','id','id_kegiatan');
	}
}
