<?php

class UserKegiatanModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'user_kegiatan';

	public function userkpa()
	{
		return $this->belongsTo('KPAModel','id_user');
	}

	public function program()
	{
		return $this->belongsTo('ProgramModel','id_program');
	}

	public function kegiatan()
	{
		return $this->belongsTo('KegiatanModel','id_kegiatan');
	}

	// public function userkegiatan()
	// {
	// 	return $this->belongsTo('UserKegiatanModel','id_kegiatan');
	// }
}
