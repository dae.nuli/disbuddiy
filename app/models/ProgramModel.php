<?php

class ProgramModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'program';

	public function userkpa()
	{
		return $this->belongsTo('KPAModel','id_user_kpa');
	}
}
