<?php

class UserProgramModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'user_program';

	public function userkpa()
	{
		return $this->belongsTo('KPAModel','id_user');
	}

	public function program()
	{
		return $this->belongsTo('ProgramModel','id_program');
	}
}
