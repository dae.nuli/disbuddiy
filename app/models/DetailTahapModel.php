<?php

class DetailTahapModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'detail_tahap';

	public function tahap()
	{
		return $this->belongsTo('TahapModel','id_tahap');
	}

	public function item($idKegiatan,$idTahap)
	{
		$ikm = ItemKegiatanModel::where('id_kegiatan',$idKegiatan)->where('id_detail_tahap',$idTahap)->orderBy('id','desc')->get();
		if(count($ikm)>0){
			foreach ($ikm as $key => $val) {
				$data[]['name']            = '<li>'.$val->name.'</li>';
				$data[]['vol']             = '<li>'.$val->vol.'</li>';
				$data[]['satuan']          = '<li>'.$val->satuan.'</li>';
				$data[]['fisik_rencana']   = '<li>'.$val->fisik_rencana.'</li>';
				$data[]['fisik_realisasi'] = '<li>'.$val->fisik_realisasi.'</li>';
				$data[]['sppd_rencana']    = '<li>'.$val->sppd_rencana.'</li>';
				$data[]['sppd_realisasi']  = '<li>'.$val->sppd_realisasi.'</li>';
			}
			return $data;
		}
	}
}
