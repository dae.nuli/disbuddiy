<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProsentaseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('prosentase', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_program');
            $table->integer('id_kegiatan');
            $table->integer('fisik_value');
            $table->integer('sppd_value');
            $table->integer('spj_value');
		    $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
