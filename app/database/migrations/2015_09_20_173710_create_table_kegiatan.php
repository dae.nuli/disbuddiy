<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKegiatan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kegiatan', function($table)
		{
		    $table->increments('id');
		    $table->integer('id_program');
		    $table->string('code_kegiatan', 100);
		    $table->string('name');
		    $table->string('pagu_kegiatan', 100);
		    $table->string('description');
		    $table->integer('created_by');
		    $table->integer('updated_by');
		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
