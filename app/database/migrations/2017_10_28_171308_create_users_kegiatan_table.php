<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersKegiatanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('user_kegiatan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_program');
            $table->integer('id_kegiatan');
            $table->integer('id_user');
            $table->string('pagu_kegiatan')->nullable();
            $table->string('pagu_kegiatan_perubahan')->nullable();
            $table->text('keterangan')->nullable();
		    $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
