<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpaUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('kpa_users', function ($table) {
            $table->increments('id');
            $table->string('code_kpa');
            $table->string('instansi_name');
            $table->string('email');
            $table->string('email_staff');
            $table->string('username');
            $table->string('phone');
            $table->string('address');
            $table->string('bidang');
            $table->string('personil_name');
            $table->string('password');
            $table->enum('is_active', array('0', '1'))->default('0');
            $table->enum('is_complete', array('0', '1', '2'))->default('0');
            $table->string('activation');
		    $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
