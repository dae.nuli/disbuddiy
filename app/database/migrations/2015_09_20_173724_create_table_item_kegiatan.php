<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableItemKegiatan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_kegiatan', function($table)
		{
		    $table->increments('id');
		    $table->integer('id_user');
		    $table->integer('id_kegiatan');
		    $table->integer('id_detail_tahap');
		    $table->string('name')->nullable();
		    $table->string('vol',100)->nullable();
		    $table->string('satuan',50)->nullable();
		    $table->string('fisik_rencana')->nullable();
		    $table->string('fisik_realisasi')->nullable();
		    $table->string('anggaran_rencana',50)->nullable();
		    $table->string('anggaran_sppd',50)->nullable();
		    $table->string('anggaran_spj',50)->nullable();
		    $table->string('keterangan')->nullable();
		    $table->string('photo')->nullable();
		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
