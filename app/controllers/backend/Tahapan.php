<?php
class Tahapan extends BaseController {
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Tahapan');
		View::share('path','Index');
		$year = (Session::get('years'))?Session::get('years'):date('Y');
		$qr   = DetailTahapModel::orderBy('id_tahap','asc')->orderBy('month','asc');
		if(!empty($year)){
			$qr = $qr->where('year',$year);
		}
		$firstYear         = DetailTahapModel::orderBy('year','asc')->take(1)->first();
		$data['year']      = $year;
		$data['yearRange'] = range($firstYear->year,date('Y')+2);
		$data['tahapan']   = $qr->get();
		return View::make('backend.tahap.index',$data);
	}

	public function postYear()
	{
		$year = Input::get('year');
		if($year){
			Session::put('years',$year);
		}
		return Redirect::to('admin/tahapan');
	}

	public function getCreate()
	{
		View::share('path','Create');
		View::share('title','Tahapan');
		$data['yearRange'] = range(date('Y'),date('Y')+2);
		$data['bulan']     = range(1,12);
		$data['tahapan1']   = TahapModel::find(1);
		$data['tahapan2']   = TahapModel::find(2);
		$data['tahapan3']   = TahapModel::find(3);
		return View::make('backend.tahap.create',$data);
	}

	public function postIndex()
	{
		// echo "<pre>";
		// print_r(Input::all());
		// echo "</pre>";
		// return;
		$rules = array(
			'tahapan1' => 'required',
			'tahapan2' => 'required',
			'tahapan3' => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/tahapan/create')->withErrors($valid)->withInput();
		}else{
			$tahapan1 = Input::get('tahapan1');
			$tahapan2 = Input::get('tahapan2');
			$tahapan3 = Input::get('tahapan3');

			$bulan1   = Input::get('bulan1');
			$bulan2   = Input::get('bulan2');
			$bulan3   = Input::get('bulan3');

			$tahun   = Input::get('tahun');

			$dtm     = DetailTahapModel::where('year',$tahun)->count();
			if(!empty($dtm)){
				return Redirect::to('admin/tahapan/create')->with('tahapan_alert','Data <b><u>tahun '.$tahun.'</u></b> Sudah ada');
			}else{
				if(!empty($tahapan1)){
					foreach ($bulan1 as $bul1) {
						$cm           = new DetailTahapModel;
						$cm->id_tahap = $tahapan1;
						$cm->month    = $bul1;
						$cm->year     = $tahun;
						$cm->save();
					}
				}
				if(!empty($tahapan2) && count($bulan2)>0){
					foreach ($bulan2 as $bul2) {
						$cm           = new DetailTahapModel;
						$cm->id_tahap = $tahapan2;
						$cm->month    = $bul2;
						$cm->year     = $tahun;
						$cm->save();
					}
				}
				if(!empty($tahapan3) && count($bulan3)>0){
					foreach ($bulan3 as $bul3) {
						$cm           = new DetailTahapModel;
						$cm->id_tahap = $tahapan3;
						$cm->month    = $bul3;
						$cm->year     = $tahun;
						$cm->save();
					}
				}
				return Redirect::to('admin/tahapan')->with('tahapan','Data telah ditambah');
			}
		}
	}
	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','Tahapan');
		$year = (Session::get('years'))?Session::get('years'):date('Y');
		$data['tahap']   = TahapModel::orderBy('id','asc')->get();
		$data['year']    = $year;
		$data['bulan']   = range(1,12);
		$data['tahapan'] = DetailTahapModel::find($id);
		return View::make('backend.tahap.edit',$data);
	}
	public function postEdit($id)
	{
		$rules = array(
			'tahapan' => 'required',
			'bulan'   => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/tahapan/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$tahapan = Input::get('tahapan');
			$bulan   = Input::get('bulan');
			
			$cm           = DetailTahapModel::find($id);
			$cm->id_tahap = $tahapan;
			$cm->month    = $bulan;
			$cm->save();
			return Redirect::to('admin/tahapan')->with('tahapan','Data telah diperbarui');
		}
	}
	public function getDelete($id)
	{
		$years = date('Y');
		$pm    = DetailTahapModel::find($id);
		if($pm->year <= $years){
			return Redirect::to('admin/tahapan')->with('tahapan_alert','Data tahun lalu sampai sekarang tidak boleh dihapus');
		}else{
			$pm->delete();
			$dtm = DetailTahapModel::where('year',$pm->year)->count();
			if(!empty($dtm)){
				return Redirect::to('admin/tahapan')->with('tahapan','Data telah dihapus');
			}else{
				Session::forget('years');
				return Redirect::to('admin/tahapan')->with('tahapan','Data telah dihapus');
			}
		}
	}
}