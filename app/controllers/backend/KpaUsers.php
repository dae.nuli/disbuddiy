<?php

class KpaUsers extends BaseController {
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','KPA');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');

		$qr	= KPAModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('instansi_name','LIKE',"%$cari%")->orWhere('code_kpa','LIKE',"%$cari%");
		}
		$qr = $qr->paginate($this->limit);
		
		$data['kpa'] = $qr;
		return View::make('backend.kpa.index',$data);
	}
	public function getCreate()
	{
		View::share('path','KPA');
		View::share('title','Create');
		return View::make('backend.kpa.create');
	}

	public function postIndex()
	{
		$rules = array(
			'kode_pa_kpa'   => 'required|unique:kpa_users,code_kpa',
			'nama_instansi' => 'required',
			'email'         => 'required|email|unique:kpa_users,email'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/kpa/create')->withErrors($valid)->withInput();
		}else{
// Revisi
			$email  = Input::get('email');
			$user   = explode('@', $email);
			$cm                = new KPAModel;
			$cm->instansi_name = Input::get('nama_instansi');
			$cm->email         = $email;
			$cm->code_kpa      = Input::get('kode_pa_kpa');
			$cm->username      = $user[0];
			$cm->phone         = '';
			$cm->bidang        = '';
			$cm->personil_name = '';
			$cm->address       = '';
			$cm->save();

			return Redirect::to('admin/kpa')->with('kpa','Data KPA telah ditambahkan');

// Cara yang pertama
			// $email  = Input::get('email');
			// $user   = explode('@', $email);
			// $random = Str::random(7);
			// $pass   = sha1($random.'sha1');
			// $cm                = new KPAModel;
			// $cm->instansi_name = Input::get('nama_instansi');
			// $cm->email         = $email;
			// $cm->code_kpa      = Input::get('kode_pa_kpa');
			// $cm->username      = $user[0];
			// $cm->password      = $pass;
			// $cm->phone         = '';
			// $cm->bidang        = '';
			// $cm->personil_name = '';
			// $cm->address       = '';
			// $cm->save();

			// Mail::send('backend.login.message',$data = array('username' => $user[0], 'password' => $random),function($message) use ($cm){
			// 	$message->to($cm->email,$cm->instansi_name)->subject('New Account');
			// });
			// return Redirect::to('admin/kpa')->with('kpa','Data KPA telah ditambahkan');
		}
	}

	public function getSend($id)
	{
		if(!empty($id)){
			$km = KPAModel::find($id);
			if(!empty($km)){
				$random = Str::random(7);
				$pass   = sha1('111111sha1');
				// $pass   = sha1($random.'sha1');

				$km->password  = $pass;
				$km->is_active = 1;
				$km->is_complete   = 2;
				$km->save();

				// Mail::send('backend.login.message',$data = array('username' => $km->username, 'password' => $random),function($message) use ($km){
				// 	$message->to($km->email,$km->instansi_name)->subject('New Account');
				// });
				return Redirect::to('admin/kpa')->with('kpa','Data KPA telah diaktifkan dan dikirim');
			}
		}
	}

	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','KPA');

		$data['user'] = KPAModel::find($id);
		return View::make('backend.kpa.edit',$data);
	}

	public function getReset($id)
	{
		if(!empty($id)){
			$km = KPAModel::find($id);
			if(!empty($km)){
				$user   = explode('@', $km->email);
				$random = Str::random(7);
				$pass   = sha1($random.'sha1');
				
				$km->password      = $pass;
				$km->save();

				Mail::send('backend.login.message_reset',$data = array('username' => $user[0], 'password' => $random),function($message) use ($km){
					$message->to($km->email,$km->instansi_name)->subject('Reset Password');
				});
				return Redirect::to('admin/kpa')->with('kpa','Data KPA telah direset');
			}
		}
	}

	public function postEdit($id)
	{
		$rules = array(
			'kode_pa_kpa'   => 'required|unique:kpa_users,code_kpa,'.$id,
			'email'         => 'required|email|unique:kpa_users,email,'.$id,
			'email_staff'   => 'email|unique:kpa_users,email_staff,'.$id,
			'nama_instansi' => 'required',
			'telefon' => 'numeric'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/kpa/edit/'.$id)->withErrors($valid);
		}else{
			$email  = Input::get('email');
			$passwd = Input::get('password');
			$user   = explode('@', $email);
			$pass   = sha1($passwd.'sha1');

			$cm                = KPAModel::find($id);
			$cm->instansi_name = Input::get('nama_instansi');
			$cm->email         = $email;
			$cm->code_kpa      = Input::get('kode_pa_kpa');
			$cm->username      = $user[0];
			if(!empty($passwd)){
				$cm->password      = $pass;
			}
			$cm->phone         = (Input::get('telefon')?Input::get('telefon'):'');
			$cm->email_staff   = (Input::get('email_staff')?Input::get('email_staff'):'');
			$cm->personil_name = (Input::get('nama_penanggung_jawab')?Input::get('nama_penanggung_jawab'):'');
			$cm->address       = (Input::get('alamat')?Input::get('alamat'):'');
			$cm->save();
			return Redirect::to('admin/kpa')->with('kpa','Data kpa telah diperbarui'); 
		}
	}

	public function getDelete($id)
	{
		$km = KPAModel::find($id);
		if(!empty($km)){
			$km->delete();
			return Redirect::to('admin/kpa')->with('kpa','Data kpa telah dihapus');
		}
	}

	public function getModalDetailKpa($id)
	{
		View::share('title','KPA');
		View::share('path','Detail');

		$data['kpa'] = KPAModel::find($id);
		return View::make('backend.kpa.modal_detail_kpa',$data);
	}

	public function getNonactive($id)
	{
		if(!empty($id)){
			$km = KPAModel::find($id);
			if(count($km)>0){
				$km->is_active = 0;
				$km->save();
			}
		}
		return Redirect::to('admin/kpa')->with('kpa','KPA telah di non aktifkan');
	}

	public function getActive($id)
	{
		if(!empty($id)){
			$km = KPAModel::find($id);
			if(count($km)>0){
				$km->is_active = 1;
				$km->save();
			}
		}
		return Redirect::to('admin/kpa')->with('kpa','KPA telah diaktifkan');
	}

}