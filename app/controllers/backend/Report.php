<?php

class Report extends BaseController {
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Report');
		View::share('path','Index');
		$data['limit']  = $this->limit;
		$OM = OrdersModel::where('status',1)->orderBy('created_at','asc');
		if(Session::has('R_from_date') && Session::has('R_to_date')){
			$OM = $OM->whereBetween('created_at',array(Session::get('R_from_date').' 00:00:00',Session::get('R_to_date').' 23:59:59'));
		}
		$data['report']     = $OM->paginate($this->limit);
		$data['grandTotal'] = Helper::ReportGrandTotal(Session::get('R_from_date'),Session::get('R_to_date'));
		return View::make('backend.report.index',$data);
	}
	public function getFilter()
	{
		$R_date_filter = Input::get('R_date_filter');
		if(!empty($R_date_filter)){
			$dates       = explode('/', $R_date_filter);
			$from        = date('Y-m-d',strtotime($dates[0]));
			$to          = date('Y-m-d',strtotime($dates[1]));
			$date1       = abs(strtotime($from));
			$date2       = abs(strtotime($to));
			
			if($date1 != '25200' && $date2 != '25200'){
				Session::put('R_from_date',$from);
				Session::put('R_to_date',$to);
				Session::put('R_date_filter',$R_date_filter);
			}else{
				Session::forget('R_from_date');
				Session::forget('R_to_date');
				Session::forget('R_date_filter');
			}
		}else{
			Session::forget('R_from_date');
			Session::forget('R_to_date');
			Session::forget('R_date_filter');
		}
		return Redirect::to('admin/report');
	}
}