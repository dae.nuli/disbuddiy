<?php

class Pengaduan extends BaseController {
	public $limit = 10;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function getIndex()
	{
		View::share('title','Pengaduan Masyarakat');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');

		$qr	= PengaduanModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('name','LIKE',"%$cari%");
		}
		$qr = $qr->paginate($this->limit);
		
		$data['index'] = $qr;
		return View::make('backend.pengaduan.index',$data);
	}

	public function getDetail($id)
	{
		View::share('title','Pengaduan Masyarakat');
		View::share('path','Detail');
		$data['detail'] = PengaduanModel::find($id);
		return View::make('backend.pengaduan.detail',$data);
	}

	public function getDelete($id)
	{
		$cm = PengaduanModel::find($id);
		$cm->delete();
		return Redirect::to('admin/pengaduan')->with('pengaduan','Data has been deleted');
	}
}
