<?php

class About extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function getIndex()
	{
		// return 33;
		View::share('title','About');
		View::share('path','Index');
		$data['about'] = AboutModel::find(1);
		return View::make('backend.about.index',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name'                   => 'required',
			'email'                  => 'required|email',
			'address'                => 'required',
			'phone_number'           => 'required|numeric',
			'web_keywords'           => 'required',
			'nilai'       => 'required|numeric'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/about')->withErrors($valid)->withInput();
		}else{
			$picture                    = Input::get('picture');
			$cm                         = AboutModel::find(1);
			$cm->name                   = Input::get('name');
			$cm->email                  = Input::get('email');
			$cm->web_keywords           = Input::get('web_keywords');
			$cm->address                = Input::get('address');
			$cm->phone                  = Input::get('phone_number');
			$cm->under                  = Input::get('nilai');
			$cm->save();
			return Redirect::to('admin/about')->with('about','Data telah diperbarui');
		}
	}
}
