<?php

class Dashboard extends BaseController {
	public $limit = 10;
	public function getIndex()
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$filter        = Input::get('filter');
		$tahun         = Input::get('tahun');
		$firstYear     = ProgramModel::orderBy('year','asc')->take(1)->first();
		$data['tahun'] = range($firstYear->year,date('Y')+2);

		if(!empty($filter) && !empty($tahun)){
			if($filter==1){

				$qr	= ProgramModel::orderBy('id','desc');
				if($cari){
					$qr = $qr->where('name','LIKE',"%$cari%");
				}
				if($tahun){
					$qr = $qr->where('year',$tahun);
				}
				$qr = $qr->paginate($this->limit);
				$data['program'] = $qr;

			}elseif ($filter==2) {

				$qr	= KegiatanModel::join('program', 'kegiatan.id_program','=','program.id')
						->select('kegiatan.id as id','kegiatan.name as kegiatan_name', 'program.year as year', 'kegiatan.code_kegiatan as code_kegiatan', 'kegiatan.pagu_kegiatan as pagu_kegiatan', 'program.id as program_id')
						->orderBy('kegiatan.id','desc');
				if($cari){
					$qr = $qr->where('kegiatan.name','LIKE',"%$cari%");
				}
				if($tahun){
					$qr = $qr->where('program.year',$tahun);
				}
				$qr = $qr->paginate($this->limit);
				$data['kegiatan'] = $qr;

			}elseif($filter==3) {

				$qr	= KPAModel::join('user_program','kpa_users.id','=','user_program.id_user')
						->join('program','user_program.id_program','=','program.id')
						->select(DB::raw('count(kpa_users.id) as total, kpa_users.*, program.*, kpa_users.id as kpa_id'))
						->orderBy('kpa_users.id','desc')
						->groupBy('kpa_users.id');
				if($cari){
					$qr = $qr->where('instansi_name','LIKE',"%$cari%");
				}
				if($tahun){
					$qr = $qr->where('year',$tahun);
				}
				$qr = $qr->paginate($this->limit);
				$data['user'] = $qr;

			}
		}
		// return Helper::unders();
		$nilai           = Helper::underValue();
		$IDUser          = (Helper::unders())?array_unique(Helper::unders()):0;
		// print_r(array_unique($IDUser));
		// return;
		$data['under80'] = ($IDUser)?KPAModel::whereIn('id',$IDUser)->paginate($this->limit):0;
		// $data['under80'] = ProsentaseModel::where('fisik_value','<=',$nilai)->orWhere('sppd_value','<=',$nilai)->orWhere('spj_value','<=',$nilai)->groupBy('id_user')->paginate($this->limit);
		return View::make('backend.dashboard.index',$data);
	}

	public function getModalPrintByUser($kpa,$tahun)
	{
		// $tahun = Input::get('tahun');
		$pm = ProgramModel::orderBy('id','asc');
		if(!empty($tahun)){
			$pm = $pm->where('year',$tahun);
		}
		$pm = $pm->get();
		foreach ($pm as $val) {
			$program[] = $val->id;
		}
		$data['kpa']     = $kpa;
		$data['tahun']   = $tahun;
		$data['tahap']   = TahapModel::orderBy('id','asc')->get();
		$data['program'] = UserProgramModel::whereIn('id_program',$program)->where('id_user',$kpa)->get();
		return View::make('backend.dashboard.modal_print',$data);
	}

	public function getPrint()
	{
		$rules = array(
			'program'  => 'required|numeric',
			'kegiatan' => 'required|numeric',
			'tahap'    => 'required|numeric'
		);
		$valid = Validator::make(Input::all(),$rules);
		if(!$valid->fails())
		{
			$year     = Input::get('tahun');
			$kpa      = Input::get('kpa');
			$program  = Input::get('program');
			$kegiatan = Input::get('kegiatan');
			$tahap    = Input::get('tahap');
			if(!empty($kegiatan) && !empty($tahap)){
				$dtm = DetailTahapModel::where('id_tahap',$tahap)->where('year',$year)->get();
				foreach ($dtm as $key => $value) {
					$tahapKegiatan[] = $value->id;
				}
				$data['tahun']		 = $year;
				$data['kpa']         = KPAModel::find($kpa);
				$data['tahap']       = $tahap;
				$data['program']     = ProgramModel::find($program);
				$data['item']        = ItemKegiatanModel::where('id_kegiatan',$kegiatan)
										->whereIn('id_detail_tahap',$tahapKegiatan)
										->where('id_user',$kpa)
										->orderBy('id_detail_tahap','asc')->get();
				$data['new']         = UserKegiatanModel::where('id_program',$program)
										->where('id_kegiatan',$kegiatan)
										->where('id_user',$kpa)->first();
				$daat['kegiatan']    = UserKegiatanModel::where('id_user',$kpa)->where('id_program',$program)->where('id_kegiatan',$kegiatan)->first();
				$print               = View::make('adminlte.program.print',$data);
		    	return PDF::load($print, 'A4', 'landscape')->show();
			}
		}
	}

	public function postSearchKegiatan()
	{
		$program = Input::get('ID_PRO');
		$kpa     = Input::get('KPA');
		if(!empty($program) && !empty($kpa)){
			$ukm = UserKegiatanModel::where('id_program',$program)->where('id_user',$kpa)->get();
			foreach ($ukm as $key => $value) {
				$data[] = '<option value="'.$value->id_kegiatan.'">'.$value->kegiatan->name.'</option>';
			}
			if(isset($data)){
				return $data;
			}else{
				return '<option>- Data is empty -</option>';
			}
		}
	}

	public function postKegiatanByProgram($program,$kpa)
	{
		if(!empty($program) && !empty($kpa)){
			$kegiatan = UserKegiatanModel::where('id_program',$program)->where('id_user',$kpa)->get();
			return $kegiatan;
		}
	}

	public function getKpaProgram($kpa)
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($kpa)){
			$nilai           = Helper::underValue();
			$data['nilai']   = $nilai;
			$data['back']    = URL::to('admin/home');
			$data['limit']   = $this->limit;
			$data['kpa']     = KPAModel::find($kpa);
			// $data['program'] = ProsentaseModel::where('fisik_value','<=',$nilai)
			// 					->orWhere('sppd_value','<=',$nilai)
			// 					->orWhere('spj_value','<=',$nilai)
			// 					->where('id_user',$kpa)
			// 					->groupBy('id_program')
			// 					->orderBy('id_program','asc')
			// 					->paginate($this->limit);
			$program = Helper::under_kegiatan($kpa);
			$data['program'] = ProgramModel::whereIn('id',array_unique($program))->paginate($this->limit);
			return View::make('backend.dashboard.kpa_program',$data);
		}
	}

	public function getKpaKegiatan($kpa,$program)
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($kpa)){
			$nilai            = Helper::underValue();
			$data['back']     = URL::to('admin/home/kpa-program/'.$kpa);
			$data['limit']    = $this->limit;
			$data['kpa']      = KPAModel::find($kpa);
			$data['program']  = ProgramModel::find($program);
			// $data['kegiatan'] = ProsentaseModel::where('fisik_value','<=',$nilai)
			// 					->orWhere('sppd_value','<=',$nilai)
			// 					->orWhere('spj_value','<=',$nilai)
			// 					->where('id_user',$kpa)
			// 					->where('id_program',$program)
			// 					->orderBy('id_kegiatan','asc')
			// 					->paginate($this->limit);
			$kegiatan = Helper::under_kegiatan_list($program,$kpa);
			$data['kegiatan'] = KegiatanModel::whereIn('id',$kegiatan)->paginate($this->limit);
			return View::make('backend.dashboard.kpa_kegiatan',$data);
		}
	}
	public function getKpaSubKegiatan($user,$program,$kegiatan)
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($user) && !empty($kegiatan)){
			$data['back']     = URL::to('admin/home/kpa-kegiatan/'.$user.'/'.$program);
			$km               = KegiatanModel::find($kegiatan);
			$data['user']     = KPAModel::find($user);
			$data['kegiatan'] = $km;
			$data['program']  = ProgramModel::find($km->id_program);
			$data['item']     = ItemKegiatanModel::where('id_kegiatan',$kegiatan)->where('id_user',$user)->orderBy('id_detail_tahap','asc')->get();
			$data['ikm']      = UserKegiatanModel::where('id_program',$km->id_program)->where('id_kegiatan',$kegiatan)->where('id_user',$user)->first();
			return View::make('backend.dashboard.kpa_item',$data);
		}
	}
	public function getUserProgram($filter,$user,$year,$search=null)
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($filter) && !empty($user) && !empty($year)){
			$data['search']  = $search;
			$data['back']    = URL::to('admin/home?filter='.$filter.'&tahun='.$year.'&search=').($search?$search:'');
			$data['filter']  = $filter;
			$data['year']    = $year;
			$data['limit']   = $this->limit;
			$data['program'] = KPAModel::join('user_program','kpa_users.id','=','user_program.id_user')
							->join('program','user_program.id_program','=','program.id')
							->select('program.*')
							->where('kpa_users.id',$user)
							->where('program.year',$year)
							->orderBy('kpa_users.id','desc')
							->paginate($this->limit);
			$data['kpa'] = KPAModel::find($user);
			return View::make('backend.dashboard.user_program',$data);
		}
	}

	public function getUserKegiatan($filter,$user,$program,$year,$search=null)
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($filter) && !empty($user) && !empty($year)){
			$data['search']  = $search;
			$data['back']     = URL::to('admin/home/user-program/'.$filter.'/'.$user.'/'.$year).($search?'/'.$search:'');
			$data['filter']   = $filter;
			$data['year']     = $year;
			$data['limit']    = $this->limit;
			
			$pm               = ProgramModel::where('id',$program)->where('year',$year)->first();
			$upm              = UserProgramModel::where('id_program',$pm->id)->where('id_user',$user)->first();
			$data['kegiatan'] = UserKegiatanModel::where('id_program',$upm->id_program)->where('id_user',$user)->paginate($this->limit);
			$data['program']  = $pm;
			$data['kpa']      = KPAModel::find($user);
			return View::make('backend.dashboard.user_kegiatan',$data);
		}
	}

	public function getUserSubKegiatan($user,$program,$kegiatan,$year,$search=null)
	{
		// return Request::segment(2);
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($user) && !empty($year)){
			$data['search']  = $search;
			$data['back']     = URL::to('admin/home/user-kegiatan/3/'.$user.'/'.$program.'/'.$year).($search?'/'.$search:'');
			$data['year']     = $year;
			// $data['limit']    = $this->limit;
			
			// $pm               = ProgramModel::where('id',$program)->where('year',$year)->first();
			// $upm              = UserProgramModel::where('id_program',$pm->id)->where('id_user',$user)->first();
			// $data['kegiatan'] = UserKegiatanModel::where('id_program',$upm->id_program)->where('id_user',$user)->paginate($this->limit);
			// $data['program']  = $pm;
			// $data['kpa']      = KPAModel::find($user);

			$km               = KegiatanModel::find($kegiatan);
			$data['user']     = KPAModel::find($user);
			$data['kegiatan'] = $km;
			$data['program']  = ProgramModel::find($km->id_program);
			// $data['back']     = URL::to('admin/home/detail/'.$filter.'/'.$id.'/'.$tahun);
			$data['item']     = ItemKegiatanModel::where('id_kegiatan',$kegiatan)->where('id_user',$user)->orderBy('id_detail_tahap','asc')->get();
			$data['ikm']      = UserKegiatanModel::where('id_program',$km->id_program)->where('id_kegiatan',$kegiatan)->where('id_user',$user)->first();
			$data['download'] = url('admin/home/download/');
			return View::make('backend.dashboard.user_sub_kegiatan',$data);
		}
	}

	public function getDownload($id)
	{
		$itkm = ItemKegiatanModel::find($id);
		if (is_file(public_path('assets/photo_bukti/'.$itkm->photo))) {
			return Response::download(public_path('assets/photo_bukti/'.$itkm->photo));
		} else {
			return 'file does not exists';
		}
	}

	public function getDetail($filter,$id,$tahun,$search=null)
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($filter) && !empty($id) && !empty($tahun)){
			$data['back']  = URL::to('admin/home?filter='.$filter.'&tahun='.$tahun.'&search=').($search?$search:'');
			$data['limit'] = $this->limit;
			if($filter==1){
				$upm     = UserProgramModel::where('id_program',$id)->get();
				$users[] = '';
				foreach ($upm as $key => $value) {
					$users[] = $value->id_user;
				}
				$data['program']   = ProgramModel::find($id);
				$data['kpa_users'] = KPAModel::whereIn('id',$users)->paginate($this->limit);
				return View::make('backend.dashboard.detail_user_kpa',$data);
			}elseif ($filter==2) {
				$data['filter']   = $filter;
				$data['id']       = $id;
				$data['tahun']    = $tahun;
				$data['item']     = ItemKegiatanModel::where('id_kegiatan',$id)->groupBy('id_user')->paginate($this->limit);
				$km               = KegiatanModel::find($id);
				$data['kegiatan'] = $km;
				$data['program']  = ProgramModel::find($km->id_program);
				return View::make('backend.dashboard.detail_kegiatan',$data);
			}
		}
	}

	public function getDetailProgram($filter,$id,$tahun,$search=null)
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($filter) && !empty($id) && !empty($tahun)){
			$data['back']  = URL::to('admin/home?filter='.$filter.'&tahun='.$tahun.'&search=').($search?$search:'');
			$data['limit'] = $this->limit;
			if($filter==1){
				$data['program'] = ProgramModel::find($id);
				$data['kegiatan'] = KegiatanModel::where('id_program',$id)->paginate($this->limit);
				return View::make('backend.dashboard.detail_program',$data);
			}
		}
	}

	public function getUserItem($user,$kegiatan,$filter,$id,$tahun)
	{
		View::share('path','Index');
		View::share('title','Dashboard');
		if(!empty($user) && !empty($kegiatan)){
			$km               = KegiatanModel::find($kegiatan);
			$data['user'] = KPAModel::find($user);
			$data['kegiatan'] = $km;
			$data['program']  = ProgramModel::find($km->id_program);
			$data['back']     = URL::to('admin/home/detail/'.$filter.'/'.$id.'/'.$tahun);
			$data['item']     = ItemKegiatanModel::where('id_kegiatan',$kegiatan)->where('id_user',$user)->orderBy('id_detail_tahap','asc')->get();
			$data['ikm'] 	  = UserKegiatanModel::where('id_program',$km->id_program)->where('id_kegiatan',$kegiatan)->where('id_user',$user)->first();
			$data['download'] = url('admin/home/download/');
			return View::make('backend.dashboard.user_item',$data);
		}
	}

	public function getModalDetailJumlahKpa($idProgram)
	{
		$upm     = UserProgramModel::where('id_program',$idProgram)->get();
		$users[] = '';
		foreach ($upm as $key => $value) {
			$users[] = $value->id;
		}
		$data['kpa_users'] = KPAModel::whereIn('id',$users)->get();
		return View::make('backend.dashboard.modal_detail_jumlah_kpa',$data);
	}

	public function postIndex()
	{
		$sales = Input::get('sales');
		if($sales){
			Session::put('D_year',$sales);
		}
		return Redirect::to('admin/home');
	}

	public function MonthName($month)
	{
		for ($i=1; $i <=$month ; $i++) { 
			switch ($i) {
				case 1:
					$Mth[] = 'January';
				break;
				
				case 2:
					$Mth[] = 'February';
				break;
				
				case 3:
					$Mth[] = 'March';
				break;
				
				case 4:
					$Mth[] = 'April';
				break;
				
				case 5:
					$Mth[] = 'May';
				break;
				
				case 6:
					$Mth[] = 'June';
				break;
				
				case 7:
					$Mth[] = 'July';
				break;
				
				case 8:
					$Mth[] = 'August';
				break;
				
				case 9:
					$Mth[] = 'September';
				break;
				
				case 10:
					$Mth[] = 'October';
				break;
				
				case 11:
					$Mth[] = 'November';
				break;
				
				case 12:
					$Mth[] = 'December';
				break;
			}
		}
		return $Mth;
	}

	public function logout()
	{
		// Session::forget('admin');
		Session::flush();
		return Redirect::to('login');
	}

	public function ChartItemSales($year)
	{
		$vm = DB::table('orders_detail')
             ->select(DB::raw('month(created_at) as month, year(created_at) as year'))
             ->whereRaw('year(created_at) = '.$year)
             ->get();
             	if(count($vm) > 0){
			        $mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;

				    foreach ($vm as $row) {
				    	if($row->month == 1){
					        $mon['jan']++;
				    	}elseif($row->month == 2){
					        $mon['feb']++;
				    	}elseif($row->month == 3){
					        $mon['mar']++;
				    	}elseif($row->month == 4){
					        $mon['apr']++;
				    	}elseif($row->month == 5){
					        $mon['mei']++;
				    	}elseif($row->month == 6){
					        $mon['jun']++;
				    	}elseif($row->month == 7){
					        $mon['jul']++;
				    	}elseif($row->month == 8){
					        $mon['agu']++;
				    	}elseif($row->month == 9){
					        $mon['sep']++;
				    	}elseif($row->month == 10){
					        $mon['oct']++;
				    	}elseif($row->month == 11){
					        $mon['nov']++;
				    	}elseif($row->month == 12){
					        $mon['dec']++;
				    	}
				    }
				    return $mon;
				}else{
					$mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;
			        return $mon;
				}
	}

	public function ChartItemSalesOfNow($year)
	{
		$months = date('n');
		$vm = DB::table('orders_detail')
             ->select(DB::raw('month(created_at) as month, year(created_at) as year'))
             ->whereRaw('month(created_at) <= '.$months)
             ->whereRaw('year(created_at) = '.$year)
             ->get();
         		for ($i=1; $i <= $months; $i++) { 
         			$mon[$i] = 0;
         		}
             	if(count($vm) > 0){
				    foreach ($vm as $key => $row) {
				    	
				    	if($row->month == 1){
					        $mon[1]++;
				    	}elseif($row->month == 2){
					        $mon[2]++;
				    	}elseif($row->month == 3){
					        $mon[3]++;
				    	}elseif($row->month == 4){
					        $mon[4]++;
				    	}elseif($row->month == 5){
					        $mon[5]++;
				    	}elseif($row->month == 6){
					        $mon[6]++;
				    	}elseif($row->month == 7){
					        $mon[7]++;
				    	}elseif($row->month == 8){
					        $mon[8]++;
				    	}elseif($row->month == 9){
					        $mon[9]++;
				    	}elseif($row->month == 10){
					        $mon[10]++;
				    	}elseif($row->month == 11){
					        $mon[11]++;
				    	}elseif($row->month == 12){
					        $mon[12]++;
				    	}
				    }
				}
			    return $mon;
	}
}