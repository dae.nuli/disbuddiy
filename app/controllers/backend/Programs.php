<?php
class Programs extends BaseController {
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Program');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');

		$year = (Session::get('year'))?Session::get('year'):date('Y');
		$qr   = ProgramModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('name','LIKE',"%$cari%")->orWhere('code_program','LIKE',"%$cari%");
		}
		if(!empty($year)){
			$qr = $qr->where('year',$year);
		}
		$qr = $qr->paginate($this->limit);
		$firstYear         = ProgramModel::orderBy('year','asc')->take(1)->first();
		$data['year']      = $year;
		$data['yearRange'] = range($firstYear->year,date('Y')+2);
		$data['program']   = $qr;
		return View::make('backend.program.index',$data);
	}

	public function postYear()
	{
		$year = Input::get('year');
		if($year){
			Session::put('year',$year);
		}
		return Redirect::to('admin/program');
	}

	public function getDetailSubKegiatan($kpa,$kegiatan)
	{
		$pam              = KPAModel::find($kpa);
		$km               = KegiatanModel::find($kegiatan);
		$pm               = ProgramModel::find($km->id_program);
		$data['kegiatan'] = $km;
		$data['program']  = $pm;
		View::share('title','Program');
		View::share('path','Detail Kegiatan');
		$data['kpa'] 	  = $pam;
		$data['new']      = UserKegiatanModel::where('id_program',$pm->id)->where('id_kegiatan',$kegiatan)->where('id_user',$kpa)->first();
		$data['user']     = $pam->code_kpa;
		$data['item']     = ItemKegiatanModel::where('id_kegiatan',$kegiatan)->where('id_user',$kpa)->orderBy('id_detail_tahap','asc')->get();
		return View::make('backend.program.detail_sub_kegiatan',$data);
	}

	public function getModalImportProgram()
	{
		$data['program'] = ProgramModel::orderBy('id','asc')->get();
		return View::make('backend.program.modal_import_program',$data);
	}

	public function postModalImportProgram()
	{
		$rules = array(
			'program' => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/program/create')->withErrors($valid)->withInput();
		}else{
			$program = Input::get('program');
			return Redirect::to('admin/program/create?program='.$program);
		}
	}

	public function getModalDetailUser()
	{
		View::share('title','Program');
		View::share('path','program');

		$data['userKPA'] = KPAModel::all();
		return View::make('backend.program.modal_pilih_user',$data);
	}

	public function postCheckUser($idProgram)
	{
		$code = Input::get('kode_kpa');
		if(!empty($code)){
			$users[] = '';
			$pm = UserProgramModel::where('id_program',$idProgram)->get();
			foreach ($pm as $a => $b) {
				$users[] = $b->id_user;
			}
			if(count($users)>0){
				$km = KPAModel::whereNotIn('id',$users)->where('code_kpa',$code)->first();
			}else{
				$km = KPAModel::where('code_kpa',$code)->first();
			}
			if(empty($km)){
				$data['valid'] = false;
				$data['message'] = 'Data KPA salah/Data sudah ada';
				echo json_encode($data);
			}else{
				$data['valid'] = true;
				echo json_encode($data);
			}
		}
	}

	public function postFindUser($idProgram)
	{
		$kode = Input::get('query');
		if(!empty($kode)){
			$users[] = '';
			$pm = UserProgramModel::where('id_program',$idProgram)->get();
			foreach ($pm as $a => $b) {
				$users[] = $b->id_user;
			}
			if(count($users)>0){
				$km = KPAModel::whereNotIn('id',$users)->where('code_kpa','like',$kode.'%')->get();
			}else{
				$km = KPAModel::where('code_kpa','like',$kode.'%')->get();
			}
			if(count($km)>0){
				$data['query'] = 'Unit';
				foreach ($km as $key => $val) {
					$datas[$key]['value']   = $val->code_kpa;
					$datas[$key]['name']    = $val->instansi_name;
					$datas[$key]['kode']    = $val->personil_name;
				}
				$data['suggestions'] = $datas;
				return json_encode($data);
			}else{
				$data['query'] = 'Unit';
				$data['suggestions'] = '';
				return json_encode($data);
			}
		}
	}

	public function postAddUser($idProgram)
	{
		if(!empty($idProgram)){
			$kode_kpa = Input::get('kode_kpa');
			if(!empty($kode_kpa)){
				$km = KPAModel::where('code_kpa',$kode_kpa)->first();
				if(count($km)>0){
					$upm = UserProgramModel::where('id_program',$idProgram)->where('id_user',$km->id)->count();
					if($upm>0){
						return Redirect::to('admin/program/sync/'.$idProgram)->with('kpa_alert','KPA dengan program ini sudah ada');
					}else{
						$um             = new UserProgramModel;
						$um->id_program = $idProgram;
						$um->id_user    = $km->id;
						$um->save();
						return Redirect::to('admin/program/sync/'.$idProgram)->with('kpa','KPA berhasil ditambah');
					}
				}else{
					return Redirect::to('admin/program/sync/'.$idProgram)->with('kpa_alert','KPA tidak ditemukan');
				}
			}else{
				return Redirect::to('admin/program/sync/'.$idProgram)->with('kpa_alert','KPA tidak ditemukan');
			}
		}
	}
	
	public function getCreate()
	{
		View::share('path','Create');
		View::share('title','Program');
		$program = Input::get('program');
		$data['yearRange'] = range(date('Y'),date('Y')+2);
		if(!empty($program)){
			$data['program']  = ProgramModel::find($program);
			$data['kegiatan'] = KegiatanModel::where('id_program',$program)->get();
			return View::make('backend.program.create_import',$data);
		}else{
			return View::make('backend.program.create',$data);
		}
	}

	public function postIndex()
	{
		$rules = array(
			'kode_program' => 'required|unique:program,code_program',
			'nama_program' => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/program/create')->withErrors($valid)->withInput();
		}else{
			$cm               = new ProgramModel;
			$cm->code_program = Input::get('kode_program');
			$cm->name         = Input::get('nama_program');
			$cm->year         = Input::get('tahun');
			$cm->created_by	  = $this->admin['id'];
			$cm->save();

			$kode        = Input::get('kode');
			$name        = Input::get('name');
			$pagu        = Input::get('pagu');
			$description = Input::get('description');
			if(!empty($name)){
				foreach ($name as $key => $value) {
					$km = new KegiatanModel;
					$km->id_program    = $cm->id;
					$km->code_kegiatan = $kode[$key];
					$km->name          = $name[$key];
					$km->pagu_kegiatan = ($pagu[$key]?$pagu[$key]:'');
					$km->description   = ($description[$key]?$description[$key]:'');
					$km->created_by    = $this->admin['id'];
					$km->save();
				}

			}
			// Session::forget('year');
			return Redirect::to('admin/program')->with('program','Data telah ditambah');
		}
	}

	public function getDetail($id)
	{
		View::share('path','Detail');
		View::share('title','Program');

		$data['program'] = ProgramModel::find($id);
		$data['kegiatan'] = KegiatanModel::where('id_program',$id)->get();
		return View::make('backend.program.detail',$data);	
	}

	public function getModalAddKegiatan($id)
	{
		$data['idProgram'] = $id;
		return View::make('backend.program.modal_add_kegiatan',$data);
	}

	public function getModalAddKegiatanKpa($program,$kpa)
	{
		$data['idProgram'] = $program;
		$data['idKPA']     = $kpa;
		$ukm = UserKegiatanModel::where('id_program',$program)->where('id_user',$kpa)->get();
		$kegiatan[] = '';
		foreach ($ukm as $key => $value) {
			$kegiatan[] = $value->id_kegiatan;
		}
		$data['kegiatan']  = KegiatanModel::where('id_program',$program)->whereNotIn('id',$kegiatan)->get();
		return View::make('backend.program.modal_add_kegiatan_kpa',$data);
	}

	public function postAddKegiatanKpa($program,$kpa)
	{
		$dp['_token']         = Input::get('_token');
		$dp['nama_kegiatan']  = Input::get('nama_kegiatan');
		$dp['pagu_awal']      = str_replace(',', '', Input::get('pagu_awal'));
		$dp['pagu_perubahan'] = (Input::get('pagu_perubahan')?str_replace(',', '', Input::get('pagu_perubahan')):'');
		$dp['keterangan']     = Input::get('keterangan');

		$rules = array(
			'nama_kegiatan'  => 'required',
			'pagu_awal'      => 'required|numeric',
			'pagu_perubahan' => 'numeric'
		);
		$valid = Validator::make($dp,$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/program/sync-kegiatan/'.$kpa.'/'.$program)->withErrors($valid);
		}else{
			$km                          = new UserKegiatanModel;
			$km->id_program              = $program;
			$km->id_kegiatan             = Input::get('nama_kegiatan');
			$km->id_user                 = $kpa;
			$km->pagu_kegiatan           = str_replace(',', '', Input::get('pagu_awal'));
			$km->pagu_kegiatan_perubahan = (Input::get('pagu_perubahan')?str_replace(',', '', Input::get('pagu_perubahan')):'');
			$km->keterangan              = Input::get('keterangan');
			$km->save();

			$um = UserProgramModel::where('id_program',$program)->where('id_user',$kpa)->count();
			if(empty($um)){
				$upm = new UserProgramModel;
				$upm->id_program = $program;
				$upm->id_user = $kpa;
				$upm->save();
			}
			return Redirect::to('admin/program/sync-kegiatan/'.$kpa.'/'.$program)->with('program','Data kegiatan telah ditambah'); 
		}
	}

	public function postAddKegiatan($program)
	{
		$rules = array(
			// 'kode_kegiatan' => 'required|unique:kegiatan,code_kegiatan',
			'kode_kegiatan' => 'required',
			'nama_kegiatan' => 'required'
			// 'pagu_kegiatan' => 'required|numeric',
			// 'keterangan'    => 'required' 
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/program/detail/'.$program)->withErrors($valid);
		}else{
			$kms = KegiatanModel::where('id_program',$program)->count();
			if($kms<20){
				$km                = new KegiatanModel;
				$km->id_program    = $program;
				$km->code_kegiatan = Input::get('kode_kegiatan');
				$km->name          = Input::get('nama_kegiatan');
				// $km->pagu_kegiatan = Input::get('pagu_kegiatan');
				// $km->description   = Input::get('keterangan');
				$km->save();
				return Redirect::to('admin/program/detail/'.$program)->with('program','Data kegiatan telah ditambah'); 
			}else{
				return Redirect::to('admin/program/detail/'.$program)->with('program_alert','Data kegiatan tidak boleh lebih dari 20'); 
			}
		}
	}

	public function getEditKegiatan($idProgram,$idKegiatan)
	{
		View::share('path','Ubah Kegiatan');
		View::share('title','Program');

		$data['kegiatan'] = KegiatanModel::where('id',$idKegiatan)->where('id_program',$idProgram)->first();
		return View::make('backend.program.edit_kegiatan',$data);	
	}

	public function postEditKegiatan($idProgram,$idKegiatan)
	{
		$rules = array(
			// 'kode_kegiatan' => 'required|unique:kegiatan,code_kegiatan,'.$idKegiatan,
			'kode_kegiatan' => 'required',
			'nama_kegiatan' => 'required'
			// 'pagu_kegiatan' => 'required',
			// 'keterangan'    => 'required' 
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/program/edit-kegiatan/'.$idProgram.'/'.$idKegiatan)->withErrors($valid);
		}else{
			$km                = KegiatanModel::find($idKegiatan);
			$km->code_kegiatan = Input::get('kode_kegiatan');
			$km->name          = Input::get('nama_kegiatan');
			// $km->pagu_kegiatan = Input::get('pagu_kegiatan');
			$km->description   = Input::get('keterangan');
			$km->save();
			return Redirect::to('admin/program/detail/'.$idProgram)->with('program','Data kegiatan telah diubah'); 
		}		
	}

	public function getDeleteKegiatan($program,$kegiatan)
	{
		if(!empty($program) && !empty($kegiatan)){
			KegiatanModel::find($kegiatan);
			UserKegiatanModel::where('id_kegiatan',$kegiatan)->delete();
			ItemKegiatanModel::where('id_kegiatan',$kegiatan)->delete();
			// KegiatanModel::where('id',$kegiatan)->where('id_program',$program)->delete();
			return Redirect::to('admin/program/detail/'.$program)->with('program','Data kegiatan telah dihapus');
		}
	}

	public function getModalAddUser($id)
	{
		View::share('path','Pilih KPA');
		View::share('title','Program');

		$users[] = '';
		$pm = UserProgramModel::where('id_program',$id)->get();
		foreach ($pm as $a => $b) {
			$users[] = $b->id_user;
		}
		if(count($users)>0){
			$kpa = KPAModel::whereNotIn('id',$users);
		}else{
			$kpa = KPAModel::orderBy('id','asc');
		}
		$data['program'] = ProgramModel::find($id);
		$data['kpa']     = $kpa->get();
		// Session::put('kembali',URL::to('admin/program/sync-user/'.$id));
		return View::make('backend.program.modal_add_kpa',$data);
	}

	public function postModalAddUser($id)
	{
		$rules = array(
			'nama_instansi' => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/program/sync/'.$id)->withErrors($valid);
		}else{
			$kpa = Input::get('nama_instansi');
			$um  = UserProgramModel::where('id_program',$id)->where('id_user',$kpa)->count();
			if(empty($um)){
				$upm = new UserProgramModel;
				$upm->id_program = $id;
				$upm->id_user = $kpa;
				$upm->save();
			}
			return Redirect::to('admin/program/sync/'.$id)->with('program','Data PA/KPA telah ditambah'); 
		}
		
	}

	public function getSyncUser($id)
	{
		View::share('path','Pilih KPA');
		View::share('title','Program');

		$cari          = Input::get('search');
		$users[] = '';
		$pm = UserProgramModel::where('id_program',$id)->get();
		foreach ($pm as $a => $b) {
			$users[] = $b->id_user;
		}
		if(count($users)>0){
			$kpa = KPAModel::whereNotIn('id',$users);
			if($cari){
				$kpa = $kpa->where('instansi_name','LIKE',"%$cari%");
			}
		}else{
			$kpa = KPAModel::orderBy('id','asc');
			if($cari){
				$kpa = $kpa->where('instansi_name','LIKE',"%$cari%");
			}
		}
		$data['limit']   = $this->limit;
		$data['program'] = ProgramModel::find($id);
		$data['kpa']     = $kpa->paginate($this->limit);
		// Session::put('kembali',URL::to('admin/program/sync-user/'.$id));
		return View::make('backend.program.sync_choose_kpa',$data);
	}

	public function getSyncKegiatan($user,$program)
	{
		View::share('path','Pilih Kegiatan');
		View::share('title','Program');

		$data['program']  = ProgramModel::find($program);
		$data['kpa']      = KPAModel::find($user);
		$data['kegiatan'] = UserKegiatanModel::where('id_program',$program)->where('id_user',$user)->get();
		return View::make('backend.program.sync_choose_kegiatan',$data);	
	}

	public function getSync($id)
	{
		View::share('path','Sync');
		View::share('title','Program');

		$data['limit'] = $this->limit;
		$data['program'] = ProgramModel::find($id);
		$data['userProgram'] = UserProgramModel::where('id_program',$id)->paginate($this->limit);
		// $data['userKegiatan'] = UserKegiatanModel::where('id_program',$id)->groupBy('id_program')->paginate($this->limit);
		Session::put('kembali',URL::to('admin/program/sync/'.$id));
		return View::make('backend.program.sync',$data);
	}

	public function getDeleteSync($program,$id)
	{
		if(!empty($program)&&!empty($id)){
			$upm = UserProgramModel::find($id);
			$ukm = UserKegiatanModel::where('id_program',$program)->where('id_user',$upm->id_user)->get();
			if(count($ukm)>0){
				foreach ($ukm as $key => $value) {
					ItemKegiatanModel::where('id_user',$upm->id_user)->where('id_kegiatan',$value->id_kegiatan)->delete();
				}
			}
			UserKegiatanModel::where('id_program',$program)->where('id_user',$upm->id_user)->delete();
			$upm->delete();
			return Redirect::to('admin/program/sync/'.$program)->with('kpa','KPA berhasil dihapus');
		}
	}

	public function getModalSyncUser($id)
	{
		$data['idProgram'] = $id;
		return View::make('backend.program.sync_user',$data);
	}
	
	public function getModalSyncEditKegiatan($id,$program,$kpa)
	{
		$data['program'] = $program;
		$data['kpa']     = $kpa;
		$data['pagu']    = UserKegiatanModel::find($id);
		return View::make('backend.program.modal_sync_edit_kegiatan',$data);
	}

	public function postEditPaguKegiatan($id,$program,$kpa)
	{
		$dp['_token']         = Input::get('_token');
		$dp['pagu_awal']      = str_replace(',', '', Input::get('pagu_awal'));
		$dp['pagu_perubahan'] = (Input::get('pagu_perubahan')?str_replace(',', '', Input::get('pagu_perubahan')):'');
		$dp['keterangan']     = Input::get('keterangan');

		$rules = array(
			'pagu_awal'      => 'required|numeric',
			'pagu_perubahan' => 'numeric'
		);
		$valid = Validator::make($dp,$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/program/sync-kegiatan/'.$kpa.'/'.$program)->withErrors($valid);
		}else{
			$cm                          = UserKegiatanModel::find($id);
			$cm->pagu_kegiatan           = str_replace(',', '', Input::get('pagu_awal'));
			$cm->pagu_kegiatan_perubahan = (Input::get('pagu_perubahan')?str_replace(',', '', Input::get('pagu_perubahan')):'');
			$cm->keterangan              = Input::get('keterangan');
			$cm->save();
			return Redirect::to('admin/program/sync-kegiatan/'.$kpa.'/'.$program)->with('program','Data kegiatan telah diperbarui'); 
		}
	}

	public function getModalDetailKpa($id)
	{
		$data['kpa'] = KPAModel::find($id);
		return View::make('backend.program.modal_detail_kpa',$data);
	}
	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','Program');

		$data['program'] = ProgramModel::find($id);
		$data['yearRange'] = range(date('Y'),date('Y')+2);
		return View::make('backend.program.edit',$data);
	}

	public function postUpdate($id)
	{
		$rules = array(
			'kode_program' => 'required|unique:program,code_program,'.$id,
			'nama_program' => 'required',
			'tahun'        => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/program/edit/'.$id)->withErrors($valid);
		}else{
			$cm               = ProgramModel::find($id);
			$cm->code_program = Input::get('kode_program');
			$cm->name         = Input::get('nama_program');
			$cm->year         = Input::get('tahun');
			$cm->updated_by   = $this->admin['id'];
			$cm->save();
			return Redirect::to('admin/program')->with('program','Data program telah diperbarui'); 
		}
	}
	public function getDeleteSyncKegiatan($id,$kpa,$program)
	{
		$pm = UserKegiatanModel::find($id);
		ItemKegiatanModel::where('id_user',$kpa)->where('id_kegiatan',$pm->id_kegiatan)->delete();
		$pm->delete();
		return Redirect::to('admin/program/sync-kegiatan/'.$kpa.'/'.$program)->with('program','Data kegiatan telah dihapus');
	}

	public function getDelete($id)
	{
		$pm = ProgramModel::find($id);
		$tahun = $pm->year;

		$pmTahun = ProgramModel::where('year',$tahun)->count();
		if($pmTahun<2){
			Session::forget('year');
		}
		$pm->delete();
		$km = KegiatanModel::where('id_program',$id)->get();
		if(count($km)>0){
			foreach ($km as $key => $value) {
				ItemKegiatanModel::where('id_kegiatan',$value->id)->delete();
			}
		}
		KegiatanModel::where('id_program',$id)->delete();
		ProsentaseModel::where('id_program',$id)->delete();
		UserProgramModel::where('id_program',$id)->delete();
		UserKegiatanModel::where('id_program',$id)->delete();
		return Redirect::to('admin/program')->with('program','Data program telah dihapus');
	}
}