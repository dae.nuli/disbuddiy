<?php
class Kegiatans extends BaseController {
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	// public function getIndex()
	// {
	// 	View::share('title','kegiatan');
	// 	View::share('path','Index');
	// 	$data['limit'] = $this->limit;
	// 	$cari          = Input::get('search');

	// 	$qr	= KegiatanModel::orderBy('id','desc');
	// 	if($cari){
	// 		$qr = $qr->where('name','LIKE',"%$cari%");
	// 	}
	// 	$qr = $qr->paginate($this->limit);
		
	// 	$data['kegiatan'] = $qr;
	// 	return View::make('backend.kegiatan.index',$data);
	// }

	// public function getModalDetailUser()
	// {
	// 	View::share('title','Program');
	// 	View::share('path','program');

	// 	$data['userKPA'] = KPAModel::all();
	// 	return View::make('backend.kegiatan.modal_pilih_user',$data);
	// }
	
	// public function getCreate()
	// {
	// 	View::share('path','Create');
	// 	View::share('title','kegiatan');
	// 	return View::make('backend.kegiatan.create');
	// }

	// public function postIndex()
	// {
	// 	$rules = array(
	// 		'nama_program' => 'required',
	// 		'pilih_user'   => 'required'
	// 		);
	// 	$valid = Validator::make(Input::all(),$rules);
	// 	if($valid->fails())
	// 	{
	// 		return Redirect::to('admin/program/create')->withErrors($valid)->withInput();
	// 	}else{
	// 		$cm          = new ProgramModel;
	// 		$cm->phone   = Input::get('phone_number');
	// 		$cm->address = Input::get('address');
	// 		$cm->save();
	// 		return Redirect::to('admin/program')->with('program','Data has been added');
	// 	}
	// }

	// public function getEdit($id)
	// {
	// 	View::share('path','Edit');
	// 	View::share('title','Clients');

	// 	$data['clients'] = ProgramModel::find($id);
	// 	return View::make('backend.clients.edit',$data);
	// }

	// public function postUpdate($id)
	// {
	// 	$rules = array(
	// 		'name'         => 'required',
	// 		'email'        => 'required|unique:clients,email,'.$id,
	// 		'phone_number' => 'required',
	// 		'address'      => 'required'
	// 	);
	// 	$valid = Validator::make(Input::all(),$rules);
	// 	if($valid->fails())
	// 	{
	// 		return Redirect::to('admin/clients/edit/'.$id)->withErrors($valid);
	// 	}else{
	// 		$cm          = ProgramModel::find($id);
	// 		$cm->name    = Input::get('name');
	// 		$cm->email   = Input::get('email');
	// 		$cm->phone   = Input::get('phone_number');
	// 		$cm->address = Input::get('address');
	// 		$cm->save();
	// 		return Redirect::to('admin/clients')->with('clients','Data has been updated'); 
	// 	}
	// }

	// public function getDelete($id)
	// {
	// 	$cm = ProgramModel::find($id);
	// 	$om = OrdersModel::where('id_client',$id)->count();
	// 	if($om > 0){
	// 		return Redirect::to('admin/clients')->with('clients_alert','Data is used');
	// 	}else{
	// 		$cm->delete();
	// 		return Redirect::to('admin/clients')->with('clients','Data has been deleted');
	// 	}
	// }
}