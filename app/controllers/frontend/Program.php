<?php

class Program extends BaseController {
	public $limit = 10;
	public $ip;
	public $admin;

	public function __construct()
	{
		 $this->admin = Session::get('kpaUsers');
	}

	public function getIndex()
	{
		View::share('title','Program');
		View::share('path','Home');
		$cari          = Input::get('search');
		
		$year          = (Session::get('user_year'))?Session::get('user_year'):date('Y');
		$data['limit'] = $this->limit;
		// $year       = date('Y');
		$upm           = UserProgramModel::join('program', 'user_program.id_program','=','program.id')
						->where('user_program.id_user',$this->admin['id'])
						->where('program.year',$year)
						->where('program.deleted_at',null)
						->where('user_program.deleted_at',null);
		if($cari){
			$upm = $upm->where('program.name','LIKE',"%$cari%");
		}
		$firstYear         = UserProgramModel::join('program', 'user_program.id_program','=','program.id')
							->where('user_program.id_user',$this->admin['id'])
							->where('program.deleted_at',null)
							->where('user_program.deleted_at',null)
							->where('program.year','<=',date('Y'))
							->orderBy('program.year','asc')
							->groupBy('program.year')->first();
							// ->groupBy('program.year')->get(); //Original
		$data['year']      = $year;
		$data['yearRange'] = range($firstYear->year, date('Y'));
		// $data['yearRange'] = $firstYear; //Original
		$data['program']   = $upm->get();
		$data['kpa']       = $this->admin['id'];
		// $data['program'] = UserProgramModel::where('id_user',$this->admin['id'])->where('year',$year)->get();
		return View::make('adminlte.program.index',$data);
	}

	public function getModalPrint($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$ikm              = ProgramModel::find($id);
			$data['program']  = $ikm;
			$data['ide']      = $ide;
			$data['kegiatan'] = UserKegiatanModel::where('id_user',$this->admin['id'])->where('id_program',$id)->get();
			$data['tahap'] 	  = TahapModel::orderBy('id','asc')->get();
			return View::make('adminlte.program.modal_print',$data);
		}
	}

	public function getPrint($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$year     = (Session::get('user_year'))?Session::get('user_year'):date('Y');
			$kegiatan = Helper::de(Input::get('kegiatan'));
			$tahap    = Input::get('tahap');
			if(!empty($kegiatan) && !empty($tahap)){
				$dtm = DetailTahapModel::where('id_tahap',$tahap)->where('year',$year)->get();
				foreach ($dtm as $key => $value) {
					$tahapKegiatan[] = $value->id;
				}
				$data['tahun']		 = $year;
				$data['kpa']         = KPAModel::find($this->admin['id']);
				$data['tahap']       = $tahap;
				$data['program']     = ProgramModel::find($id);
				$data['item']        = ItemKegiatanModel::where('id_kegiatan',$kegiatan)
										->whereIn('id_detail_tahap',$tahapKegiatan)
										->where('id_user',$this->admin['id'])
										->orderBy('id_detail_tahap','asc')->get();
				$data['new']         = UserKegiatanModel::where('id_program',$id)
										->where('id_kegiatan',$kegiatan)
										->where('id_user',$this->admin['id'])->first();
				// $data['kegiatan'] = KegiatanModel::find($kegiatan);
				$daat['kegiatan']    = UserKegiatanModel::where('id_user',$this->admin['id'])->where('id_program',$id)->where('id_kegiatan',$kegiatan)->first();
				// return View::make('adminlte.program.print',$data);
				$print               = View::make('adminlte.program.print',$data);
		    	return PDF::load($print, 'A4', 'landscape')->show();
			}
		}
	}
	public function postIndex()
	{
		$year = Input::get('year');
		if($year){
			Session::put('user_year',$year);
		}
		return Redirect::to('panel/program');
	}
 
	public function getDetail($ide)
	{
		// $id = Helper::de($ide);
		// if(!empty($id)){
		// 	$pm = ProgramModel::find($id);
		// 	View::share('title',$pm->name);
		// 	View::share('path','');
		// 	$data['program']  = $pm;
		// 	$data['kegiatan'] = KegiatanModel::where('id_program',$id)->get();
		// 	$data['idUser']   = $this->admin['id'];
		// 	$data['code']     = $this->admin['code'];
		// 	return View::make('adminlte.kegiatan.index',$data);
		// }
		$id = Helper::de($ide);
		if(!empty($id)){
			$pm = ProgramModel::find($id);
			View::share('title',$pm->name);
			View::share('path','');
			$data['program']  = $pm;
			$data['kegiatan'] = UserKegiatanModel::where('id_program',$id)->where('id_user',$this->admin['id'])->get();
			$data['idUser']   = $this->admin['id'];
			$data['code']     = $this->admin['code'];
			return View::make('adminlte.kegiatan.index',$data);
		}
	}

	public function getDetailKegiatan($ide)
	{
		// $id = Helper::de($ide);
		// if(!empty($id)){
		// 	$ukm = UserKegiatanModel::find($id);
		// 	$data['kegiatan'] = KegiatanModel::find($ukm->id_kegiatan);
		// 	$data['program']  = ProgramModel::find($data['kegiatan']->id_program);
		// 	View::share('title',$data['program']->name);
		// 	View::share('path','');
		// 	$data['new'] = $uk
		// 	$data['user'] = $this->admin['code'];
		// 	$data['item'] = ItemKegiatanModel::where('id_kegiatan',$ukm->id_kegiatan)->where('id_user',$this->admin['id'])->orderBy('id_detail_tahap','asc')->get();
		// 	return View::make('adminlte.kegiatan.detail',$data);
		// }
		$id = Helper::de($ide);
		if(!empty($id)){
			$data['kegiatan']    = KegiatanModel::find($id);
			$data['program']     = ProgramModel::find($data['kegiatan']->id_program);
			View::share('title',$data['program']->name);
			View::share('path','');
			$data['new']         = UserKegiatanModel::where('id_program',$data['program']->id)->where('id_kegiatan',$id)->where('id_user',$this->admin['id'])->first();
			$data['user']        = $this->admin['code'];
			$data['item']        = ItemKegiatanModel::where('id_kegiatan',$id)->where('id_user',$this->admin['id'])->orderBy('id_detail_tahap','asc')->get();
			$data['id_kegiatan'] = Helper::en($id);
			return View::make('adminlte.kegiatan.detail',$data);
		}
	}

	public function getModalAddSub($id)
	{
		$data['kegiatan'] = $id;
		return View::make('adminlte.kegiatan.modal_add_sub',$data);
	}

	public function getModalEditSub($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$ikm    = ItemKegiatanModel::find($id);
			$data['kegiatan']    = $ikm;
			$data['ide']         = $ide;
			$data['id_kegiatan'] = Helper::en($ikm->id_kegiatan);
			return View::make('adminlte.kegiatan.modal_edit_sub',$data);
		}
	}

	public function postEditSubItem($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$ikm = ItemKegiatanModel::find($id);
			$rules = array(
				'sub_kegiatan'     => 'required',
				'volume'           => 'required|numeric',
				'satuan'           => 'required',
				'realisasi_fisik'  => 'required|numeric',
				'anggaran_sppd'    => 'numeric',
				'anggaran_spj'     => 'numeric',
				'rencana_fisik'    => 'required|numeric',
				'rencana_anggaran' => 'required|numeric'
			);
			$valid = Validator::make(Input::all(),$rules);
			if($valid->fails())
			{
				return Redirect::to('panel/program/detail-kegiatan/'.Helper::en($ikm->id_kegiatan))->withErrors($valid)->withInput();
			}else{

				$IKM      = ItemKegiatanModel::where('id','!=',$id)->where('id_user',$this->admin['id'])->where('id_kegiatan',$ikm->id_kegiatan)->get();
				$ukm      = UserKegiatanModel::where('id_kegiatan',$ikm->id_kegiatan)->where('id_user',$this->admin['id'])->first();
				$paguAwal = (!empty($ukm->pagu_kegiatan_perubahan))?$ukm->pagu_kegiatan_perubahan:$ukm->pagu_kegiatan;
				$anggaran = Input::get('rencana_anggaran');
				if(count($IKM)>0){
					foreach ($IKM as $key => $value) {
						$AR[] = $value->anggaran_rencana;
					}
					$planning_anggaran = array_sum($AR)+$anggaran;

					if($planning_anggaran>$paguAwal){
						return Redirect::to('panel/program/detail-kegiatan/'.Helper::en($ikm->id_kegiatan))->with('program_alert','Total anggaran rencana tidak boleh melebihi Rp '.number_format($paguAwal,0,",","."));
					}
				}else{
					if($anggaran>$paguAwal){
						return Redirect::to('panel/program/detail-kegiatan/'.Helper::en($ikm->id_kegiatan))->with('program_alert','Total anggaran rencana tidak boleh melebihi Rp '.number_format($paguAwal,0,",","."));
					}
				}

				$km                   = ItemKegiatanModel::find($id);
				if (Input::hasFile('photo'))
				{
					
					$photo = Input::file('photo');
					$photoName = rand(111111,999999).'.'.$photo->getClientOriginalExtension();
				    $itkm = ItemKegiatanModel::where('photo', $photoName)->count();
				    while ($itkm) {
						$photoName = rand(111111,999999).'.'.$photo->getClientOriginalExtension();
					    $itkm = ItemKegiatanModel::where('photo', $photoName)->count();
				    }
				    if (!is_dir(public_path('assets/photo_bukti'))) {
				    	mkdir(public_path('assets/photo_bukti'), 0700);
				    }
					if (is_file(public_path('assets/photo_bukti/'.$ikm->photo))) {
						unlink(public_path('assets/photo_bukti/'.$ikm->photo));
					}
				    $photo->move(public_path('assets/photo_bukti'), $photoName);
					$km->photo       = $photoName;
				}
				$km->name             = Input::get('sub_kegiatan');
				$km->vol              = Input::get('volume');
				$km->satuan           = Input::get('satuan');
				$km->fisik_realisasi  = Input::get('realisasi_fisik');
				$km->anggaran_sppd    = Input::get('anggaran_sppd');
				$km->anggaran_spj     = Input::get('anggaran_spj');
				$km->fisik_rencana    = Input::get('rencana_fisik');
				$km->anggaran_rencana = Input::get('rencana_anggaran');
				$km->keterangan       = (Input::get('keterangan'))?Input::get('keterangan'):'';
				$km->save();

				$UM = UserKegiatanModel::where('id_kegiatan',$ikm->id_kegiatan)->where('id_user',$this->admin['id'])->first();
				Helper::insertCountRata($this->admin['id'],$UM->id_program,$ikm->id_kegiatan);
				return Redirect::to('panel/program/detail-kegiatan/'.Helper::en($km->id_kegiatan))->with('program','Data sub kegiatan telah perbarui');
			}
		}
	}

	public function postAddSubItem($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$rules = array(
				'bulan'            => 'required',
				'sub_kegiatan'     => 'required',
				'volume'           => 'required|numeric',
				'satuan'           => 'required',
				'rencana_fisik'    => 'required|numeric',
				'rencana_anggaran' => 'required|numeric'
			);
			$valid = Validator::make(Input::all(),$rules);
			if($valid->fails())
			{
				return Redirect::to('panel/program/detail-kegiatan/'.$ide)->withErrors($valid)->withInput();
			}else{
				$bulan = Input::get('bulan');
				$tahun = date('Y');
				$tahap = Helper::findTahap($bulan,$tahun);

				$ikm      = ItemKegiatanModel::where('id_user',$this->admin['id'])->where('id_kegiatan',$id)->get();
				$ukm      = UserKegiatanModel::where('id_kegiatan',$id)->where('id_user',$this->admin['id'])->first();
				$paguAwal = (!empty($ukm->pagu_kegiatan_perubahan))?$ukm->pagu_kegiatan_perubahan:$ukm->pagu_kegiatan;
				// $paguAwal = $ukm->pagu_kegiatan;
				$anggaran = Input::get('rencana_anggaran');
				if(count($ikm)>0){
					foreach ($ikm as $key => $value) {
						$AR[] = $value->anggaran_rencana;
					}
					$planning_anggaran = array_sum($AR)+$anggaran;
					

					if($planning_anggaran>$paguAwal){
						return Redirect::to('panel/program/detail-kegiatan/'.$ide)->with('program_alert','Total anggaran rencana tidak boleh melebihi Rp '.number_format($paguAwal,0,",","."));
					}
				}else{
					if($anggaran>$paguAwal){
						return Redirect::to('panel/program/detail-kegiatan/'.$ide)->with('program_alert','Total anggaran rencana tidak boleh melebihi Rp '.number_format($paguAwal,0,",","."));
					}
				}

				$km                   = new ItemKegiatanModel;
				if (Input::hasFile('photo'))
				{
					
					$photo = Input::file('photo');
					$photoName = rand(111111,999999).'.'.$photo->getClientOriginalExtension();
				    $itkm = ItemKegiatanModel::where('photo', $photoName)->count();
				    while ($itkm) {
						$photoName = rand(111111,999999).'.'.$photo->getClientOriginalExtension();
					    $itkm = ItemKegiatanModel::where('photo', $photoName)->count();
				    }
				    if (!is_dir(public_path('assets/photo_bukti'))) {
				    	mkdir(public_path('assets/photo_bukti'), 0700);
				    }
					// if (is_file(public_path('assets/photo_bukti/'.$ikm->photo))) {
					// 	unlink(public_path('assets/photo_bukti/'.$ikm->photo));
					// }
				    $photo->move(public_path('assets/photo_bukti'), $photoName);
					$km->photo       = $photoName;
				}

				$km->id_user          = $this->admin['id'];
				$km->id_kegiatan      = $id;
				$km->id_detail_tahap  = $tahap->id;
				$km->name             = Input::get('sub_kegiatan');
				$km->vol              = Input::get('volume');
				$km->satuan           = Input::get('satuan');
				$km->fisik_rencana    = Input::get('rencana_fisik');
				$km->anggaran_rencana = Input::get('rencana_anggaran');
				$km->keterangan       = (Input::get('keterangan'))?Input::get('keterangan'):'';
				$km->save();

				$UM = UserKegiatanModel::where('id_kegiatan',$id)->where('id_user',$this->admin['id'])->first();
				Helper::insertCountRata($this->admin['id'],$UM->id_program,$id);
				return Redirect::to('panel/program/detail-kegiatan/'.$ide)->with('program','Data sub kegiatan telah ditambah');
			}
		}
	}
	public function getDeleteItem($ide,$kegiatan)
	{
		$id          = Helper::de($ide);
		$id_kegiatan = Helper::de($kegiatan);
		if(!empty($id)){
			$ukm = UserKegiatanModel::where('id_kegiatan',$id_kegiatan)->where('id_user',$this->admin['id'])->first();
			$pm  = ItemKegiatanModel::find($id)->delete();
			Helper::insertCountRata($this->admin['id'],$ukm->id_program,$id_kegiatan);
			return Redirect::to('panel/program/detail-kegiatan/'.$kegiatan)->with('program','Data telah dihapus');
		}
	} 
}