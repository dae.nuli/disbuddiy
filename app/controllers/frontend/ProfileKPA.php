<?php

class ProfileKPA extends BaseController {
	public $limit = 10;
	public $ip;
	public $admin;

	public function __construct()
	{
		 $this->ip = $_SERVER['REMOTE_ADDR'];
		 $this->admin = Session::get('kpaUsers');
	}

	public function getIndex()
	{
		View::share('title','Profil');
		View::share('path','Profil');
		$data['profile'] = KPAModel::find($this->admin['id']);
		return View::make('adminlte.profile.index',$data);
	}

	public function postUpdate()
	{
		$rules = array(
			'nama_penanggung_jawab' => 'required',
			'nomor_telefon'         => 'required|numeric',
			'alamat'                => 'required',
			'password'              => 'min:5',
			'ulangi_password'       => 'same:password'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('panel/profile')->withErrors($valid)->withInput();
		}else{
			$pass = Input::get('password');
			$km                = KPAModel::find($this->admin['id']);
			$km->phone         = Input::get('nomor_telefon');
			$km->address       = Input::get('alamat');
			$km->personil_name = Input::get('nama_penanggung_jawab');
			if(!empty($pass)){
				$km->password = sha1(Input::get('password').'sha1');
			}
			$km->save();
			return Redirect::to('panel/profile')->with('profile','Profil telah diperbarui');
		}
	}

	public function postIndex()
	{
		$rules = array(
			'nama_penanggung_jawab' => 'required',
			'alamat_email'          => 'required|email|unique:kpa_users,email_staff',
			'nomor_telefon'         => 'required|numeric',
			'alamat'                => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('panel/home')->withErrors($valid)->withInput();
		}else{
			$random =	Str::random(20);
			$nm     =	KPAModel::where('activation',$random)->count();
			while ($nm > 0) {
				$random =	Str::random(20);
				$nm     =	KPAModel::where('activation',$random)->count();
			}
			$km                = KPAModel::find($this->admin['id']);
			$km->email_staff   = Input::get('alamat_email');
			$km->phone         = Input::get('nomor_telefon');
			$km->address       = Input::get('alamat');
			$km->personil_name = Input::get('nama_penanggung_jawab');
			$km->is_complete   = 2;
			// $km->is_complete   = 1;
			$km->activation    = $random;
			$km->save();

			// Mail::queue('adminlte.login.aktivasi',$data = array('code' => $random),function($message) use ($km){
			// 	$message->to($km->email_staff,$km->instansi_name)->subject('Activation');
			// });
			return Redirect::to('panel/home')->with('profile_success','Silahkan cek email anda untuk mengaktifkan akun');
		}
	}

	public function postResend()
	{
		$rules = array(
			'alamat_email' => 'required|email|unique:kpa_users,email_staff,'.$this->admin['id']
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('panel/home')->withErrors($valid);
		}else{
			$random =	Str::random(20);
			$nm     =	KPAModel::where('activation',$random)->count();
			while ($nm > 0) {
				$random =	Str::random(20);
				$nm     =	KPAModel::where('activation',$random)->count();
			}
			
			$km                = KPAModel::find($this->admin['id']);
			$km->email_staff   = Input::get('alamat_email');
			$km->activation    = $random;
			$km->save();
			// Mail::queue('adminlte.login.aktivasi',$data = array('code' => $random),function($message) use ($km){
			// 	$message->to($km->email_staff,$km->instansi_name)->subject('Activation');
			// });
			return Redirect::to('panel/home')->with('profile_success','Silahkan cek email anda untuk mengaktifkan akun');
		}
	}
}