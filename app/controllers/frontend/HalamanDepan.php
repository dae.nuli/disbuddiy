<?php

class HalamanDepan extends BaseController {
	public $limit = 10;
	public $ip;
	public $admin;

	public function __construct()
	{
		 $this->ip = $_SERVER['REMOTE_ADDR'];
		 $this->admin = Session::get('kpaUsers');
	}

	public function index()
	{
		View::share('title','Home');
		return View::make('adminlte.login.login');
	}

	public function auth()
	{
		View::share('title','Login');
		return View::make('adminlte.login.login');
	}

	public function postAuth()
	{
		// return View::make('backend.login');
		$rules = array(
			'username' => 'required',
			'password' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			
			return Redirect::to('auth')->withErrors($valid);

		}else{

			$um = KPAModel::where('username',Input::get('username'))
				  ->where('password',sha1(Input::get('password').'sha1'))
				  ->where('deleted_at',null)
				  ->first();
			if(count($um) > 0)
			{
			  	if($um->is_active==1){
				  	$data = array(
						'id'         => $um->id,
						'code'       => $um->code_kpa,
						'instansi'   => $um->instansi_name,
						'complete'   => $um->is_complete,
						'created_at' => $um->created_at
				  		);
					Session::put('kpaUsers',$data);
					return Redirect::to('panel/home')->with('valid','Welcome to dashboard');
				}else{
				  	return Redirect::to('auth')->with('error_login','your account is not active');
				}
			}else{
			  	return Redirect::to('auth')->with('error_login','your account is not valid');
			}

		}
	}
	
	// Panel After KTA Login
	public function getIndex()
	{
		View::share('title','Dashboard');
		View::share('path','');
		// $data['limit'] = $this->limit;
		// $cari          = Input::get('search');
		// $filter        = Input::get('filter');
		// $tahun         = Input::get('tahun');
		// $year          = date('Y');
		// $firstYear     = UserProgramModel::join('program', 'user_program.id_program','=','program.id')
		// 					->where('user_program.id_user',$this->admin['id'])
		// 					->where('program.year',$year)
		// 					->where('deleted_at',null)
		// 					->orderBy('program.year','asc')->take(1)->first();
		// $data['tahun']   = range($firstYear->year,date('Y')+2);
		$cari          = Input::get('search');
		
		$year          = (Session::get('user_year'))?Session::get('user_year'):date('Y');
		$data['limit'] = $this->limit;
		// $year       = date('Y');
		$upm           = UserProgramModel::join('program', 'user_program.id_program','=','program.id')
						->where('user_program.id_user',$this->admin['id'])
						->where('program.year',$year)
						->where('program.deleted_at',null)
						->where('user_program.deleted_at',null);
		if($cari){
			$upm = $upm->where('program.name','LIKE',"%$cari%");
		}
		$firstYear         = UserProgramModel::join('program', 'user_program.id_program','=','program.id')
							->where('user_program.id_user',$this->admin['id'])
							->where('program.deleted_at',null)
							->where('user_program.deleted_at',null)
							->where('program.year','<=',date('Y'))
							->orderBy('program.year','asc')
							->groupBy('program.year')->first();
							// ->groupBy('program.year')->get();
		$data['year']      = $year;
		$data['yearRange'] = range(!empty($firstYear->year)? $firstYear->year : date('Y'), date('Y'));
		// $data['yearRange'] = range($firstYear->year, date('Y'));
		// $data['yearRange'] = $firstYear;
		$data['program']   = $upm->get();
		$data['kpa']       = $this->admin['id'];
		return View::make('adminlte.dashboard.index',$data);
	}

	public function postIndex()
	{
		$year = Input::get('year');
		if($year){
			Session::put('user_year',$year);
		}
		return Redirect::to('panel/home');
	}
	public function getDetail($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$pm = ProgramModel::find($id);
			View::share('title',$pm->name);
			View::share('path','');
			$data['program']  = $pm;
			$data['kegiatan'] = KegiatanModel::where('id_program',$id)->get();
			$data['idUser']   = $this->admin['id'];
			return View::make('adminlte.kegiatan.index',$data);
		}
	}

	public function getDetailKegiatan($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$data['kegiatan'] = KegiatanModel::find($id);
			$data['program']  = ProgramModel::find($data['kegiatan']->id_program);
			View::share('title',$data['program']->name);
			View::share('path','');
			$data['item'] = ItemKegiatanModel::where('id_kegiatan',$id)->where('id_user',$this->admin['id'])->orderBy('id_detail_tahap','asc')->get();
			return View::make('adminlte.kegiatan.detail',$data);
		}
	}

	public function getModalAddSub($id)
	{
		$data['kegiatan'] = $id;
		return View::make('adminlte.kegiatan.modal_add_sub',$data);
	}

	public function getModalEditSub($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$data['kegiatan'] = ItemKegiatanModel::find($id);
			$data['ide']      = $ide;
			return View::make('adminlte.kegiatan.modal_edit_sub',$data);
		}
	}

	public function postEditSubItem($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$ikm = ItemKegiatanModel::find($id);
			$rules = array(
				'realisasi_fisik' => 'required|numeric',
				'anggaran_sppd'   => 'numeric',
				'anggaran_spj'    => 'numeric'
			);
			$valid = Validator::make(Input::all(),$rules);
			if($valid->fails())
			{
				return Redirect::to('panel/home/detail-kegiatan/'.Helper::en($ikm->id_kegiatan))->withErrors($valid)->withInput();
			}else{
				$km                  = ItemKegiatanModel::find($id);
				$km->fisik_realisasi = Input::get('realisasi_fisik');
				$km->anggaran_sppd   = Input::get('anggaran_sppd');
				$km->anggaran_spj    = Input::get('anggaran_spj');
				$km->keterangan      = (Input::get('keterangan'))?Input::get('keterangan'):'';
				$km->save();

				return Redirect::to('panel/home/detail-kegiatan/'.Helper::en($km->id_kegiatan))->with('program','Data sub kegiatan telah perbarui');
			}
		}
	}

	public function postAddSubItem($ide)
	{
		$id = Helper::de($ide);
		if(!empty($id)){
			$rules = array(
				'bulan'            => 'required',
				'sub_kegiatan'     => 'required',
				'volume'           => 'required|numeric',
				'satuan'           => 'required',
				'rencana_fisik'    => 'required|numeric',
				'rencana_anggaran' => 'required|numeric'
			);
			$valid = Validator::make(Input::all(),$rules);
			if($valid->fails())
			{
				return Redirect::to('panel/home/detail-kegiatan/'.$ide)->withErrors($valid)->withInput();
			}else{
				$bulan = Input::get('bulan');
				$tahun = date('Y');
				$tahap = Helper::findTahap($bulan,$tahun);

				$km                   = new ItemKegiatanModel;
				$km->id_user          = $this->admin['id'];
				$km->id_kegiatan      = $id;
				$km->id_detail_tahap  = $tahap->id;
				$km->name             = Input::get('sub_kegiatan');
				$km->vol              = Input::get('volume');
				$km->satuan           = Input::get('satuan');
				$km->fisik_rencana    = Input::get('rencana_fisik');
				$km->anggaran_rencana = Input::get('rencana_anggaran');
				$km->keterangan       = (Input::get('keterangan'))?Input::get('keterangan'):'';
				$km->save();

				return Redirect::to('panel/home/detail-kegiatan/'.$ide)->with('program','Data sub kegiatan telah ditambah');
			}
		}
	}
	public function activate($code)
	{
		if(!empty($code))
		{
			$km = KPAModel::where('activation',$code)->first();
			if(count($km)>0){
				$km->is_complete = 2;
				$km->save();
				return Redirect::to('auth')->with('akun_aktif','Silahkan login, akun anda sudah aktif.');
			}
		}
	}



	// Panel After KTA Login

	public function register()
	{
		View::share('title','Register');
		return View::make('eshopper.register.index');
	}

	public function postRegister()
	{
		$rules = array(
			'nama_instansi'   => 'required',
			'bidang'          => 'required',
			'nama_personil'   => 'required',
			'nomor_telepon'   => 'required|numeric',
			'alamat_email'    => 'required|email|unique:kpa_users,email',
			'alamat_kantor'   => 'required',
			'password'        => 'required|min:5',
			'ulangi_password' => 'required|same:password'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('register')->withErrors($valid)->withInput();
		}else{
			$um                = new KPAModel;
			$um->instansi_name = Input::get('nama_instansi');
			$um->email         = Input::get('alamat_email');
			$um->phone         = Input::get('nomor_telepon');
			$um->address       = Input::get('alamat_kantor');
			$um->bidang        = Input::get('bidang');
			$um->personil_name = Input::get('nama_personil');
			$um->password      = sha1(Input::get('password').'sha1');
			$um->save();

			return Redirect::to('register')->with('register','Terimakasih sudah melakukan registrasi');
		}
	}

	public function logout()
	{
		Session::flush();
		return Redirect::to('auth');
	}


	// public function how()
	// {
	// 	View::share('title','Cara Pemesanan');
	// 	$data['how'] = HowToBuyModel::find(1);
	// 	$data['description'] = AboutModel::find(1)->how_to_buy_description;
	// 	return View::make('front.how.index',$data);
	// }

	// public function contact()
	// {
	// 	View::share('title','Hubungi Kami');
	// 	$data['description'] = AboutModel::find(1);
	// 	return View::make('eshopper.contact.index',$data);
	// }

 
	public function not()
	{
		return View::make('front.404');
	}
}