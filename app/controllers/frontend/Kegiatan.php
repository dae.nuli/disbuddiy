<?php

class Kegiatan extends BaseController {
	public $limit = 10;
	public $ip;
	public $admin;

	public function __construct()
	{
		 $this->admin = Session::get('kpaUsers');
	}

	public function getIndex($idProgram=null,$idKegiatan=null)
	{
		// return's';
		View::share('title','Kegiatan');
		if(!empty($idProgram)){
			$pm = ProgramModel::where('id_user_kpa',$this->admin['id'])
					->where('id',$idProgram)
					->first();
			if(count($pm)>0){
				if(!empty($idKegiatan)){
					$data['limit']       = $this->limit;
					$data['kegiatan']    = KegiatanModel::where('id_program',$idProgram)->orderBy('id','desc')->paginate($this->limit);
					$data['programName'] = $pm->name;
					$data['idProgram']   = $idProgram;
					$data['idUser']      = $this->admin['id'];
					return View::make('eshopper.panel.kegiatan.index',$data);
				}
			}
		}
	}

}