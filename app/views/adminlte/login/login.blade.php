
<?php $about = Helper::about(); ?>
<!DOCTYPE html>
<html class="latar">
  <head>
    <meta charset="UTF-8">
    <title>{{isset($about->name)?$about->name:'Administrator'}} | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{asset('adminlte2/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{asset('adminlte2/font-awesome-4.4.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset('adminlte2/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{asset('adminlte2/plugins/iCheck/square/blue.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="{{URL::to('auth')}}"><b>Monev</b>DAIS</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Halaman login untuk PA/KPA</p>
        @if(Session::has('akun_aktif'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4>  <i class="icon fa fa-check"></i> Selamat!</h4>
          {{Session::get('akun_aktif')}}
        </div>
        @endif
        <form action="{{URL::to('auth')}}" method="post" id="request">
          <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="Username" data-validation="required" data-validation-error-msg="Form username harus diisi." data-validation-event="keyup"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            {{$errors->first('username','<p class="text-red">:message</p>')}}
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off" data-validation="required" data-validation-error-msg="Form password harus diisi." data-validation-event="keyup"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            {{$errors->first('password','<p class="text-red">:message</p>')}}
          </div>
          <div class="row">
            <div class="col-xs-8">    
              {{-- <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>   --}}                      
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat waiting">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

{{--         <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div> --}}<!-- /.social-auth-links -->

        <a href="#" style="color:#000">Lupa Password ?</a><br>
        <a href="#" style="color:#000" class="text-center">Silahkan Hubungi Administrator</a>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{asset('adminlte2/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{asset('adminlte2/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="{{asset('adminlte2/plugins/iCheck/icheck.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          $('.waiting').addClass('disabled');
       }
    });
    </script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
