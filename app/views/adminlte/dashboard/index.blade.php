@extends('adminlte.layouts.content')

@section('body-content')

@if(Session::has('profile_success'))

{{-- <div class="alert callout callout-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  {{Session::get('profile_success')}}.
</div> --}}
@endif

@if(Helper::IsComplete()==0)
<div class="callout callout-danger">
  	<h4>Warning!</h4>
  	<p>Silahkan lengkapi data profil anda, agar sistem ini bisa digunakan.</p>
</div>
<div class="box box-default">
  	<div class="box-header with-border">
    	<h3 class="box-title">Profil KPA</h3>
  	</div>
  	<div class="box-body">
    	<form action="{{URL::to('panel/profile')}}" method="POST" id="request">
          <div class="box-body lengkapi">
            <div class="form-group">
            	<div class="row">
                <div class="col-xs-12">
  		              <label for="nama">Nama Penanggung Jawab</label>
  		              <input type="text" name="nama_penanggung_jawab" value="{{Input::old('nama_penanggung_jawab')}}" class="form-control" autocomplete="off" id="nama" data-validation="required" data-validation-error-msg="Form nama penanggung jawab harus diisi." data-validation-event="keyup">
                    {{$errors->first('nama_penanggung_jawab','<p class="text-red">:message</p>')}}
  		          </div>
  		      </div>
            </div>
            <div class="form-group">
            	<div class="row">
                  <div class="col-xs-12">
  		              <label for="email">Alamat Email</label>
  		              <input type="email" name="alamat_email" value="{{Input::old('alamat_email')}}" class="form-control" autocomplete="off" id="email" data-validation="email" data-validation-error-msg="Format alamat email salah." data-validation-event="keyup">
                    {{$errors->first('alamat_email','<p class="text-red">:message</p>')}}
  		            </div>
	            </div>
            </div>
            <div class="form-group">
            	<div class="row">
                  <div class="col-xs-12">
  		              <label for="nomor">Nomor Telefon</label>
  		              <input type="text" name="nomor_telefon" value="{{Input::old('nomor_telefon')}}" class="form-control" autocomplete="off" id="nomor" data-validation="number" data-validation-error-msg="Form nomor telefon harus diisi dengan angka." data-validation-event="keyup">
                    {{$errors->first('nomor_telefon','<p class="text-red">:message</p>')}}
  		            </div>
  		        </div>
            </div>
            <div class="form-group">
            	<div class="row">
                <div class="col-xs-12">
		              <label for="alamat">Alamat</label>
		              <textarea class="form-control" name="alamat" id="alamat" rows="4" data-validation="required" data-validation-error-msg="Form alamat harus diisi." data-validation-event="keyup">{{Input::old('alamat')}}</textarea>
                  {{$errors->first('alamat','<p class="text-red">:message</p>')}}
		            </div>
	            </div>
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer lengkapi">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
  	</div><!-- /.box-body -->
</div><!-- /.box -->
@elseif(Helper::IsComplete()==1)
@if(count($errors))
<div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
  @foreach ($errors->all('<li>:message</li>') as $message)
  {{$message}}
  @endforeach
</div>
@endif
  <div class="callout callout-info">
      <h4>Info</h4>
      <p>Silahkan cek email anda untuk mengaktifkan akun.</p>
  </div>
  <div class="box box-default collapsed-box">
    <div class="box-header with-border">
      Apabila email aktifasi belum masuk, silahkan <a href="#" class="btn btn-primary btn-xs" data-widget="collapse">kirim ulang</a>
      {{-- Apabila email aktifasi belum masuk, silahkan <a href="{{URL::to('panel/profile')}}" class="btn btn-primary btn-xs" data-widget="collapse">kirim ulang</a> --}}
    </div>
    <div class="box-body" style="display:none">
      <form action="{{URL::to('panel/profile/resend')}}" method="POST" id="request">
          <div class="box-body lengkapi"> 
            <div class="form-group">
              <div class="row">
                    <div class="col-xs-12">
                  <label for="email">Alamat Email</label>
                  <input type="email" name="alamat_email" class="form-control" autocomplete="off" id="email" data-validation="email" data-validation-error-msg="Format alamat email salah." data-validation-event="keyup">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer lengkapi">
            <button type="submit" class="btn btn-primary">Kirim</button>
          </div>
        </form>
    </div>
  </div>
@else
{{-- Dashboard Filter --}}
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
               {{Form::open(array('url' => 'panel/program', 'class'=>'pull-left', 'method' => 'post'))}}
               <select class="form-control tahun" style="padding:0;" name="year" onchange="this.form.submit()">
               @foreach($yearRange as $row)
                   <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
                   {{-- <option value="{{$row->year}}" {{($year==$row->year) ? 'selected' : ''}}>{{$row->year}}</option> --}}
               @endforeach
               </select>
               {{Form::close()}}
               {{Form::open(array('url'=>'panel/program', 'method'=>'GET'))}}
               <?php $search = Input::get('search'); ?>
               <div class="input-group">
                  <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Cari Program" autocomplete="off">
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
               </div>
               {{Form::close()}}
         </div><!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
               <tbody>
                  <tr>
                     <th>Kode Program</th>
                     <th>Nama Program</th>
                     <th>Jumlah Kegiatan</th>
                     <th>Tahun</th>
                     <th></th>
                  </tr>
                  @foreach($program as $row)
                  <?php $totalProgram = Helper::UserKegiatan($row->id_program,$kpa); ?>
                  <tr>
                     <td>{{$row->program->code_program}}</td>
                     <td>
                        <a href="{{URL::to('panel/program/detail/'.Helper::en($row->id_program))}}">{{$row->program->name}}</a>
                     </td>
                     <td>{{$totalProgram}}</td>
                     <td>{{$row->program->year}}</td>
                     <td>
                        <a href="{{URL::to('panel/program/detail/'.Helper::en($row->id_program))}}" class="btn btn-success btn-xs"><i class="fa fa-eye"></i> Detail</a>
                        <a class="btn btn-info btn-xs please-waiting print-program" data-program="{{Helper::en($row->id_program)}}"><i class="fa fa-print"></i> Print</a>
                     </td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
         </div><!-- /.box-body -->
         <div class="box-footer">
         </div>
      </div><!-- /.box -->
   </div>
</div>
<div id="open-modal"></div>
@endif

@stop

@section('end-script')
    @parent
    <script type="text/javascript" src="{{asset('assets/js/AdminLTE/jquery.blockUI.js')}}"></script>
    <script src="{{asset('assets/js/Chart.js')}}"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script src="{{ asset('assets/js/AdminLTE/custom.js')}}" type="text/javascript"></script>
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>
    <script type="text/javascript">
     $.validate({
        form : '#request',
        onSuccess : function() {
          waiting();
        }
    });
    $(function() {
        $('.tahun, .filter').select2({
            theme: "classic"
        });
    });
 
    </script>
@stop