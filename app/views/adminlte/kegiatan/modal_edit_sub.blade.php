<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$('.rencana-fisik').on('keyup',function (e){
   var nilai = $(this).val();
   $('.realisasi-fisik').attr('data-validation-allowing',"range[1;"+nilai+"]");
   console.log('keyup');
});
$('.rencana-anggaran').on('keyup',function (e){
   var nilai = $(this).val();
   $('.anggaran-sppd').attr('data-validation-allowing',"range[1;"+nilai+"]");
   $('.anggaran-spj').attr('data-validation-allowing',"range[1;"+nilai+"]");
   console.log('keyup');
});


$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog custom-width">
      <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
           <h4 class="modal-title">Tambah Sub Kegiatan</h4>
         </div>
          {{Form::open(array('url'=>'panel/program/edit-sub-item/'.$ide, 'id'=>'request', 'method'=>'POST', 'files'=>true))}}
         {{-- <form action="{{URL::to('panel/program/edit-sub-item/'.$ide)}}" method="POST"> --}}
         <div class="modal-body">
               <div class="box-body">
                     <div class="row">
                         <div class="col-xs-4">
                              <div class="form-group">
                                  <label>Sub Kegiatan</label>
                                 <input name="sub_kegiatan" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="Form sub kegiatan harus diisi." placeholder="Sub Kegiatan" data-validation-event="keyup" value="{{$kegiatan->name}}">
                                 {{-- <input disabled="" class="form-control" autocomplete="off" value="{{$kegiatan->name}}"> --}}
                             </div>
                         </div>
                         <div class="col-xs-2">
                              <div class="form-group">
                                  <label>Volume</label>
                                 <input name="volume" class="form-control" autocomplete="off" data-validation="number" data-validation-error-msg="Form volume harus diisi dengan angka." placeholder="Volume" data-validation-event="keyup" value="{{$kegiatan->vol}}">
                             </div>
                         </div>
                         <div class="col-xs-2">
                              <div class="form-group">
                                  <label>Satuan</label>
                                 <input name="satuan" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="Form satuan harus diisi." placeholder="Satuan" data-validation-event="keyup" value="{{$kegiatan->satuan}}">
                             </div>
                         </div>
                         <div class="col-xs-2">
                              <div class="form-group">
                                    <label>Rencana Fisik</label>
                                   <input name="rencana_fisik" class="form-control rencana-fisik" autocomplete="off" data-validation="number" data-validation-error-msg="Form rencana fisik harus diisi dengan angka." placeholder="Rencana Fisik (%)" data-validation-event="keyup" value="{{$kegiatan->fisik_rencana}}">
                                   {{-- <input disabled="" class="form-control" autocomplete="off" value="{{$kegiatan->fisik_rencana}}"> --}}
                               </div>
                         </div>
                         <div class="col-xs-2">
                              <div class="form-group">
                                    <label>Realisasi Fisik</label>
                                    <input name="realisasi_fisik" class="form-control realisasi-fisik" autocomplete="off" data-validation="number" data-validation-error-msg="Form realisasi fisik harus diisi dengan angka dan tidak melebihi rencana fisik." value="{{$kegiatan->fisik_realisasi}}" placeholder="Realisasi Fisik (%)" data-validation-allowing="range[1;{{$kegiatan->fisik_rencana}}]" data-validation-event="keyup" data-validation-optional="true">
                                    {{-- <input name="realisasi_fisik" class="form-control" autocomplete="off" data-validation="number" data-validation-error-msg="Form realisasi fisik harus diisi dengan angka dan tidak melebihi rencana fisik." value="{{$kegiatan->fisik_realisasi}}" placeholder="Realisasi Fisik (%)" data-validation-allowing="range[1;{{$kegiatan->fisik_rencana}}]" data-validation-event="keyup"> --}}
                               </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Rencana Anggaran</label>
                                 <input name="rencana_anggaran" class="form-control rencana-anggaran" autocomplete="off" data-validation="number" data-validation-error-msg="Form rencana anggaran harus diisi dengan angka." placeholder="Rencana Anggaran" data-validation-event="keyup" value="{{$kegiatan->anggaran_rencana}}">
                                 {{-- <input disabled="" class="form-control" placeholder="Rencana Anggaran" value="{{$kegiatan->anggaran_rencana}}"> --}}
                            </div>
                         </div>
                         <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Anggaran SP2D</label>
                                 <input name="anggaran_sppd" autocomplete="off" class="form-control anggaran-sppd" value="{{$kegiatan->anggaran_sppd}}" placeholder="Anggaran SP2D" data-validation="number" data-validation-error-msg="Form anggaran sp2d harus diisi dengan angka dan tidak melebihi rencana anggaran." data-validation-optional="true" data-validation-allowing="range[1;{{$kegiatan->anggaran_rencana}}]" data-validation-event="keyup">
                              </div>
                         </div>
                         <div class="col-xs-4">
                              <div class="form-group">
                                    <label>Anggaran SPJ</label>
                                    <input name="anggaran_spj" autocomplete="off" class="form-control anggaran-spj" value="{{$kegiatan->anggaran_spj}}" placeholder="Anggaran SPJ" data-validation="number" data-validation-error-msg="Form anggaran spj harus diisi dengan angka dan tidak melebihi rencana anggaran." data-validation-optional="true" data-validation-allowing="range[1;{{$kegiatan->anggaran_rencana}}]" data-validation-event="keyup" data-validation-event="keyup">
                              </div>
                         </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                              <div class="form-group">
                                 <label>Keterangan</label>
                                 <input type="text" name="keterangan" autocomplete="off" class="form-control" value="{{$kegiatan->keterangan}}" placeholder="Keterangan">
                              </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="form-group">
                              <label>Foto</label>
                              <input type="file" name="photo" autocomplete="off" class="form-control">
                           </div>
                        </div>
                     </div>
               </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default close-waiting" data-dismiss="modal">Tutup</button>
           <button type="submit" class="btn btn-primary">Submit</button>
         </div>
        {{Form::close()}}
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div>