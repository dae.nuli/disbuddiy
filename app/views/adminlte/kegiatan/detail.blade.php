@extends('adminlte.layouts.content')

@section('header-content')
<?php
if(!empty($new->pagu_kegiatan_perubahan)){
  $pagu = $new->pagu_kegiatan_perubahan;
}else{
  $pagu = $new->pagu_kegiatan;
}
?>
<div class="row">
   <div class="col-xs-12">
      <div class="info-box bg-aqua">
         <span class="info-box-icon"><i class="ion ion-clipboard"></i></span>
         <div class="info-box-content">
            <span class="info-box-text">{{$program->year}} / 3.17.{{$user}}.{{$program->code_program}} / {{$program->name}}</span>
            <span class="info-box-number">3.17.{{$user}}.{{$program->code_program}}.{{$kegiatan->code_kegiatan}} / {{$kegiatan->name}} / Rp {{number_format($pagu,0,",",".")}}</span>
            {{-- <div class="progress">
               <div class="progress-bar" style="width: 50%"></div>
            </div> --}}
            <span class="progress-description">
              {{$kegiatan->description}}
            </span>
         </div><!-- /.info-box-content -->
      </div>
   </div>
</div>
@stop

@section('body-content')
@if(Session::has('program_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('program_alert')}}.
    </div>
@endif
<div class="row">
<div class="col-xs-12">
@if(count($errors))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b>
        @foreach ($errors->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </div>
@endif
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><!--{{$kegiatan->name}}--></h3>
    <div class="box-tools">
      <div class="input-group">
        <a href="{{URL::to('#')}}" class="btn btn-primary waiting tambah-sub" data-kegiatan="{{Helper::en($kegiatan->id)}}">Tambah Sub</a>
      </div>
    </div>
  </div><!-- /.box-header -->
  <div class="box-body table-responsive no-padding">
   <table class="table table-bordered table-hover item-table">
      <thead style="background-color: #B9B9B9;">
      	<tr>
            <th rowspan="2">Tahap</th>
            <th rowspan="2">Bulan</th>
            <th rowspan="2">Sub Kegiatan</th>
            <th rowspan="2">Volume</th>
            <th rowspan="2">Satuan</th>
            <th colspan="2">Fisik</th>
            <th colspan="3">Anggaran</th>
            <th rowspan="2">Keterangan</th>
            <th rowspan="2">Bukti</th>
            <th rowspan="2">Progress</th>
            <th rowspan="2"></th>
         </tr>
         <tr>
            <th>Rencana (%)</th>
            <th>Realisasi (%)</th>
            <th>Rencana</th>
            <th>SP2D</th>
            <th>SPJ</th>
         </tr>
      </thead>
      <tbody>
        <?php
         $ra[]   = '';
         $sp2d[] = '';
         $spj[]  = '';
         ?>
         @foreach($item as $row)
         <?php
          if(!empty($row->fisik_rencana) && !empty($row->fisik_realisasi)){
            $FR[] = floor(($row->fisik_realisasi/$row->fisik_rencana)*100);
          }else{
            $FR[] = 0;
          }

          if(!empty($row->anggaran_sppd) && !empty($row->anggaran_rencana)){
            $AS[] = floor(($row->anggaran_sppd/$row->anggaran_rencana)*100);
          }else{
            $AS[] = 0;
          }

          if(!empty($row->anggaran_spj) && !empty($row->anggaran_rencana)){
            $AJ[] = floor(($row->anggaran_spj/$row->anggaran_rencana)*100);
          }else{
            $AJ[] = 0;
          }

          $prosentase_fisik = ((!empty($row->fisik_rencana) && !empty($row->fisik_realisasi)))?floor(($row->fisik_realisasi/$row->fisik_rencana)*100):'';
          $prosentase_sppd  = ((!empty($row->anggaran_sppd) && !empty($row->anggaran_rencana)))?floor(($row->anggaran_sppd/$row->anggaran_rencana)*100):'';
          $prosentase_spj   = ((!empty($row->anggaran_spj) && !empty($row->anggaran_rencana)))?floor(($row->anggaran_spj/$row->anggaran_rencana)*100):'';

          $ra[]   = ($row->anggaran_rencana?$row->anggaran_rencana:0);
          $sp2d[] = ($row->anggaran_sppd?$row->anggaran_sppd:0);
          $spj[]  = ($row->anggaran_spj?$row->anggaran_spj:0);
          $BULAN  = date('n');
          ?>
         <tr {{( (($row->tahap->month<$BULAN) && ($row->fisik_realisasi=="")) || (($row->tahap->month<$BULAN) && ($row->anggaran_sppd=="")) || (($row->tahap->month<$BULAN) && ($row->anggaran_spj=="")) )?'class="red-row"':''}}>
            <td>Tahap {{$row->tahap->id_tahap}}</td>
            <td>{{Helper::Month($row->tahap->month)}}</td>
            <td>{{($row->name?$row->name:'-')}}</td>
            <td>{{($row->vol?$row->vol:'-')}}</td>
            <td>{{($row->satuan?$row->satuan:'-')}}</td>
            <td>{{($row->fisik_rencana?$row->fisik_rencana:'-')}}</td>
            <td>{{($row->fisik_realisasi?$row->fisik_realisasi:'-')}}</td>
            <td>{{($row->anggaran_rencana?'Rp '.number_format($row->anggaran_rencana,0,",","."):'-')}}</td>
            <td>{{($row->anggaran_sppd?'Rp '.number_format($row->anggaran_sppd,0,",","."):'-')}}</td>
            <td>{{($row->anggaran_spj?'Rp '.number_format($row->anggaran_spj,0,",","."):'-')}}</td>
            <td>{{($row->keterangan?$row->keterangan:'-')}}</td>
            <td>{{($row->photo)?$row->photo:'-'}}</td>
            <td>
              {{(!empty($row->fisik_rencana) && !empty($row->fisik_realisasi))?'<span class="badge bg-green" data-toggle="tooltip" data-html="true" data-placement="top" title="FISIK">'.$prosentase_fisik.'%</span>':''}} 
              
              {{(!empty($row->anggaran_sppd)&&!empty($row->anggaran_rencana))?'<span class="badge bg-yellow" data-toggle="tooltip" data-html="true" data-placement="top" title="SP2D">'.$prosentase_sppd.'%</span>':''}}
              
              {{(!empty($row->anggaran_spj)&&!empty($row->anggaran_rencana))?'<span class="badge bg-light-blue" data-toggle="tooltip" data-html="true" data-placement="top" title="SPJ">'.$prosentase_spj.'%</span>':''}}
              
              {{(!empty($prosentase_sppd)&&!empty($prosentase_spj))?'<span class="badge bg-red" data-toggle="tooltip" data-html="true" data-placement="top" title="SP2D dan SPJ">'.abs($prosentase_sppd-$prosentase_spj).'%</span>':''}}
            </td>
            <td>
              <a data-sub="{{Helper::en($row->id)}}" class="btn btn-info btn-xs edit-item"><i class="fa fa-fw fa-edit"></i> Ubah</a>
              <a href="{{URL::to('panel/program/delete-item/'.Helper::en($row->id).'/'.$id_kegiatan)}}" class="btn btn-danger btn-xs please-waiting"><i class="fa fa-fw fa-trash-o"></i> Hapus</a>
            </td>
         </tr>
         @endforeach
      </tbody>
      <tfoot style="background-color: darkgray;">
        <tr>
          <th style="text-align:center">TOTAL</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          @if(array_sum($ra)<=$pagu)
            <th>Rp {{(count($ra)>0)?number_format(array_sum($ra),0,",","."):'0'}}</th>
          @else
            <th>Rp {{(count($ra)>0)?'<span class="text-red">'.number_format(array_sum($ra),0,",",".").'</span>':'0'}}</th>
          @endif
          <th>Rp {{(count($sp2d)>0)?number_format(array_sum($sp2d),0,",","."):'0'}}</th>
          <th>Rp {{(count($spj)>0)?number_format(array_sum($spj),0,",","."):'0'}}</th>
          <th></th>
          <th></th>
          <th>
            <span class="badge bg-green">{{(isset($FR))?ceil(array_sum($FR)/count($item)):''}}</span> 
            <span class="badge bg-yellow">{{(isset($AS))?ceil(array_sum($AS)/count($item)):''}}</span> 
            <span class="badge bg-light-blue">{{(isset($AJ))?ceil(array_sum($AJ)/count($item)):''}}</span> 
          </th>
          <th></th>
        </tr>
      </tfoot>
   </table>
  </div><!-- /.box-body -->
  <div class="box-footer">
      <a href="{{URL::to('panel/program/detail/'.Helper::en($program->id))}}" class="btn btn-default">{{trans('button.bc')}}</a>
  </div>
</div><!-- /.box -->
</div>
</div>
<div id="open-modal"></div>
@stop

@section('end-script')
    @parent
    <script type="text/javascript" src="{{asset('assets/js/AdminLTE/jquery.blockUI.js')}}"></script>
    <script src="{{ asset('assets/js/AdminLTE/custom.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
    })
    </script>
@stop