@extends('adminlte.layouts.content')

@section('header-content')
<div class="row">
  <div class="col-xs-12">
    <div class="info-box bg-aqua">
       <span class="info-box-icon"><i class="ion ion-clipboard"></i></span>
       <div class="info-box-content">
          <span class="info-box-number">{{$program->year}} / 3.17.{{$code}}.{{$program->code_program}} / {{$program->name}}</span>
       </div><!-- /.info-box-content -->
    </div>
  </div>
</div>
@stop

@section('body-content')
<div class="row">
<div class="col-xs-12">
<div class="box">
  <div class="box-header">
    <h3 class="box-title"></h3>
    <div class="box-tools">
      <div class="input-group">
        <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
        <div class="input-group-btn">
          <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        </div>
      </div>
    </div>
  </div><!-- /.box-header -->
  <div class="box-body table-responsive no-padding">
    <table class="table table-hover">
      <tbody>
      	<tr>
  	      <th>Kode Kegiatan</th>
  	      <th>Nama Kegiatan</th>
          <th>Pagu Kegiatan</th>
          <th>Rata - Rata</th>
  	      <th>Keterangan</th>
  	    </tr>
  	    @foreach($kegiatan as $row)
        <?php
        if(!empty($row->pagu_kegiatan_perubahan)){
          $pagu = $row->pagu_kegiatan_perubahan;
        }else{
          $pagu = $row->pagu_kegiatan;
        }
        ?>        
  	    <tr>
  	      <td>{{$row->kegiatan->code_kegiatan}}</td>
  	      <td><a href="{{URL::to('panel/program/detail-kegiatan/'.Helper::en($row->id_kegiatan))}}">{{$row->kegiatan->name}}</a></td>
  	      <td>Rp {{($pagu)?number_format($pagu,0,",","."):'0'}}</td>
          <td>
            <span class="badge bg-green">{{Helper::countRata($idUser,$row->id_kegiatan)}}</span> 
            <span class="badge bg-yellow">{{Helper::countRataSp2d($idUser,$row->id_kegiatan)}}</span> 
            <span class="badge bg-light-blue">{{Helper::countRataSpj($idUser,$row->id_kegiatan)}}</span>
          </td>
  	      <td>{{$row->keterangan}}</td>
  	    </tr>
  	    @endforeach
    </tbody>
    </table>
  </div><!-- /.box-body -->
  <div class="box-footer">
      <a href="{{URL::to('panel/program')}}" class="btn btn-default">{{trans('button.bc')}}</a>
  </div>
</div><!-- /.box -->
</div>
</div>
@stop