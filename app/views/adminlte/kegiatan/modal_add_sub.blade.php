<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog custom-width">
      <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
           <h4 class="modal-title">Tambah Sub Kegiatan</h4>
         </div>
          {{Form::open(array('url'=>'panel/program/add-sub-item/'.$kegiatan, 'id'=>'request', 'method'=>'POST', 'files'=>true))}}
         {{-- <form action="{{URL::to('panel/program/add-sub-item/'.$kegiatan)}}" method="POST"> --}}
         <div class="modal-body">
               <div class="box-body">
                     <div class="row">
                         <div class="col-xs-3">
                          <div class="form-group">
                             <label></label>
                             <select name="bulan" class="form-control" data-validation="required" data-validation-error-msg="Form bulan harus dipilih.">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                             </select>
                             {{-- <input placeholder="Bulan"> --}}
                          </div>
                         </div>
                         <div class="col-xs-5">
                          <div class="form-group">
                              <label></label>
                              <input name="sub_kegiatan" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="Form sub kegiatan harus diisi." placeholder="Sub Kegiatan" data-validation-event="keyup">
                          </div>
                         </div>
                         <div class="col-xs-2">
                          <div class="form-group">
                              <label></label>
                             <input name="volume" class="form-control" autocomplete="off" data-validation="number" data-validation-error-msg="Form volume harus diisi dengan angka." placeholder="Volume" data-validation-event="keyup">
                         </div>
                         </div>
                         <div class="col-xs-2">
                          <div class="form-group">
                              <label></label>
                             <input name="satuan" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="Form satuan harus diisi." placeholder="Satuan" data-validation-event="keyup">
                         </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-6">
                          <div class="form-group">
                              <label></label>
                             <input name="rencana_fisik" class="form-control" autocomplete="off" data-validation="number" data-validation-error-msg="Form rencana fisik harus diisi dengan angka." placeholder="Rencana Fisik (%)" data-validation-event="keyup">
                         </div>
                         </div>
                         <div class="col-xs-6">
                          <div class="form-group">
                              <label></label>
                              <input disabled="" class="form-control" placeholder="Realisasi Fisik">
                          </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-4">
                          <div class="form-group">
                              <label></label>
                             <input name="rencana_anggaran" class="form-control" autocomplete="off" data-validation="number" data-validation-error-msg="Form rencana anggaran harus diisi dengan angka." placeholder="Rencana Anggaran" data-validation-event="keyup">
                         </div>
                         </div>
                         <div class="col-xs-4">
                          <div class="form-group">
                              <label></label>
                             <input disabled="" class="form-control" placeholder="Anggaran SP2D">
                         </div>
                         </div>
                        <div class="col-xs-4">
                           <div class="form-group">
                              <label></label>
                              <input disabled="" class="form-control" placeholder="Anggaran SPJ">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="form-group">
                              <label></label>
                              <input type="text" name="keterangan" autocomplete="off" class="form-control" placeholder="Keterangan">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="form-group">
                              <label>Foto</label>
                              <input type="file" name="photo" autocomplete="off" class="form-control">
                           </div>
                        </div>
                     </div>
               </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default close-waiting" data-dismiss="modal">Tutup</button>
           <button type="submit" class="btn btn-primary">Submit</button>
         </div>
        {{Form::close()}}
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div>