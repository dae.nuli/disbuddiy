@extends('adminlte.layouts.base')

@section('head-script')
    <!-- Bootstrap 3.3.4 -->
    <link href="{{asset('adminlte2/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{asset('adminlte2/font-awesome-4.4.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{asset('adminlte2/ionicons-2.0.1/css/ionicons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset('adminlte2/dist/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{asset('adminlte2/dist/css/skins/_all-skins.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@stop

@section('header')

      <header class="main-header">               
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <a href="#" class="navbar-brand" style="padding:5px 15px">
                <img src="{{asset('assets/img/jogja.png')}}" style="width:100px">
              </a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                @include('adminlte.layouts.part.menu')
                {{-- <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                    </div>
                </form>  --}}                         
            </div><!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        {{-- @include('adminlte.layouts.part.message') --}}
                        {{-- @include('adminlte.layouts.part.notif') --}}
                        {{-- @include('adminlte.layouts.part.task') --}}
                        @include('adminlte.layouts.part.user')                  
                    </ul>
                </div><!-- /.navbar-custom-menu -->
          </div><!-- /.container-fluid -->
        </nav>
      </header>
@stop

@section('body')
<!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="">
        {{-- <div class="container"> --}}
          <!-- Content Header (Page header) -->
          <section class="content-header">
                @yield('header-content')
            {{-- <h1>
              {{$title}}
              <small>{{$path}}</small>
            </h1> --}}
            {{-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> --}}
          </section>

          <!-- Main content -->
          <section class="content">
                @yield('body-content')
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
@stop

@section('footer')
{{--     <footer class="main-footer">
        <div class="container">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.0
        </div>
            <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
        </div><!-- /.container -->
    </footer> --}}
@stop

@section('end-script')
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('adminlte2/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{asset('adminlte2/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="{{asset('adminlte2/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='{{asset("adminlte2/plugins/fastclick/fastclick.min.js")}}'></script>
    <!-- AdminLTE App -->
    <script src="{{asset('adminlte2/dist/js/app.min.js')}}" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('adminlte2/dist/js/demo.js')}}" type="text/javascript"></script>
@stop