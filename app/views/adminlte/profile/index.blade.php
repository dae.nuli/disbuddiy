@extends('adminlte.layouts.content')

@section('body-content')
@if(Session::has('profile'))
    <div class="alert callout callout-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{Session::get('profile')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'panel/profile/update', 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <label for="nama_instansi">Nama Instansi</label>
                        <input type="text" disabled="" value="{{$profile->instansi_name}}" class="form-control" id="nama_instansi">
                    </div>

                    <div class="col-xs-4">
                        <label for="email">Alamat Email</label>
                        <input type="email" disabled="" value="{{$profile->email}}" class="form-control" id="email">
                    </div>

                    <div class="col-xs-4">
                        <label for="username">Username</label>
                        <input type="text" disabled="" value="{{$profile->username}}" class="form-control" id="username">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <label for="nama_penanggung_jawab">Nama Penanggung Jawab</label>
                        <input type="text" name="nama_penanggung_jawab" autocomplete="off" value="{{$profile->personil_name}}" class="form-control" id="nama_penanggung_jawab">
                        {{$errors->first('nama_penanggung_jawab','<p class="text-red">:message</p>')}}
                    </div>

                    <div class="col-xs-4">
                        <label for="email_penanggung_jawab">Email Penanggung Jawab</label>
                        <input type="email" disabled="" value="{{$profile->email_staff}}" class="form-control" id="email_penanggung_jawab">
                        {{$errors->first('email_penanggung_jawab','<p class="text-red">:message</p>')}}
                    </div>

                    <div class="col-xs-4">
                        <label for="nomor_telefon">Nomor Telefon</label>
                        <input type="text" name="nomor_telefon" value="{{$profile->phone}}" class="form-control" id="nomor_telefon" autocomplete="off">
                        {{$errors->first('nomor_telefon','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" autocomplete="off" id="password">
                        <small class="help-block">Complete this form if you wanto change the password.</small>
                        {{$errors->first('password','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="ulangi_password">Ulangi Password</label>
                        <input type="password" name="ulangi_password" class="form-control" autocomplete="off" id="ulangi_password">
                        {{$errors->first('ulangi_password','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea id="alamat" name="alamat" class="form-control" style="height:100px;resize:none" rows="3">{{$profile->address}}</textarea>
                {{$errors->first('alamat','<p class="text-red">:message</p>')}}
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('panel/home')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop