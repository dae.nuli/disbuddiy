@extends('adminlte.layouts.content')

@section('end-script')
   @parent
   <script type="text/javascript" src="{{asset('assets/js/AdminLTE/jquery.blockUI.js')}}"></script>
   <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
   <script src="{{ asset('assets/js/AdminLTE/custom.js')}}" type="text/javascript"></script>
   <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
   <script src="{{asset('assets/css/select2.js')}}"></script>
   <script type="text/javascript">
   $(function() {
      $('.tahun').select2({
         theme: "classic"
      });
   });
   </script>
@stop

@section('body-content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
               {{Form::open(array('url' => 'panel/program', 'class'=>'pull-left', 'method' => 'post'))}}
               <select class="form-control tahun" style="padding:0;" name="year" onchange="this.form.submit()">
               @foreach($yearRange as $row)
                   <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
               @endforeach
               </select>
               {{Form::close()}}
               {{Form::open(array('url'=>'panel/program', 'method'=>'GET'))}}
               <?php $search = Input::get('search'); ?>
               <div class="input-group">
                  <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Cari Program" autocomplete="off">
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
               </div>
               {{Form::close()}}
         </div><!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
               <tbody>
               	<tr>
              	      <th>Kode Program</th>
              	      <th>Nama Program</th>
              	      <th>Jumlah Kegiatan</th>
              	      <th>Tahun</th>
                     <th></th>
           	      </tr>
        	         @foreach($program as $row)
                  <?php $totalProgram = Helper::UserKegiatan($row->id_program,$kpa); ?>
              	   <tr>
              	      <td>{{$row->program->code_program}}</td>
              	      <td>
                        <a href="{{URL::to('panel/program/detail/'.Helper::en($row->id_program))}}">{{$row->program->name}}</a>
                     </td>
              	      <td>{{$totalProgram}}</td>
              	      <td>{{$row->program->year}}</td>
                     <td>
                        <a href="{{URL::to('panel/program/detail/'.Helper::en($row->id_program))}}" class="btn btn-success btn-xs"><i class="fa fa-eye"></i> Detail</a>
                        <a class="btn btn-info btn-xs please-waiting print-program" data-program="{{Helper::en($row->id_program)}}"><i class="fa fa-print"></i> Print</a>
                     </td>
              	   </tr>
        	         @endforeach
               </tbody>
            </table>
         </div><!-- /.box-body -->
         <div class="box-footer">
         </div>
      </div><!-- /.box -->
   </div>
</div>
<div id="open-modal"></div>
@stop