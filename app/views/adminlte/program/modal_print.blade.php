<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog custom-width">
      <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
           <h4 class="modal-title">Print</h4>
         </div>
         {{Form::open(array('url'=>'panel/program/print/'.Helper::en($program->id), 'id'=>'request', 'method'=>'GET'))}}
         <div class="modal-body">
            <div class="box-body">
               <div class="row">
                  <div class="col-xs-6">
                     <div class="form-group">
                        <label></label>
                        <select name="kegiatan" class="form-control" data-validation="required" data-validation-error-msg="Form kegiatan harus dipilih.">
                           <option value="">- Pilih Kegiatan -</option>
                           @foreach($kegiatan as $row)
                              <option value="{{Helper::en($row->kegiatan->id)}}">{{$row->kegiatan->name}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="form-group">
                        <label></label>
                        <select name="tahap" class="form-control" data-validation="required" data-validation-error-msg="Form tahap harus dipilih.">
                           <option value="">- Pilih Tahap -</option>
                           @foreach($tahap as $row)
                              <option value="{{$row->id}}">{{$row->name}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default close-waiting" data-dismiss="modal">Tutup</button>
           <button type="submit" class="btn btn-primary">Submit</button>
         </div>
        {{Form::close()}}
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div>