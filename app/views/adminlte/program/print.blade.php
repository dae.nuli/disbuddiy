<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example 2</title>
    <link rel="stylesheet" href="{{asset('adminlte2/dist/css/pdf.css')}}" media="all" />
  </head>
  <body>
<?php
if(!empty($new->pagu_kegiatan_perubahan)){
  $pagu = $new->pagu_kegiatan_perubahan;
}else{
  $pagu = $new->pagu_kegiatan;
}
if($tahap==1){
	$tahaps = 'I';
}elseif ($tahap==2) {
	$tahaps = 'II';
}elseif ($tahap==3) {
	$tahaps = 'III';
}
?>
  	<h5 class="title">PEMERINTAH DAERAH DAERAH ISTIMEWA YOGYAKARTA</h5>
  	<h5 class="title">Kepatihan Danurejan 55213, Telepon: 512665, 562811</h5>
  	<hr>
  	<h5 class="sub-title">LAPORAN KINERJA PELAKSANAAN DANA KEISTIMEWAAN</h5>
	<h5 class="sub-title">DAERAH ISTIMEWA YOGYAKARTA</h5>
	<h5 class="sub-title">TAHAP {{$tahaps}} TAHUN ANGGARAN {{$tahun}}</h5>
  	<div>
  		<table>
  			<tr>
  				<td>SKPD</td><td> : {{$kpa->instansi_name}}</td>
  			</tr>
  		</table>
  	</div>
  	<div>
  		<table class="table-content">
  			<thead>
	  			<tr class="tr-atas">
		            <th rowspan="2">Sub Kegiatan</th>
		            <th rowspan="2">Volume</th>
		            <th rowspan="2">Satuan</th>
		            <th colspan="2">Fisik</th>
		            <th colspan="3">Anggaran</th>
		         </tr>
		         <tr class="tr-bawah">
		            <th>Rencana (%)</th>
		            <th>Realisasi (%)</th>
		            <th>Rencana</th>
		            <th>SP2D</th>
		            <th>SPJ</th>
		         </tr>
		         <tr class="nomor">
		         	<th>1</th>
		         	<th>2</th>
		         	<th>3</th>
		         	<th>4</th>
		         	<th>5</th>
		         	<th>6</th>
		         	<th>7</th>
		         	<th>8</th>
		         </tr>
  			</thead>
  			<tbody>
	        <?php
	        $ra[]   = '';
	        $sp2d[] = '';
	        $spj[]  = '';
	        ?>
	        @foreach($item as $row)
	        <?php
	        $ra[]   = ($row->anggaran_rencana?$row->anggaran_rencana:0);
	        $sp2d[] = ($row->anggaran_sppd?$row->anggaran_sppd:0);
	        $spj[]  = ($row->anggaran_spj?$row->anggaran_spj:0);
	        ?>
	        <tr>
	            <td>{{($row->name?$row->name:'-')}}</td>
	            <td>{{($row->vol?$row->vol:'-')}}</td>
	            <td>{{($row->satuan?$row->satuan:'-')}}</td>
	            <td>{{($row->fisik_rencana?$row->fisik_rencana:'-')}}</td>
	            <td>{{($row->fisik_realisasi?$row->fisik_realisasi:'-')}}</td>
	            <td>{{($row->anggaran_rencana?'Rp '.number_format($row->anggaran_rencana,0,",","."):'-')}}</td>
	            <td>{{($row->anggaran_sppd?'Rp '.number_format($row->anggaran_sppd,0,",","."):'-')}}</td>
	            <td>{{($row->anggaran_spj?'Rp '.number_format($row->anggaran_spj,0,",","."):'-')}}</td>
	        </tr>
        @endforeach
	    </tbody>
		    <tfoot style="background-color: darkgray;">
		        <tr>
			        <th style="text-align:center">TOTAL</th>
			        <th></th>
			        <th></th>
			        <th></th>
			        <th></th>
			        @if(array_sum($ra)<=$pagu)
			        <th>Rp {{(count($ra)>0)?number_format(array_sum($ra),0,",","."):'0'}}</th>
			        @else
			        <th>Rp {{(count($ra)>0)?'<span class="text-red">'.number_format(array_sum($ra),0,",",".").'</span>':'0'}}</th>
			        @endif
			        <th>Rp {{(count($sp2d)>0)?number_format(array_sum($sp2d),0,",","."):'0'}}</th>
			        <th>Rp {{(count($spj)>0)?number_format(array_sum($spj),0,",","."):'0'}}</th>
		        </tr>
		    </tfoot>
  		</table>
  	</div>
  </body>
</html>