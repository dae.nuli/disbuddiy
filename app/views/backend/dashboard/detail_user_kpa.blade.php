@extends('backend.layouts.content')

@section('body-content')
<div class="row">
   <div class="col-xs-12">
      <div class="callout callout-info">
          <h4>{{$program->year}} / {{$program->code_program}} / {{$program->name}}</h4>
      </div>
   </div>
</div>
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Kode PA/KPA</th>
                <th>Nama Instansi</th>
                <th>Nomor Telefon</th>
                <th>Status</th>
                <th>Register</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($kpa_users as $row)
            <tr>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$nomor++}}.</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->code_kpa}}</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->instansi_name}}</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->phone}}</td>
                <td>{{($row->is_active)?'<span class="label label-success">Active</span>':'<span class="label label-danger">Not Active</span>'}}</td>
                <td>{{date('d F Y, H:i:s',strtotime($row->created_at))}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$kpa_users->links()}}
    </div>
</div>
<div class="box-footer clearfix">
        <a href="{{$back}}" class="btn btn-default">{{trans('button.bc')}}</a>
</div>
<div id="open-modal"></div>
@stop