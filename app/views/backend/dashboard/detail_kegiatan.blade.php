@extends('backend.layouts.content')

@section('body-content')
<div class="row">
   <div class="col-xs-12">
      <div class="callout callout-info">
          <h4>{{$program->year}} / {{$program->code_program}} / {{$program->name}}</h4>
          <p>{{$kegiatan->code_kegiatan}} / {{$kegiatan->name}} / Rp {{number_format(Helper::totalPaguKegiatan($kegiatan->id),0,",",".")}}</p>
      </div>
   </div>
</div>
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Kode PA/KPA</th>
                <th>Nama Instansi</th>
                <th>Nomor Telefon</th>
                <th>Rata - Rata</th>
                <th>Status</th>
                <th>Register</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($item as $row)
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->user->code_kpa}}</td>
                <td><a href="{{URL::to('admin/home/user-item/'.$row->id_user.'/'.$row->id_kegiatan.'/'.$filter.'/'.$id.'/'.$tahun)}}">{{$row->user->instansi_name}}</a></td>
                <td>{{$row->user->phone}}</td>
                <td>
                    <span class="badge bg-green" data-toggle="tooltip" data-html="true" data-placement="top" title="FISIK">{{Helper::countRata($row->id_user,$row->id_kegiatan)}}</span>
                    <span class="badge bg-yellow" data-toggle="tooltip" data-html="true" data-placement="top" title="SP2D">{{Helper::countRataSp2d($row->id_user,$row->id_kegiatan)}}</span>
                    <span class="badge bg-light-blue" data-toggle="tooltip" data-html="true" data-placement="top" title="SPJ">{{Helper::countRataSpj($row->id_user,$row->id_kegiatan)}}</span>
                </td>
                <td>{{($row->user->is_active)?'<span class="label label-success">Active</span>':'<span class="label label-danger">Not Active</span>'}}</td>
                <td>{{date('d F Y, H:i:s',strtotime($row->user->created_at))}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$item->links()}}
    </div>
</div>
<div class="box-footer clearfix">
        <a href="{{$back}}" class="btn btn-default">{{trans('button.bc')}}</a>
</div>
<div id="open-modal"></div>
@stop

@section('end-script')
    @parent
    <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
    })
    </script>
@stop