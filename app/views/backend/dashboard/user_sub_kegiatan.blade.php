@extends('backend.layouts.content')

@section('header-content')

@stop

@section('body-content')
<?php
if(!empty($ikm->pagu_kegiatan_perubahan)){
  $pagu = $ikm->pagu_kegiatan_perubahan;
}else{
  $pagu = $ikm->pagu_kegiatan;
}
?>
<div class="row">
   <div class="col-xs-12">
      <div class="callout callout-info">
          <h4>{{$program->year}} / 3.17.{{$user->code_kpa}}.{{$program->code_program}} / {{$program->name}}</h4>
          <p>3.17.{{$user->code_kpa}}.{{$program->code_program}}.{{$kegiatan->code_kegiatan}} / {{$kegiatan->name}} / Rp {{number_format($pagu,0,",",".")}}</p>
      </div>
   </div>
</div>
<div class="box no-border">
  <div class="box-body table-responsive no-padding">
   <table class="table table-bordered table-hover item-table">
      <thead style="background-color: #B9B9B9;">
      	<tr>
            <th rowspan="2">Tahap</th>
            <th rowspan="2">Bulan</th>
            <th rowspan="2">Sub Kegiatan</th>
            <th rowspan="2">Volume</th>
            <th rowspan="2">Satuan</th>
            <th colspan="2">Fisik</th>
            <th colspan="3">Anggaran</th>
            <th rowspan="2">Keterangan</th>
            <th rowspan="2">Bukti</th>
            <th rowspan="2">Progress</th>
         </tr>
         <tr>
            <th>Rencana (%)</th>
            <th>Realisasi (%)</th>
            <th>Rencana</th>
            <th>SP2D</th>
            <th>SPJ</th>
         </tr>
      </thead>
      <tbody>
        <?php
         $ra[]   = '';
         $sp2d[] = '';
         $spj[]  = '';
         ?>
         @foreach($item as $row)
         <?php
          if(!empty($row->fisik_rencana) && !empty($row->fisik_realisasi)){
            $FR[] = floor(($row->fisik_realisasi/$row->fisik_rencana)*100);
          }else{
            $FR[] = 0;
          }

          if(!empty($row->anggaran_sppd) && !empty($row->anggaran_rencana)){
            $AS[] = floor(($row->anggaran_sppd/$row->anggaran_rencana)*100);
          }else{
            $AS[] = 0;
          }

          if(!empty($row->anggaran_spj) && !empty($row->anggaran_rencana)){
            $AJ[] = floor(($row->anggaran_spj/$row->anggaran_rencana)*100);
          }else{
            $AJ[] = 0;
          }

          $ra[]   = ($row->anggaran_rencana?$row->anggaran_rencana:0);
          $sp2d[] = ($row->anggaran_sppd?$row->anggaran_sppd:0);
          $spj[]  = ($row->anggaran_spj?$row->anggaran_spj:0);
          $BULAN  = date('n');
          ?>
         <tr {{( (($row->tahap->month<$BULAN) && ($row->fisik_realisasi=="")) || (($row->tahap->month<$BULAN) && ($row->anggaran_sppd=="")) || (($row->tahap->month<$BULAN) && ($row->anggaran_spj=="")) )?'class="red-row"':''}}>
            <td>Tahap {{$row->tahap->id_tahap}}</td>
            <td>{{Helper::Month($row->tahap->month)}}</td>
            <td>{{($row->name?$row->name:'-')}}</td>
            <td>{{($row->vol?$row->vol:'-')}}</td>
            <td>{{($row->satuan?$row->satuan:'-')}}</td>
            <td>{{($row->fisik_rencana?$row->fisik_rencana:'-')}}</td>
            <td>{{($row->fisik_realisasi?$row->fisik_realisasi:'-')}}</td>
            <td>{{($row->anggaran_rencana?'Rp '.number_format($row->anggaran_rencana,0,",","."):'-')}}</td>
            <td>{{($row->anggaran_sppd?'Rp '.number_format($row->anggaran_sppd,0,",","."):'-')}}</td>
            <td>{{($row->anggaran_spj?'Rp '.number_format($row->anggaran_spj,0,",","."):'-')}}</td>
            <td>{{($row->keterangan?$row->keterangan:'-')}}</td>
            <td>{{($row->photo?'<a href="'.$download.'/'.$row->id.'" target="_blank">Download</a>':'-')}}</td>
            <td>
              {{(!empty($row->fisik_rencana) && !empty($row->fisik_realisasi))?'<span class="badge bg-green" data-toggle="tooltip" data-html="true" data-placement="top" title="FISIK">'.floor(($row->fisik_realisasi/$row->fisik_rencana)*100).'%</span>':''}}

              {{(!empty($row->anggaran_sppd) && !empty($row->anggaran_rencana))?'<span class="badge bg-yellow" data-toggle="tooltip" data-html="true" data-placement="top" title="SP2D">'.floor(($row->anggaran_sppd/$row->anggaran_rencana)*100).'%</span>':''}}
              
              {{(!empty($row->anggaran_spj) && !empty($row->anggaran_rencana))?'<span class="badge bg-light-blue" data-toggle="tooltip" data-html="true" data-placement="top" title="SPJ">'.floor(($row->anggaran_spj/$row->anggaran_rencana)*100).'%</span>':''}}
            </td>
         </tr>
         @endforeach
      </tbody>
      <tfoot style="background-color: darkgray;">
        <tr>
          <th style="text-align:center">TOTAL</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          @if(array_sum($ra)<=$pagu)
            <th>Rp {{(count($ra)>0)?number_format(array_sum($ra),0,",","."):'0'}}</th>
          @else
            <th>Rp {{(count($ra)>0)?'<span class="text-red">'.number_format(array_sum($ra),0,",",".").'</span>':'0'}}</th>
          @endif
          <th>Rp {{(count($sp2d)>0)?number_format(array_sum($sp2d),0,",","."):'0'}}</th>
          <th>Rp {{(count($spj)>0)?number_format(array_sum($spj),0,",","."):'0'}}</th>
          <th></th>
          <th></th>
          <th>
            <span class="badge bg-green">{{(isset($FR))?ceil(array_sum($FR)/count($item)):''}}</span> 
            <span class="badge bg-yellow">{{(isset($AS))?ceil(array_sum($AS)/count($item)):''}}</span> 
            <span class="badge bg-light-blue">{{(isset($AJ))?ceil(array_sum($AJ)/count($item)):''}}</span> 
          </th>
        </tr>
      </tfoot>
   </table>
  </div><!-- /.box-body -->
  <div class="box-footer">
      <a href="{{$back}}" class="btn btn-default">{{trans('button.bc')}}</a>
  </div>
</div><!-- /.box -->
@stop