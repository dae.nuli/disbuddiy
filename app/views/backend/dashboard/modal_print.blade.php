<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$.validate({
   form : '#requests',
   onSuccess : function() {
      waiting();
   }
});
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});

loads();
$(document).on('change','.choose-program',function(){
    loads();
});
function loads(){
    var program = $('.choose-program').val();
    var kpa     = $('.choose-program').data('kpa');
    $.ajax({
        url: '{{URL::to("admin/home/search-kegiatan")}}',
        type: 'POST',
        cache: false,
        beforeSend:function(){
            $('.choose-kegiatan').html('<option>Please Waiting<option>');
        },
        success:function(data){
            $('.choose-kegiatan').html(data);
        },
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {ID_PRO: program, KPA:kpa},
    });
}
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {{Form::open(array('url'=>'admin/home/print', 'id'=>'requests', 'method'=>'GET'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Print</h4>
            </div>
            <input type="hidden" name="kpa" value="{{$kpa}}">
            <input type="hidden" name="tahun" value="{{$tahun}}">
            <div class="modal-body">
                <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Program</label>
                                    <select name="program" class="form-control choose-program" data-kpa="{{$kpa}}" data-validation="number" data-validation-error-msg="Form program harus dipilih.">
                                        @foreach($program as $row)
                                            <option value="{{$row->program->id}}">{{$row->program->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <label>Kegiatan</label>
                                <select name="kegiatan" class="form-control choose-kegiatan" data-validation="number" data-validation-error-msg="Form kegiatan harus dipilih.">
                                </select>
                            </div>
                            <div class="col-xs-4">
                                <label>Tahap</label>
                                <select name="tahap" class="form-control" data-validation="number" data-validation-error-msg="Form tahap harus dipilih.">
                                    @foreach($tahap as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-default close-waiting" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>