@extends('backend.layouts.content')

@section('body-content')
<div class="row">
   <div class="col-xs-12">
      <p>{{$kpa->code_kpa}} / {{$kpa->instansi_name}} / {{$kpa->personil_name}}</p>
      <p class="lead">{{$program->year}} / {{$program->code_program}} / {{$program->name}}</p>
   </div>
</div>
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Kode Kegiatan</th>
                <th>Nama Kegiatan</th>
                <th>Pagu Kegiatan</th>
                <th>Keterangan</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($kegiatan as $row)
            <?php 
                $totalPaguKegiatan = Helper::totalPaguKPAKegiatan($program->id,$row->id_kegiatan,$kpa->id);
            ?>
            <tr>
                <td>{{$nomor++}}.</td>
                @if(!empty($search))
                <td><a href="{{URL::to('admin/home/user-sub-kegiatan/'.$kpa->id.'/'.$program->id.'/'.$row->id_kegiatan.'/'.$year.'/'.$search)}}">{{$row->kegiatan->code_kegiatan}}</a></td>
                @else
                <td><a href="{{URL::to('admin/home/user-sub-kegiatan/'.$kpa->id.'/'.$program->id.'/'.$row->id_kegiatan.'/'.$year)}}">{{$row->kegiatan->code_kegiatan}}</a></td>
                @endif
                <td>{{$row->kegiatan->name}}</td>
                <td>Rp {{number_format($totalPaguKegiatan,0,",",".")}}</td>
                <td>{{$row->keterangan}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$kegiatan->links()}}
    </div>
</div>
<div class="box-footer clearfix">
        <a href="{{$back}}" class="btn btn-default">{{trans('button.bc')}}</a>
</div>
<div id="open-modal"></div>
@stop