@extends('backend.layouts.content')

@section('header-content')

@stop

@section('body-content')
    {{Form::open(array('url'=>'admin/home', 'id'=>'request', 'method'=>'GET'))}}
    <?php $search = Input::get('search'); ?>
    <?php $filter = Input::get('filter'); ?>
    <?php $year   = Input::get('tahun'); ?>
    <div class="row">
        <div class="col-xs-3">
            <div class="form-group">
                <select name="filter" class="form-control filter" style="padding:0;" data-validation="required" data-validation-error-msg="Form filter harus diisi.">
                    <option value="">- Pilih Filter -</option>
                    <option value="1" {{($filter==1?'selected':'')}}>Program</option>
                    <option value="2" {{($filter==2?'selected':'')}}>Kegiatan</option>
                    <option value="3" {{($filter==3?'selected':'')}}>User PA/KPA</option>
                </select>
            </div>
            {{-- <div id="wrap-errors-filter"></div> --}}
        </div>
        <div class="col-xs-3">
            <div class="form-group">
                <select name="tahun" class="form-control tahun" style="padding:0;" data-validation="required" data-validation-error-msg="Form tahun harus diisi.">
                    <option value="">- Pilih Tahun -</option>
                    @foreach($tahun as $row)
                    <option value="{{$row}}" {{$year==$row?'selected':''}}>{{$row}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control" placeholder="Cari" autocomplete="off">
                    <div class="input-group-btn">
                        <button class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            {{-- <div id="wrap-errors-cari"></div> --}}
        </div>
    </div>
    {{Form::close()}}

<?php 
if(Input::get('page')){
    $page = Input::get('page');
}else{
    $page = 1;
}
$nomor = $page + ($page-1) * ($limit-1);
?>
@if($filter==1 && !empty($year))
<div class="box" style="margin-top:10px">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Tahun</th>
                <th>Kode Program</th>
                <th>Nama Program</th>
                <th>Kegiatan</th>
                <th>Pagu Anggaran</th>
                <th>Jumlah PA/KPA</th>
            </tr>
            </thead> 
            <tbody>
                @foreach($program as $row)
                <?php 
                    $total     = Helper::KegiatanTotal($row->id);
                    $totalPagu = Helper::totalPagu($row->id);
                ?>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$row->year}}</td>
                    <td>{{$row->code_program}}</td>
                    <td>{{$row->name}}</td>
                    @if(!empty($search))
                        <td><a href="{{URL::to('admin/home/detail-program/'.$filter.'/'.$row->id.'/'.$year.'/'.$search)}}">{{$total}}</a></td>
                    @else
                        <td><a href="{{URL::to('admin/home/detail-program/'.$filter.'/'.$row->id.'/'.$year)}}">{{$total}}</a></td>
                    @endif
                    <td>Rp {{number_format($totalPagu,0,",",".")}}</td>
                    @if(!empty($search))
                        <td><a href="{{URL::to('admin/home/detail/'.$filter.'/'.$row->id.'/'.$year.'/'.$search)}}" class="detail-total-kpa">{{Helper::TotalKPAByProgram($row->id)}}</a></td>
                    @else
                        <td><a href="{{URL::to('admin/home/detail/'.$filter.'/'.$row->id.'/'.$year)}}" class="detail-total-kpa">{{Helper::TotalKPAByProgram($row->id)}}</a></td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$program->appends(array('filter' => Input::get('filter'),'tahun'=>Input::get('tahun'),'search'=>Input::get('search')))->links()}}
    </div>
</div>
@elseif($filter==2 && !empty($year))
<div class="box" style="margin-top:10px">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Tahun</th>
                <th>Kode Kegiatan</th>
                <th>Nama Kegiatan</th>
                {{-- <th>Total PA/KPA</th> --}}
                <th>Pagu Anggaran</th>
            </tr>
            </thead> 
            <tbody>
                @foreach($kegiatan as $row)
                {{-- <tr class="dashboard-kegiatan" title="Klik salah satu untuk melihat detail" data-idkegiatan="{{$row->id}}"> --}}
                <?php 
                    $totalPaguKegiatan = Helper::totalPaguKegiatan($row->id);
                ?>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$row->year}}</td>
                    @if(!empty($search))
                    <td>
                        <a href="{{URL::to('admin/home/detail/'.$filter.'/'.$row->id.'/'.$year.'/'.$search)}}">{{$row->code_kegiatan}}</a>
                    </td>
                    @else
                    <td>
                        <a href="{{URL::to('admin/home/detail/'.$filter.'/'.$row->id.'/'.$year)}}">{{$row->code_kegiatan}}</a>
                    </td>
                    @endif
                    <td>{{$row->kegiatan_name}}</td>
                    {{-- <td>{{Helper::countKPA($row->program_id,$row->id)}}</td> --}}
                    <td>Rp {{number_format($totalPaguKegiatan,0,",",".")}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix"> 
        {{$kegiatan->appends(array('filter' => Input::get('filter'),'tahun'=>Input::get('tahun'),'search'=>Input::get('search')))->links()}}
    </div>
</div>
@elseif($filter==3 && !empty($year))
<div class="box" style="margin-top:10px">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Tahun</th>
                <th>Kode PA/KPA</th>
                <th>Nama Instansi</th>
                <th>Nama Personil</th>
                <th>Jumlah Program</th>
                <th></th>
            </tr>
            </thead> 
            <tbody>
                @foreach($user as $row)
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$row->year}}</td>
                    <td>{{$row->code_kpa}}</td>
                    <td>{{$row->instansi_name}}</td>
                    <td>{{$row->personil_name}}</td>
                    @if(!empty($search))
                        <td><a href="{{URL::to('admin/home/user-program/'.$filter.'/'.$row->kpa_id.'/'.$row->year.'/'.$search)}}">{{$row->total}}</a></td>
                    @else
                        <td><a href="{{URL::to('admin/home/user-program/'.$filter.'/'.$row->kpa_id.'/'.$row->year)}}">{{$row->total}}</a></td>
                    @endif
                    <td><a class="btn btn-info btn-xs print-program-kpa" data-kpa="{{$row->kpa_id}}" data-tahun="{{$row->year}}"><i class="fa fa-print"></i> Print</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix"> 
        {{$user->appends(array('filter' => Input::get('filter'),'tahun'=>Input::get('tahun'),'search'=>Input::get('search')))->links()}}
    </div>
</div>
@else
<div class="box" style="margin-top:10px">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Tahun</th>
                <th>Kode PA/KPA</th>
                <th>Nama Instansi</th>
                <th>Penanggung Jawab</th>
            </tr>
            </thead> 
            <tbody>
                @if(!empty($under80))
                    @foreach($under80 as $row)
                    <?php
                    $now   = date('Y');
                    // $tahun = $row->program->year;
                    ?>
                    {{-- @if($tahun==$now) --}}
                    {{-- <tr>
                        <td>{{$nomor++}}</td>
                        <td>{{date('Y')}}</td>
                        <td>{{$row->userkpa->code_kpa}}</td>
                        <td><a href="{{URL::to('admin/home/kpa-program/'.$row->id_user)}}">{{$row->userkpa->instansi_name}}</a></td>
                        <td>{{$row->personil_name}}</td>
                    </tr> --}}
                    {{-- @endif --}}
                    <tr>
                        <td>{{$nomor++}}</td>
                        <td>{{date('Y')}}</td>
                        <td>{{$row->code_kpa}}</td>
                        <td><a href="{{URL::to('admin/home/kpa-program/'.$row->id)}}">{{$row->instansi_name}}</a></td>
                        <td>{{$row->personil_name}}</td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5" align="center">Data Kosong</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        @if(!empty($under80))
        {{$under80->links()}}
        @endif
    </div>
</div>
@endif
<div id="open-modal"></div>
@stop

@section('end-script')
    @parent
    <script src="{{asset('assets/js/Chart.js')}}"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>
    <script type="text/javascript">
     $.validate({
        form : '#request',
        onSuccess : function() {
            // $('.wait').addClass('disabled');
            // $('.wait').html('Waiting ...');
        }
    });
    $(function() {
        $('.tahun,.filter').select2({
            theme: "classic"
        });
    });
 
    </script>
@stop
