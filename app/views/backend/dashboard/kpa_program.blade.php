@extends('backend.layouts.content')

@section('body-content')
<div class="row">
   <div class="col-xs-12">
      <p class="lead">{{$kpa->code_kpa}} / {{$kpa->instansi_name}} / {{$kpa->personil_name}}</p>
   </div>
</div>
<div class="box" style="margin-top:10px">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Tahun</th>
                <th>Kode Program</th>
                <th>Nama Program</th>
                <th>Kegiatan</th>
                <th>Pagu Anggaran</th>
            </tr>
            </thead> 
            <tbody>
                <?php 
                if(Input::get('page')){
                    $page = Input::get('page');
                }else{
                    $page = 1;
                }
                $nomor = $page + ($page-1) * ($limit-1);
                ?>
                @foreach($program as $row)
                <?php 
                    // $total     = Helper::countKegiatanUnder($row->id_program,$kpa->id,$nilai);
                    $totalPagu = Helper::totalPaguKPA($row->id,$kpa->id);
                    $total = Helper::under_kegiatan_count($row->id,$kpa->id);
                    // $totalPagu = Helper::
                ?>
                {{-- <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$row->program->year}}</td>
                    <td>{{$row->program->code_program}}</td>
                    <td>{{$row->program->name}}</td>
                    <td><a href="{{URL::to('admin/home/kpa-kegiatan/'.$kpa->id.'/'.$row->id_program)}}">{{$total}}</td>
                    <td>Rp {{number_format($totalPagu,0,",",".")}}</td>
                </tr> --}}
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$row->year}}</td>
                    <td>{{$row->code_program}}</td>
                    <td>{{$row->name}}</td>
                    <td><a href="{{URL::to('admin/home/kpa-kegiatan/'.$kpa->id.'/'.$row->id)}}">{{$total}}</td>
                    <td>Rp {{number_format($totalPagu,0,",",".")}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$program->links()}}
    </div>
</div>
<div class="box-footer">
    <a href="{{$back}}" class="btn btn-default">{{trans('button.bc')}}</a>
</div>
@stop