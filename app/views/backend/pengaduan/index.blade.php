@extends('backend.layouts.content')

@section('header-content')
{{-- <div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/users/create')}}" class="btn btn-primary">Create</a>
</div> --}}
{{Form::open(array('url'=>'admin/pengaduan', 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Search staff name">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('pengaduan'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('pengaduan')}}.
    </div>
@endif

@if(Session::has('pengaduan_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('pengaduan_alert')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>NAMA</th>
                <th>EMAIL</th>
                <th>HANDPHONE</th>
                <th>USULAN</th>
                <th>DATE</th>
                <th></th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($index as $row)
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->name}}</td>
                <td>{{$row->email}}</td>
                <td>{{$row->phone_number}}</td>
                <td><a href="{{URL::to('admin/pengaduan/detail/'.$row->id)}}">{{str_limit($row->content, 20)}}</a></td>
                <td>{{date('d F Y, H:i:s',strtotime($row->created_at))}}</td>
                <td>
                    <a href="{{URL::to('admin/pengaduan/detail/'.$row->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-fw fa-info"></i> Detail</a>
                    <a href="{{URL::to('admin/pengaduan/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$index->links()}}
    </div>
</div>
<div id="open-modal"></div>
@stop