@extends('backend.layouts.content')
 
@section('body-content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Data Pengusul</h3>
    </div>
    <div class="box-body">  
        <div class="row">
            <div class="col-xs-4">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" value="{{$detail->name}}" class="form-control" readonly="">
                    
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" value="{{$detail->email}}" class="form-control" readonly="">
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label>Nomor Handphone</label>
                    <input type="text" value="{{$detail->phone_number}}" class="form-control" readonly="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Judul Pengaduan</label>
                    <input type="text" class="form-control" value="{{($detail->subject)?$detail->subject:'-'}}" readonly="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Isi Pengaduan</label>
                    <textarea class="form-control" readonly="">{{$detail->content}}</textarea>
                </div>
            </div>
        </div>
    </div><!-- /.box-body -->
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Data SKPD</h3>
    </div>
    <div class="box-body">  
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" value="{{$detail->userkpa->instansi_name}}" class="form-control" readonly="">
                    
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Program</label>
                    <input type="text" value="{{isset($detail->program->name)?$detail->program->name:'-'}}" class="form-control" readonly="">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Kegiatan</label>
                    <input type="text" value="{{isset($detail->kegiatan->name)?$detail->kegiatan->name:'-'}}" class="form-control" readonly="">
                </div>
            </div>
        </div>
    </div><!-- /.box-body -->
</div>

<div class="box-footer">
    <a href="{{URL::to('admin/pengaduan')}}" class="btn btn-default">{{trans('button.bc')}}</a>
</div>
@stop