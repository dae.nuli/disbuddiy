@extends('backend.layouts.content')

@section('end-script')
    @parent
<script src="{{asset('assets/js/highcharts.js')}}"></script>
<script type="text/javascript">
$(function () {
    $('#Vchart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Unique Visitor'
        },
        xAxis: {
            categories: [{{$month}}]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total of unique visitor'
            }
            // ,
            // stackLabels: {
            //     enabled: true,
            //     style: {
            //         fontWeight: 'bold',
            //         color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            //     }
            // }
        },
        legend: {
            align: 'right',
            x: -70,
            verticalAlign: 'top',
            y: 20,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black, 0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: 'Visitor',
            data: [{{$visitor}}]
        }]
    });


    $('#Logchart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Visitor Click'
        },
        xAxis: {
            categories: [{{$month}}]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total of Click'
            }
            // ,
            // stackLabels: {
            //     enabled: true,
            //     style: {
            //         fontWeight: 'bold',
            //         color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            //     }
            // }
        },
        legend: {
            align: 'right',
            x: -70,
            verticalAlign: 'top',
            y: 20,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black, 0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: 'Visitor',
            data: [{{$yearstatistic}}]
        }]
    });
});
</script>
@stop

@section('body-content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">
                {{Form::open(array('url' => 'admin/statistics', 'method' => 'post'))}}
                <select class="form-control" name="unique" onchange="this.form.submit()">
                    @foreach($year_range as $row)
                        <option value="{{$row}}" {{($year_unique==$row) ? 'selected' : ''}}>{{$row}}</option>
                    @endforeach
                </select>
                {{Form::close()}}
            </h3>
        </div>        
        <div id="Vchart"></div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">
                {{Form::open(array('url' => 'admin/statistics', 'method' => 'post'))}}
                <select class="form-control" name="activity" onchange="this.form.submit()">
                    @foreach($year_range as $row)
                        <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
                    @endforeach
                </select>
                {{Form::close()}}
            </h3>
        </div>
        <div id="Logchart"></div>
    </div>
@stop