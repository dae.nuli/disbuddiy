@extends('backend.layouts.content')


@section('end-script')
    @parent

    <script type="text/javascript">
        // $(document).on('click', '.actived', function() {
        //     waiting();
        // });
    </script>
@stop

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/kpa/create')}}" class="btn btn-primary">{{trans('button.cr')}}</a>
</div>
{{Form::open(array('url'=>'admin/kpa', 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Pencarian instansi" autocomplete="off">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('kpa'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('kpa')}}.
    </div>
@endif

<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Kode PA/KPA</th>
                <th>Nama Instansi</th>
                <th>Nomor Telefon</th>
                <th>Status</th>
                <th>Register</th>
                <th></th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($kpa as $row)
            <tr>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$nomor++}}.</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->code_kpa}}</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->instansi_name}}</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->phone}}</td>
                <td>{{($row->is_active)?'<span class="label label-success">Active</span>':'<span class="label label-danger">Not Active</span>'}}</td>
                <td>{{date('d F Y, H:i:s',strtotime($row->created_at))}}</td>
                <td>
                    @if($row->is_active)
                        <a href="{{URL::to('admin/kpa/nonactive/'.$row->id)}}" class="btn btn-danger btn-xs noactived"><i class="fa fa-fw fa-ban"></i> Non Aktifkan</a>
                    @else
                        @if(empty($row->password))
                            <a href="{{URL::to('admin/kpa/send/'.$row->id)}}" class="btn btn-success btn-xs actived"><i class="fa fa-fw  fa-check-circle"></i> Aktifkan</a>
                        @else
                            <a href="{{URL::to('admin/kpa/active/'.$row->id)}}" class="btn btn-success btn-xs actived"><i class="fa fa-fw  fa-check-circle"></i> Aktifkan</a>
                        @endif
                    @endif

                    <a href="{{URL::to('admin/kpa/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> {{trans('button.ed')}}</a>
                    <a href="{{URL::to('admin/kpa/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> {{trans('button.dl')}}</a>
                    <a href="{{URL::to('admin/kpa/reset/'.$row->id)}}" class="btn btn-warning btn-xs reset"><i class="fa fa-fw fa-refresh"></i> Reset</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$kpa->links()}}
    </div>
</div>
<div id="open-modal"></div>
@stop