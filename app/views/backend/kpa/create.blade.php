@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
     $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/kpa', 'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="form-group">
                <label for="kode_pa_kpa">Kode PA/KPA</label>
                <input type="text" name="kode_pa_kpa" autocomplete="off" value="{{Input::old('kode_pa_kpa')}}" class="form-control" id="kode_pa_kpa" data-validation="required" data-validation-error-msg="Form kode PA/KPA harus diisi." data-validation-event="keyup">
                {{$errors->first('kode_pa_kpa','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="nama_instansi">Nama Instansi</label>
                <input type="text" name="nama_instansi" autocomplete="off" value="{{Input::old('nama_instansi')}}" class="form-control" id="nama_instansi" data-validation="required" data-validation-error-msg="Form nama instansi harus diisi." data-validation-event="keyup">
                {{$errors->first('nama_instansi','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" autocomplete="off" value="{{Input::old('email')}}" class="form-control" id="email" data-validation="email" data-validation-error-msg="Format email salah." data-validation-event="keyup">
                {{$errors->first('email','<p class="text-red">:message</p>')}}
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/kpa')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop