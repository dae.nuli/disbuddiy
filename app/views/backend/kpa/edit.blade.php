@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
     $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/kpa/edit/'.$user->id, 'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="form-group">
                <label for="kode_pa_kpa">Kode PA/KPA</label>
                <input type="text" name="kode_pa_kpa" autocomplete="off" value="{{$user->code_kpa}}" class="form-control" id="kode_pa_kpa" data-validation="required" data-validation-error-msg="Form kode PA/KPA harus diisi." data-validation-event="keyup">
                {{$errors->first('kode_pa_kpa','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="nama_instansi">Nama Instansi</label>
                <input type="text" name="nama_instansi" autocomplete="off" value="{{$user->instansi_name}}" class="form-control" id="nama_instansi" data-validation="required" data-validation-error-msg="Form nama instansi harus diisi." data-validation-event="keyup">
                {{$errors->first('nama_instansi','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="telefon">Nomor Telefon</label>
                <input type="text" name="telefon" autocomplete="off" value="{{$user->phone}}" class="form-control" id="telefon" data-validation="number" data-validation-error-msg="Form nomor telefon harus diisi dengan angka." data-validation-event="keyup" data-validation-optional="true">
                {{$errors->first('telefon','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" autocomplete="off" value="{{$user->email}}" class="form-control" id="email" data-validation="email" data-validation-error-msg="Format email salah." data-validation-event="keyup">
                {{$errors->first('email','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" autocomplete="off" value="{{$user->username}}" readonly="" class="form-control" id="username">
                {{$errors->first('username','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="password">Password Baru</label>
                <input type="text" name="password" class="form-control" id="password">
                <small class="help-block">Complete this form if you want to change the password.</small>
                {{$errors->first('password','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="nama_penanggung_jawab">Nama Pananggung Jawab</label>
                <input type="text" name="nama_penanggung_jawab" autocomplete="off" value="{{$user->personil_name}}" class="form-control" id="nama_penanggung_jawab">
                {{$errors->first('nama_penanggung_jawab','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="email_staff">Email Staff</label>
                <input type="email" name="email_staff" autocomplete="off" value="{{$user->email_staff}}" class="form-control" id="email_staff" data-validation="email" data-validation-error-msg="Format email salah." data-validation-event="keyup" data-validation-optional="true">
                {{$errors->first('email_staff','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="alamat">Address</label>
                <input type="text" name="alamat" autocomplete="off" value="{{$user->address}}" class="form-control" id="alamat">
                {{$errors->first('alamat','<p class="text-red">:message</p>')}}
            </div>
            
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/kpa')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop