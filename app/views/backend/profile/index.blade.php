@extends('backend.layouts.content')

@section('body-content')
@if(Session::has('users'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('users')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'admin/profile/update', 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <label for="nama">Nama</label>
                        <input type="text" name="name" value="{{$profile->name}}" class="form-control" id="name">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>

                    <div class="col-xs-4">
                        <label for="email">Alamat Email</label>
                        <input type="email" disabled="" value="{{$profile->email}}" class="form-control" id="email">
                        {{$errors->first('email','<p class="text-red">:message</p>')}}
                    </div>

                    <div class="col-xs-4">
                        <label for="phone_number">Nomor Telefon</label>
                        <input type="text" name="phone_number" value="{{$profile->phone}}" class="form-control" id="phone_number">
                        {{$errors->first('phone_number','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password">
                        <small class="help-block">Complete this form if you wanto change the password.</small>
                        {{$errors->first('password','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="retype_password">Ulangi Password</label>
                        <input type="password" name="retype_password" class="form-control" id="retype_password">
                        {{$errors->first('retype_password','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="address">Alamat</label>
                <textarea id="address" name="address" class="form-control" style="height:100px" rows="3">{{$profile->address}}</textarea>
                {{$errors->first('address','<p class="text-red">:message</p>')}}
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop