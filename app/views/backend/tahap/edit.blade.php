@extends('backend.layouts.content')

@section('body-content')
@if(Session::has('tahapan_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('tahapan_alert')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'admin/tahapan/edit/'.$tahapan->id, 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <label for="tahapan">Tahapan</label>
                        <select class="form-control" name="tahapan">
                            @foreach($tahap as $row)
                                <option value="{{$row->id}}" {{($tahapan->id_tahap==$row->id)?'selected':''}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <label for="bulan">Bulan</label>
                        <select class="form-control" name="bulan">
                            @foreach($bulan as $row)
                                <option value="{{$row}}" {{($tahapan->month==$row)?'selected':''}}>{{Helper::Month($row)}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <label for="tahun">Tahun</label>
                        <select class="form-control" disabled="">
                                <option value="{{$year}}">{{$year}}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/tahapan')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary please-waiting">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop