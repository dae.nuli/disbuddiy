@extends('backend.layouts.content')

@section('end-script')
    @parent
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        $('.tahun').select2({
            theme: "classic"
        });
    });
    </script>
@stop
@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/tahapan/create')}}" class="btn btn-primary">Tambah</a>
</div>
<div class="pull-right" style="margin-left:5px">
    {{Form::open(array('url' => 'admin/tahapan/year', 'method' => 'post'))}}
    <select class="form-control tahun" name="year" style="padding:0;" onchange="this.form.submit()">
        @foreach($yearRange as $row)
            <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
        @endforeach
    </select>
    {{Form::close()}}
</div>

@stop
@section('body-content')

@if(Session::has('tahapan_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('tahapan_alert')}}.
    </div>
@endif

@if(Session::has('tahapan'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('tahapan')}}.
    </div>
@endif
    <div class="box box-primary">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tahap</th>
                        <th>Bulan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tbody>
                        <?php $no=1;?>
                        @foreach($tahapan as $row)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>Tahap {{$row->id_tahap}}</td>
                            <td>{{Helper::Month($row->month)}}</td>
                            <td>
                                <a href="{{URL::to('admin/tahapan/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Ubah</a>
                                <a href="{{URL::to('admin/tahapan/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </tbody>
            </table>
        </div>
        
    </div>
@stop