@extends('backend.layouts.content')

@section('body-content')
@if(Session::has('tahapan_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('tahapan_alert')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'admin/tahapan', 'method'=>'POST'))}}
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-2">
                        <label for="tahun">Tahun</label>
                        <select class="form-control" name="tahun">
                            @foreach($yearRange as $row)
                                <option value="{{$row}}">{{$row}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-xs-2">
                        <label for="tahapan">Tahapan</label>
                        <select class="form-control" name="tahapan1">
                            <option value="{{$tahapan1->id}}">{{$tahapan1->name}}</option>
                        </select>
                    </div>
                    <div class="col-xs-10">
                        <label for="bulan">Bulan</label>
                        <div class="form-group bulan-checkbox">
                            @foreach($bulan as $row)
                            <label><input type="checkbox" value="{{$row}}" class="bulan {{$row}}" name="bulan1[]" /><span class="text_{{$row}}">{{Helper::Month($row)}}</span></label>
                            @endforeach
                        </div>
                        {{-- <select class="form-control" name="bulan"> --}}
                            {{-- @foreach($bulan as $row) --}}
                                {{-- <option value="{{$row}}">{{Helper::Month($row)}}</option> --}}
                            {{-- @endforeach --}}
                        {{-- </select> --}}
                    </div>
                    
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-2">
                        <label for="tahapan">Tahapan</label>
                        <select class="form-control" name="tahapan2">
                            <option value="{{$tahapan2->id}}">{{$tahapan2->name}}</option>
                        </select>
                    </div>
                    <div class="col-xs-10">
                        <label for="bulan">Bulan</label>
                        <div class="form-group bulan-checkbox">
                            @foreach($bulan as $row)
                            <label><input type="checkbox" value="{{$row}}" class="bulan {{$row}}" name="bulan2[]" /><span class="text_{{$row}}">{{Helper::Month($row)}}</span></label>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-2">
                        <label for="tahapan">Tahapan</label>
                        <select class="form-control" name="tahapan3">
                            <option value="{{$tahapan3->id}}">{{$tahapan3->name}}</option>
                        </select>
                    </div>
                    <div class="col-xs-10">
                        <label for="bulan">Bulan</label>
                        <div class="form-group bulan-checkbox">
                            @foreach($bulan as $row)
                            <label><input type="checkbox" value="{{$row}}" class="bulan {{$row}}" name="bulan3[]" /><span class="text_{{$row}}">{{Helper::Month($row)}}</span></label>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/tahapan')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary please-waiting">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop

@section('end-script')
    @parent
    <script type="text/javascript">
    $( "input[type='checkbox']" ).prop('checked',false);
    $(document).on('click','.bulan',function(){
        var nilai = $(this).val();
        if($(this).prop('checked')){
            $('.'+nilai).not(this).attr('disabled','');
            // $('.text_'+nilai).not(this).css('text-decoration','line-through');
            console.log('checked');
        }else{
            $('.'+nilai).not(this).removeAttr('disabled');
            // $('.'+nilai+' span').not(this).css('text-decoration','inherit');
            console.log('unchecked');
        }
    });
    // $( "input[type='checkbox']" ).prop({
    //   disabled: true
    // });
    </script>
@stop