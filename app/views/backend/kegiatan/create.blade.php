@extends('backend.layouts.content')

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/kegiatan', 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="nama_program">Nama Program</label>
                        <select class="form-control" name="program">
                            @foreach($program as $row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>
                        <input type="text" name="nama_program" value="{{Input::old('nama_program')}}" class="form-control" id="nama_program">
                        {{$errors->first('nama_program','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="phone_number">User PA/KPA</label>
                        <input type="hidden" name="pilih_user" class="user-relasi" value="{{Input::old('pilih_user')}}">
                        <a class="form-control btn btn-default choose-user waiting">Pilih User PA/KPA</a>
                        {{$errors->first('pilih_user','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/kegiatan')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
<div id="open-modal"></div>
@stop