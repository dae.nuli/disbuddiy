@extends('backend.layouts.content')

@section('body-content')
@if(Session::has('about'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('about')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'admin/about', 'method'=>'POST', 'files'=>true))}}
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="name">Nama</label>
                        <input type="text" name="name" value="{{$about->name}}" class="form-control" id="name">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="email">Alamat Email</label>
                        <input type="text" name="email" value="{{$about->email}}" class="form-control" id="email">
                        {{$errors->first('email','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <label for="phone">Nomor Telefon</label>
                        <input type="text" name="phone_number" value="{{$about->phone}}" class="form-control" id="phone">
                        {{$errors->first('phone_number','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-4">
                        <label for="address">Alamat</label>
                        <input type="text" name="address" value="{{$about->address}}" class="form-control" id="address">
                        {{$errors->first('address','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-4">
                        <label for="nilai">Nilai</label>
                        <input type="text" name="nilai" value="{{$about->under}}" class="form-control" id="nilai">
                        {{$errors->first('nilai','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="body">Kata Kunci</label>
                <textarea id="body" name="web_keywords" class="form-control textarea" style="height:100px" rows="3">{{$about->web_keywords}}</textarea>
                {{$errors->first('web_keywords','<p class="text-red">:message</p>')}}
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary please-waiting">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop