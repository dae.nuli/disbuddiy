@extends('backend.layouts.content')
 
@section('body-content')
@if(count($errors))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b>
        @foreach ($errors->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </div>
@endif
@if(Session::has('program'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('program')}}.
    </div>
@endif
<div class="row">
   <div class="col-xs-12">
      <p class="lead">{{$program->year}} / {{$program->code_program}} / {{strtoupper($program->name)}}</p>
   </div>
</div>
        <div class="box">
            <div class="box-body">  
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="kode_program">Kode PA/KPA</label>
                                <input type="text" value="{{$kpa->code_kpa}}" autocomplete="off" class="form-control" readonly="">
                                
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="nama_program">Nama Instansi</label>
                                <input type="text" value="{{$kpa->instansi_name}}" autocomplete="off" class="form-control" readonly="">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="tahun">Nomor Telefon</label>
                                <input type="text" value="{{$kpa->phone}}" autocomplete="off" class="form-control" readonly="">
                            </div>
                        </div>
                    </div>
            </div><!-- /.box-body -->
        </div>


        <!-- general form elements -->
        {{-- <h2 class="page-header">Kegiatan</h2> --}}
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title pull-left">Kegiatan</h3>
                <div class="pull-right" style="margin-left:5px">
                    <a style="margin:10px 10px 0 10px;color:white" class="btn btn-sm btn-primary add-kegiatan-kpa waiting" data-program="{{$program->id}}" data-kpa="{{$kpa->id}}">Tambah Kegiatan</a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Kode Kegiatan</th>
                    <th>Nama Kegiatan</th>
                    <th>Pagu Awal</th>
                    <th>Pagu Perubahan</th>
                    <th>Keterangan</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <?php $nomor=1; ?>
                    @foreach($kegiatan as $row)
                    <tr>
                        <td>{{$nomor++}}</td>
                        <td>{{$row->kegiatan->code_kegiatan}}</td>
                        <td>{{$row->kegiatan->name}}</td>
                        <td>Rp {{number_format($row->pagu_kegiatan,0,",",".")}}</td>
                        <td>{{($row->pagu_kegiatan_perubahan?'Rp '.number_format($row->pagu_kegiatan_perubahan,0,",","."):'-')}}</td>
                        <td>{{($row->keterangan?$row->keterangan:'-')}}</td>
                        <td>
                            <a href="{{URL::to('admin/program/detail-sub-kegiatan/'.$kpa->id.'/'.$row->id_kegiatan)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-eye"></i> Detail</a>
                            <a class="btn btn-info btn-xs edit-pagu-kegiatan" data-id="{{$row->id}}" data-program="{{$program->id}}" data-kpa="{{$kpa->id}}"><i class="fa fa-fw fa-edit"></i> Ubah</a>
                            <a href="{{URL::to('admin/program/delete-sync-kegiatan/'.$row->id.'/'.$kpa->id.'/'.$program->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->


        <div class="box-footer">
            <a href="{{URL::to('admin/program/sync/'.$program->id)}}" class="btn btn-default">{{trans('button.bc')}}</a>
        </div>
<div id="open-modal"></div>
@stop