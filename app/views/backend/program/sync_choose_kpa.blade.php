@extends('backend.layouts.content')

@section('header-content')

{{Form::open(array('url'=>'admin/program/sync-user/'.$program->id, 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Pencarian instansi">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('kpa'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('kpa')}}.
    </div>
@endif
<div class="row">
   <div class="col-xs-12">
      <p class="lead">{{$program->year}} / {{$program->code_program}} / {{strtoupper($program->name)}}</p>
   </div>
</div>
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Kode PA/KPA</th>
                <th>Nama Instansi</th>
                <th>Nomor Telefon</th>
                <th>Status</th>
                <th>Register</th>
                <th></th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($kpa as $row)
            <tr>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$nomor++}}.</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->code_kpa}}</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->instansi_name}}</td>
                <td class="detail-kpa please-waiting" data-iduser="{{$row->id}}">{{$row->phone}}</td>
                <td>{{($row->is_active)?'<span class="label label-success">Active</span>':'<span class="label label-danger">Not Active</span>'}}</td>
                <td>{{date('d F Y, H:i:s',strtotime($row->created_at))}}</td>
                <td> 
                    <a href="{{URL::to('admin/program/sync-kegiatan/'.$row->id.'/'.$program->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-plus-circle"></i> Pilih</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$kpa->links()}}
    </div>
</div>
<div class="box-footer">
    <a href="{{URL::to('admin/program/sync/'.$program->id)}}" class="btn btn-default">{{trans('button.bc')}}</a>
</div>
<div id="open-modal"></div>
@stop