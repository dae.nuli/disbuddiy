<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>    
<link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
<script src="{{asset('assets/css/select2.js')}}"></script>
<script type="text/javascript">
$('.nama_instansi').select2({
    theme: "classic"
});
$.validate({
    form : '#request',
    onSuccess : function() {
        waiting();
    }
});
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {{Form::open(array('url'=>'admin/program/modal-add-user/'.$program->id, 'method'=>'POST','id'=>'request'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah PA/KPA</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Nama Instansi</label>
                                <select class="form-control nama_instansi" style="padding:0;" name="nama_instansi" data-validation="required" data-validation-error-msg="Form nama instansi harus diisi." data-validation-event="keyup">
                                        <option value="">- Pilih Instansi -</option>
                                    @foreach($kpa as $row)
                                        <option value="{{$row->id}}">{{$row->instansi_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">{{trans('button.cl')}}</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>