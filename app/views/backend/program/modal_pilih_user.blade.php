<script type="text/javascript">
$(function() {
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
    var relasi = $('.user-relasi').val();
    if(relasi!=''){
        var facil = relasi.split(',');
        $.each(facil,function(index, row) {
            $('input[type=checkbox][value='+row+']').prop('checked', true);
        });
    }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-pilih-kpa" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Pilih PA/KPA</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <div class="row">
                            @foreach($userKPA as $row)
                            <div class="col-xs-4"><input type="checkbox" class="nama-kpa" value="{{$row->id}}"> {{$row->instansi_name}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-pilih" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>