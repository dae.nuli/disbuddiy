<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>    
<script type="text/javascript">
$.validate({
    form : '#request',
    onSuccess : function() {
        waiting();
    }
});
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {{Form::open(array('url'=>'admin/program/edit-pagu-kegiatan/'.$pagu->id.'/'.$program.'/'.$kpa, 'method'=>'POST','id'=>'request'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Kegiatan</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Pagu Awal</label>
                                <input class="form-control autocomma" autocomplete="off" name="pagu_awal" value="{{number_format($pagu->pagu_kegiatan,0,",",",")}}" data-validation="required" data-validation-error-msg="Form pagu awal harus diisi dengan angka." data-validation-event="keyup">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Pagu Perubahan</label>
                                <input class="form-control autocomma" autocomplete="off" name="pagu_perubahan" value="{{($pagu->pagu_kegiatan_perubahan)?number_format($pagu->pagu_kegiatan_perubahan,0,",",","):''}}" data-validation-optional="true" data-validation="required" data-validation-error-msg="Form pagu perubahan harus diisi dengan angka." data-validation-event="keyup">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Keterangan</label>
                                <input class="form-control" autocomplete="off" name="keterangan" value="{{$pagu->keterangan}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">{{trans('button.cl')}}</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>