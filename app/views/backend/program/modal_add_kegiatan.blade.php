<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>    
<link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
<script src="{{asset('assets/css/select2.js')}}"></script>
<script type="text/javascript">
$.validate({
    form : '#request',
    onSuccess : function() {
        waiting();
    }
});
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {{Form::open(array('url'=>'admin/program/add-kegiatan/'.$idProgram, 'method'=>'POST','id'=>'request'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Kegiatan</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Kode Kegiatan</label>
                                        <input class="form-control" autocomplete="off" name="kode_kegiatan" data-validation="number" data-validation-error-msg="Form kode kegiatan harus diisi dengan angka." data-validation-event="keyup">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Nama Kegiatan</label>
                                        <input class="form-control" autocomplete="off" name="nama_kegiatan" data-validation="required" data-validation-error-msg="Form nama kegiatan harus diisi." data-validation-event="keyup">
                                    </div>
                                </div>
                            </div>
                        {{-- <div class="form-group">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Pagu Kegiatan</label>
                                    <input class="form-control" autocomplete="off" name="pagu_kegiatan">
                                </div>
                                <div class="col-xs-6">
                                    <label>Keterangan</label>
                                    <input class="form-control" autocomplete="off" name="keterangan">
                                </div>
                            </div>
                        </div> --}}
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">{{trans('button.cl')}}</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>