<script type="text/javascript">
$(function() {
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">User KPA</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>Nama Instansi</label>
                                <input class="form-control" disabled="" value="{{$kpa->instansi_name}}">
                            </div>
                            <div class="col-xs-4">
                                <label>Bidang</label>
                                <input class="form-control" disabled="" value="{{isset($kpa->bidang)?$kpa->bidang:'-'}}">
                            </div>
                            <div class="col-xs-4">
                                <label>Nama Personil</label>
                                <input class="form-control" disabled="" value="{{isset($kpa->personil_name)?$kpa->personil_name:'-'}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Nomor Telefon</label>
                                <input class="form-control" disabled="" value="{{$kpa->phone}}">
                            </div> 
                            <div class="col-xs-6">
                                <label>Alamat Email</label>
                                <input class="form-control" disabled="" value="{{$kpa->email}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="note">Alamat Kantor</label>
                        <textarea class="form-control" disabled="" rows="3">{{$kpa->address}}</textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default" data-dismiss="modal">{{trans('button.cl')}}</button>
            </div>
        </div>
    </div>
</div>