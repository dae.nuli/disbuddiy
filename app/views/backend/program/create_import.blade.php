@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>
    <script type="text/javascript">
     $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
            // $('.wait').addClass('disabled');
            // $('.wait').html('Waiting ...');
        }
    });
    $(function() {
        $('.tahun').select2({
            theme: "classic"
        });
    });
 
    </script>
    <script type="text/javascript">
        (function($){
            $.fn.valInc = function(vadd)
            {
                var val = $(this).val() + '' + vadd;
                $(this).val(val);
            }
        })(jQuery);

        $(document).on('click','.nama-kpa',function(){
            var rel = $('.user-relasi').val();
            var idPel = $(this).val();
            if($(this).is(':checked')){   
                if(rel==''){
                    $('.user-relasi').val(','+idPel);
                }else{
                    $('.user-relasi').valInc(','+idPel);
                }
            }else{
                $('.user-relasi').val(function(index,value){
                    return value.replace(','+idPel,'');
                });
            }
        });
        $('#request').on("keyup keypress", function(e) {
          var code = e.keyCode || e.which; 
          if (code  == 13) {               
            e.preventDefault();
            return false;
          }
        });
        var max_fields      = 21; //maximum input boxes allowed
        var wrapper         = $(".add-dinamic"); //Fields wrapper
        var add_button      = $(".add-item"); //Add button ID
        
        var x = {{count($kegiatan)+1}}; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<tr>'+
                '<td><input type="text" name="kode[]" class="form-control" autocomplete="off" data-validation="number" data-validation-error-msg="Form kode kegiatan harus diisi dengan angka."/></td>'+
                '<td><input type="text" name="name[]" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="Form nama kegiatan harus diisi."/></td>'+
                // '<td><input name="pagu[]" class="form-control" autocomplete="off" data-validation="number" data-validation-error-msg="Form pagu kegiatan harus diisi dengan angka."/></td>'+
                '<td><input name="description[]" class="form-control" autocomplete="off"/></td>'+
                '<td><a href="" class="btn btn-danger btn-xs delete-item"><i class="fa fa-fw fa-trash-o"></i> Hapus</a></td>'+
            '</tr>'); //add input box
            }
        });
        
        $(document).on("click",".delete-item", function(e){ //user click on remove text
            e.preventDefault(); $(this).parents('.add-dinamic tr').remove(); x--;
        })
    </script>

@stop

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a class="btn btn-primary import-program">Import</a>
</div>

@stop

@section('body-content')
    {{Form::open(array('url'=>'admin/program', 'id'=>'request', 'method'=>'POST'))}}
        <div class="box">
            <div class="box-body">  
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="kode_program">Kode Program</label>
                                <input type="text" name="kode_program" autocomplete="off" value="{{$program->code_program}}" class="form-control" id="kode_program" data-validation="number" data-validation-error-msg="Form kode program harus diisi dengan angka." data-validation-event="keyup">
                                {{$errors->first('kode_program','<p class="text-red">:message</p>')}}
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="nama_program">Nama Program</label>
                                <input type="text" name="nama_program" autocomplete="off" value="{{$program->name}}" class="form-control" id="nama_program" data-validation="required" data-validation-error-msg="Form nama program harus diisi." data-validation-event="keyup">
                                {{$errors->first('nama_program','<p class="text-red">:message</p>')}}
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="tahun">Tahun</label>
                                <select name="tahun" class="form-control tahun" style="padding:0;" id="tahun">
                                    @foreach($yearRange as $row)
                                    <option value="{{$row}}">{{$row}}</option>
                                    @endforeach
                                </select>
                                {{$errors->first('tahun','<p class="text-red">:message</p>')}}
                            </div>
                        </div>
                    </div>
            </div><!-- /.box-body -->
        </div>


        <!-- general form elements -->
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title pull-left">Kegiatan</h3>
                <div class="pull-right" style="margin-left:5px">
                    <a style="margin:10px 10px 0 10px;color:white" class="btn btn-sm btn-primary add-item">Tambah Kegiatan</a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Kode Kegiatan</th>
                    <th>Nama Kegiatan</th>
                    <th>Keterangan</th>
                    <th></th>
                </tr>
                </thead>
                <tbody class="add-dinamic">
                    @foreach($kegiatan as $val)
                        <tr><td><input type="text" name="kode[]" class="form-control" autocomplete="off" value="{{$val->code_kegiatan}}" data-validation="number" data-validation-error-msg="Form kode kegiatan harus diisi dengan angka."></td><td><input type="text" name="name[]" class="form-control" autocomplete="off" value="{{$val->name}}" data-validation="required" data-validation-error-msg="Form nama kegiatan harus diisi."></td><td><input name="description[]" value="{{$val->description}}" class="form-control" autocomplete="off"></td><td><a href="" class="btn btn-danger btn-xs delete-item"><i class="fa fa-fw fa-trash-o"></i> Hapus</a></td></tr>
                    @endforeach
                </tbody>
            </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->


        <div class="box-footer">
            <a href="{{URL::to('admin/program')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
<div id="open-modal"></div>
@stop