<script type="text/javascript" src="{{asset('assets/js/jquery.autocomplete.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/autocomplete.min.css')}}">
<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script> 
<script src="{{asset('assets/js/AdminLTE/security.js')}}"></script> 
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
    form : '#request',
    modules : 'security',
    onSuccess : function() {
      waiting();
    }
});
// $(document).on('keyup click','.kode_kpa',function(){
//     var kode = $('.kode_kpa').val();
//     console.log(kode);
// });
$('#autocomplete').autocomplete({
    type:'POST',
    lookupLimit:10,
    minChars:2,
    serviceUrl: '{{URL::to("admin/program/find-user/".$idProgram)}}',
    onSelect: function (suggestion) {
        console.log(suggestion)
        if(suggestion!=""){
            if(suggestion.value!="" && suggestion.name!=""){
                $('.nama_instansi').val(suggestion.name);
                $('.nama_personil').val(suggestion.kode);
                console.log(suggestion.value);
                // $(this).find('.nama_personil').val(suggestion.personil);
                // console.log('You selected: ' + suggestion.value + ', ' + suggestion.name+', '+suggestion.kode);
            }
        }
    },
    onSearchComplete: function (query, suggestions) {
        // console.log('popopo : '+query);
        // console.log(suggestions);
        // if(suggestions!=""){
        //     console.log('ada');
        // }else{
        //     console.log('kosong');
        // }

    },
    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
        // console.log(query);
    },
    onSearchStart: function(query){
    //     var kode = $('.kode_kpa').val();
    // console.log(kode);
    //     $.ajax({
    //         url: '{{URL::to("admin/program/check-user/".$idProgram)}}',
    //         type: 'POST',
    //         dataType:'json',
    //         data: {code: kode},
    //     })
    //     .done(function(data) {
    //     })
    //     .fail(function() {
    //         console.log("error");
    //     })
    //     .always(function() {
    //         console.log("complete");
    //     });
    },
    onInvalidateSelection: function () {
        // console.log('hello');
        $('.kode_kpa').val('');
        $('.nama_instansi').val('');
        $('.nama_personil').val('');
    },
    beforeRender:function(container){
        // console.log('searching...');
    }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {{Form::open(array('url'=>'admin/program/add-user/'.$idProgram, 'id'=>'request', 'method'=>'POST'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Pilih PA/KPA</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Kode PA/KPA</label>
                                <input class="form-control kode_kpa" name="kode_kpa" id="autocomplete" data-validation="server" data-validation-url='{{URL::to("admin/program/check-user/".$idProgram)}}' data-validation-req-params="">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            {{-- <div class="form-group"> --}}
                                <label>Nama Instansi</label>
                                <input class="form-control nama_instansi" readonly="">
                                {{-- <input class="form-control nama_instansi" data-validation="required" data-validation-error-msg="Form kode" data-validation-error-msg-container=".errors" readonly=""> --}}
                            {{-- </div> --}}
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Nama Penanggung Jawab</label>
                                <input class="form-control nama_personil" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            {{-- <div class="form-group errors">
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">{{trans('button.cl')}}</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>