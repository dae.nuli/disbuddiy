@extends('backend.layouts.content')
@section('end-script')
    @parent

    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>
    <script type="text/javascript">
     $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    $(function() {
        $('.tahun').select2({
            theme: "classic"
        });
    });
 
    </script>
@stop
@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/program/update/'.$program->id, 'method'=>'POST', 'id'=>'request'))}}
        <div class="box-body">  
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="kode_program">Kode Program</label>
                        <input type="text" name="kode_program" autocomplete="off" value="{{$program->code_program}}" class="form-control" id="kode_program" data-validation="number" data-validation-error-msg="Form kode program harus diisi dengan angka." data-validation-event="keyup">
                        {{$errors->first('kode_program','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="nama">Nama Program</label>
                        <input type="text" name="nama_program" autocomplete="off" value="{{$program->name}}" class="form-control" id="name" data-validation="required" data-validation-error-msg="Form nama program harus diisi." data-validation-event="keyup">
                        {{$errors->first('nama_program','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="year">Tahun</label>
                        <select name="tahun" class="form-control tahun" style="padding:0;" id="tahun">
                            @foreach($yearRange as $row)
                                <option value="{{$row}}" {{($program->year==$row)?'selected':''}}>{{$row}}</option>
                            @endforeach
                        </select>
                        {{$errors->first('year','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/program')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop