@extends('backend.layouts.content')

@section('body-content')
<div class="row">
   <div class="col-xs-12">
    <div class="callout callout-info">

        <h5>{{$program->year}} / 3.17.{{$user}}.{{$program->code_program}} / {{$program->name}}</h5>
        <h4>3.17.{{$user}}.{{$program->code_program}}.{{$kegiatan->code_kegiatan}} / {{$kegiatan->name}} / Rp {{number_format($new->pagu_kegiatan,0,",",".")}}</h4>
        <p>{{$kegiatan->description}}</p>
    </div>
   </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box no-border">
        <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover item-table">
            <thead style="background-color: #B9B9B9;">
                <tr>
                    <th rowspan="2">Tahap</th>
                    <th rowspan="2">Bulan</th>
                    <th rowspan="2">Sub Kegiatan</th>
                    <th rowspan="2">Volume</th>
                    <th rowspan="2">Satuan</th>
                    <th colspan="2">Fisik</th>
                    <th colspan="3">Anggaran</th>
                    <th rowspan="2">Keterangan</th>
                    <th rowspan="2">Progress</th>
                </tr>
                <tr>
                    <th>Rencana (%)</th>
                    <th>Realisasi (%)</th>
                    <th>Rencana</th>
                    <th>SP2D</th>
                    <th>SPJ</th>
                </tr>
            </thead>
        <tbody>
        <?php
        $ra[]   = '';
        $sp2d[] = '';
        $spj[]  = '';
        ?>
         @foreach($item as $row)
         <?php
         $ra[]   = ($row->anggaran_rencana?$row->anggaran_rencana:0);
         $sp2d[] = ($row->anggaran_sppd?$row->anggaran_sppd:0);
         $spj[]  = ($row->anggaran_spj?$row->anggaran_spj:0);
         ?>
         <tr class="edit-item" data-sub="{{Helper::en($row->id)}}">
            <td>Tahap {{$row->tahap->id_tahap}}</td>
            <td>{{Helper::Month($row->tahap->month)}}</td>
            <td>{{($row->name?$row->name:'-')}}</td>
            <td>{{($row->vol?$row->vol:'-')}}</td>
            <td>{{($row->satuan?$row->satuan:'-')}}</td>
            <td>{{($row->fisik_rencana?$row->fisik_rencana:'-')}}</td>
            <td>{{($row->fisik_realisasi?$row->fisik_realisasi:'-')}}</td>
            <td>{{($row->anggaran_rencana?'Rp '.number_format($row->anggaran_rencana,0,",","."):'-')}}</td>
            <td>{{($row->anggaran_sppd?'Rp '.number_format($row->anggaran_sppd,0,",","."):'-')}}</td>
            <td>{{($row->anggaran_spj?'Rp '.number_format($row->anggaran_spj,0,",","."):'-')}}</td>
            <td>{{($row->keterangan?$row->keterangan:'-')}}</td>
            <td>
              {{(!empty($row->fisik_rencana) && !empty($row->fisik_realisasi))?'<span class="badge bg-green" data-toggle="tooltip" data-html="true" data-placement="top" title="FISIK">'.floor(($row->fisik_realisasi/$row->fisik_rencana)*100).'%</span>':''}} 
              {{(!empty($row->anggaran_sppd)&&!empty($row->anggaran_rencana))?'<span class="badge bg-yellow" data-toggle="tooltip" data-html="true" data-placement="top" title="SP2D">'.floor(($row->anggaran_sppd/$row->anggaran_rencana)*100).'%</span>':''}}
              {{(!empty($row->anggaran_spj)&&!empty($row->anggaran_rencana))?'<span class="badge bg-light-blue" data-toggle="tooltip" data-html="true" data-placement="top" title="SPJ">'.floor(($row->anggaran_spj/$row->anggaran_rencana)*100).'%</span>':''}}
            </td>
         </tr>
         @endforeach
      </tbody>
      <tfoot style="background-color: darkgray;">
        <tr>
          <th style="text-align:center">TOTAL</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          @if(array_sum($ra)<=$new->pagu_kegiatan)
            <th>Rp {{(count($ra)>0)?number_format(array_sum($ra),0,",","."):'0'}}</th>
          @else
            <th>Rp {{(count($ra)>0)?'<span class="text-red">'.number_format(array_sum($ra),0,",",".").'</span>':'0'}}</th>
          @endif
          <th>Rp {{(count($sp2d)>0)?number_format(array_sum($sp2d),0,",","."):'0'}}</th>
          <th>Rp {{(count($spj)>0)?number_format(array_sum($spj),0,",","."):'0'}}</th>
          <th></th>
          <th></th>
        </tr>
      </tfoot>
   </table>
  </div><!-- /.box-body -->
  <div class="box-footer">
      <a href="{{URL::to('admin/program/sync-kegiatan/'.$kpa->id.'/'.$program->id)}}" class="btn btn-default">{{trans('button.bc')}}</a>
  </div>
</div><!-- /.box -->
</div>
</div>
@stop

@section('end-script')
    @parent
    <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
    })
    </script>
@stop