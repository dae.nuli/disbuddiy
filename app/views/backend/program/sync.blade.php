@extends('backend.layouts.content')

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a class="btn btn-primary tambah-kpa" data-program="{{$program->id}}">Tambah KPA</a>
    {{-- <a href="{{URL::to('admin/program/sync-user/'.$program->id)}}" class="btn btn-primary">Tambah KPA</a> --}}
</div>
@stop

@section('body-content')
@if(Session::has('kpa'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('kpa')}}.
    </div>
@endif

@if(Session::has('kpa_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('kpa_alert')}}.
    </div>
@endif
<div class="row">
   <div class="col-xs-12">
      <p class="lead">{{$program->year}} / {{$program->code_program}} / {{strtoupper($program->name)}}</p>
   </div>
</div>
<div class="box">
    {{-- <div class="box-header"> --}}
        {{-- <h3 class="box-title pull-left">{{$program->name}}</h3> --}}
        {{-- <div class="pull-right" style="margin-left:5px">
            <a style="margin:10px 10px 0 10px;color:white" class="btn btn-sm btn-primary sync waiting" data-program="{{$program->id}}">Tambah KPA</a>
        </div> --}}
    {{-- </div> --}}
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Kode KPA</th>
                <th>Nama Instansi</th>
                <th>Alamat Email</th>
                <th>Total Kegiatan</th>
                <th></th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($userProgram as $row)
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->userkpa->code_kpa}}</td>
                <td><a class="detail-kpa-program" data-iduser="{{$row->userkpa->id}}">{{$row->userkpa->instansi_name}}</a></td>
                <td>{{($row->userkpa->email?$row->userkpa->email:'-')}}</td>
                <td>{{Helper::countKegiatan($row->id_program,$row->userkpa->id)}}</td>
                <td>
                    <a href="{{URL::to('admin/program/sync-kegiatan/'.$row->userkpa->id.'/'.$row->id_program)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-file-o"></i> Kegiatan</a>
                    {{-- <a href="{{URL::to('admin/program/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Ubah</a> --}}
                    <a href="{{URL::to('admin/program/delete-sync/'.$row->id_program.'/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> {{trans('button.dl')}}</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$userProgram->links()}}
    </div>
</div>
<div class="box-footer">
    <a href="{{URL::to('admin/program')}}" class="btn btn-default">{{trans('button.bc')}}</a>
</div>
<div id="open-modal"></div>
@stop