@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
     $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/program/edit-kegiatan/'.$kegiatan->id_program.'/'.$kegiatan->id, 'method'=>'POST', 'id'=>'request'))}}
        <div class="box-body">  
            <div class="form-group">
                <label for="kode_kegiatan">Kode Kegiatan</label>
                <input type="text" name="kode_kegiatan" autocomplete="off" value="{{$kegiatan->code_kegiatan}}" class="form-control" id="kode_kegiatan" data-validation="number" data-validation-error-msg="Form kode kegiatan harus diisi dengan angka." data-validation-event="keyup">
                {{$errors->first('kode_kegiatan','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="nama_kegiatan">Nama Kegiatan</label>
                <input type="text" name="nama_kegiatan" autocomplete="off" value="{{$kegiatan->name}}" class="form-control" id="nama_kegiatan" data-validation="required" data-validation-error-msg="Form nama kegiatan harus diisi." data-validation-event="keyup">
                {{$errors->first('nama_kegiatan','<p class="text-red">:message</p>')}}
            </div>
            {{-- <div class="form-group">
                <label for="pagu_kegiatan">Pagu Kegiatan</label>
                <input type="text" name="pagu_kegiatan" autocomplete="off" value="{{$kegiatan->pagu_kegiatan}}" class="form-control" id="pagu_kegiatan">
                {{$errors->first('pagu_kegiatan','<p class="text-red">:message</p>')}}
            </div> --}}
            <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <input type="text" name="keterangan" autocomplete="off" value="{{$kegiatan->description}}" class="form-control" id="keterangan">
                {{$errors->first('keterangan','<p class="text-red">:message</p>')}}
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/program/detail/'.$kegiatan->id_program)}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop