@extends('backend.layouts.content')

@section('end-script')
    @parent

    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>
    <script type="text/javascript">
    $('.tahun').select2({
        theme: "classic"
    });
    </script>
@stop

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/program/create')}}" class="btn btn-primary">Tambah</a>
</div>
{{Form::open(array('url'=>'admin/program', 'class'=>'pull-right', 'style'=>'width:19%','method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" autocomplete="off" class="form-control pull-right" style="width: 150px;" placeholder="Cari program">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}

{{Form::open(array('url' => 'admin/program/year', 'class'=>'pull-right', 'method' => 'post'))}}
<select class="form-control tahun" style="padding:0;" name="year" onchange="this.form.submit()">
    @foreach($yearRange as $row)
        <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
    @endforeach
</select>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('program'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('program')}}.
    </div>
@endif

@if(Session::has('program_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('program_alert')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Total Kegiatan</th>
                <th>Total KPA</th>
                <th>Total Pagu</th>
                <th>Tahun</th>
                <th></th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($program as $row)
            <?php 
            $total        = Helper::KegiatanTotal($row->id);
            $totalProgram = Helper::TotalProgram($row->id);
            $totalPagu    = Helper::totalPagu($row->id);
            ?>
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->code_program}}</td>
                <td><a href="{{URL::to('admin/program/detail/'.$row->id)}}">{{$row->name}}</a></td>
                <td>{{$total}}</td>
                <td>{{$totalProgram}}</td>
                <td>Rp {{number_format($totalPagu,0,",",".")}}</td>
                <td>{{$row->year}}</td>
                <td>
                    <a href="{{URL::to('admin/program/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Ubah</a>
                    <a href="{{URL::to('admin/program/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Hapus</a>
                    <a href="{{URL::to('admin/program/sync/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-share"></i> Sync</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$program->links()}}
    </div>
</div>
<div id="open-modal"></div>
@stop