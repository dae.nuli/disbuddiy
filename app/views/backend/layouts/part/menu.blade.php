<!-- sidebar menu: : style can be found in sidebar.less -->
<?php
    $uri3  = Request::segment(3);
    $uri   = Request::segment(2);
    $uri1  = Request::segment(1);
    $admin = Session::get('admin');
?>
                    <ul class="sidebar-menu">
                        <li @if($uri1=='admin' && $uri=='home') class="active" @endif>
                            <a href="{{URL::to('admin')}}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        @if($admin['level']==1)
                        
                        <li @if($uri=='program') class="active" @endif>
                            <a href="{{URL::to('admin/program')}}">
                                <i class="fa fa-file"></i> <span>Program & Kegiatan</span>
                            </a>
                        </li>
                        
                        <li @if($uri=='kpa') class="active" @endif>
                            <a href="{{URL::to('admin/kpa')}}">
                                <i class="fa fa-users"></i> <span>PA/KPA</span>
                            </a>
                        </li>

                        <li @if($uri=='tahapan') class="active" @endif>
                            <a href="{{URL::to('admin/tahapan')}}">
                                <i class="fa fa-calendar"></i> <span>Tahapan</span>
                            </a>
                        </li>
                        <li @if($uri=='pengaduan') class="active" @endif>
                            <a href="{{URL::to('admin/pengaduan')}}">
                                <i class="fa fa-comments"></i> <span>Pengaduan</span>
                            </a>
                        </li>
                        <li @if($uri=='about') class="active" @endif>
                            <a href="{{URL::to('admin/about')}}">
                                <i class="fa fa-cog"></i> <span>Pengaturan</span>
                            </a>
                        </li>
                        
                        @else
                        {{Permission::ShowMenu($admin['level'],$uri,$uri1)}}
                        @endif
                    </ul>