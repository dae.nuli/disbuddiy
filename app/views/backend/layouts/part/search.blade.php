<!-- Sidebar user panel -->
<?php $admin = Session::get('admin'); ?>
                    <div class="user-panel">
                        <div class="pull-left image" style="background-color: #DADADA;padding: 12px;font-size: 13px;border-radius: 50%;">
                            <b class="img-circle">{{Helper::avatar($admin['name'])}}</b>
                            {{-- <img src="{{asset('assets/img/avatar3.png')}}" class="img-circle" alt="User Image" /> --}}
                        </div>
                        <div class="pull-left info">
                            <p>Hello, {{$admin['name']}}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->