<?php
class Helper {
	public static function count_views($id,$type)
	{
		return VisitorModel::where('id_article',$id)->where('type',$type)->count();
	}

 	public static function about()
	{
		return AboutModel::find(1);
	} 
	
	public static function GroupTotal($idGroup)
	{
		return UserModel::where('level',$idGroup)->count();
	}

	public static function CheckGroupName($idGroup)
	{
		return UsersGroupModel::find($idGroup)->group_name;
	}
 
	// DIY
	public static function findTahap($bulan,$tahun)
	{
		return DetailTahapModel::where('month',$bulan)->where('year',$tahun)->first();
	}

	public static function avatar($text)
	{
		$exp = explode(' ', $text);
		if(count($exp) > 2){
			return strtoupper($exp[0][0].$exp[1][0]);
		}else{
			return strtoupper($exp[0][0].$exp[0][1]);
		}
	}

	public static function unders()
	{
		// $ikm = ItemKegiatanModel::where('id_user',$idUser)->where('id_kegiatan',$idKegiatan)->get();
		// if(count($ikm)>0){
		// 	foreach ($ikm as $key => $row) {
		// 		if(empty($row->fisik_realisasi)){
		// 			$realisasi = 0;
		// 		}else{
		// 			$realisasi = $row->fisik_realisasi;
		// 		}
		// 		$progress[] = floor(($realisasi/$row->fisik_rencana)*100);
		// 	}
		// 	$total = array_sum($progress);
		// 	return ceil($total/count($ikm));
		// }else{
		// 	return 0;
		// }
		$nilaiUnder = Self::underValue();

		$pm = ProgramModel::where('year',date('Y'))->get();
		$program = '';
		foreach ($pm as $val) {
			$program[] = $val->id;
		}
		$kegiatan = '';
		if(!empty($program)){
			$km = KegiatanModel::whereIn('id_program',$program)->get();
			foreach ($km as $nilai) {
				$kegiatan[] = $nilai->id;
			}
		}
		// return $kegiatan;
		if(!empty($kegiatan)){
			$ikm = ItemKegiatanModel::whereIn('id_kegiatan',$kegiatan)->orderBy('id_user','asc')->get();
			$data = '';
			foreach ($ikm as $key => $row) {
				if(!empty($row->fisik_realisasi)){
					$fisik_realisasi = $row->fisik_realisasi;
				}else{
					$fisik_realisasi = 0;
				}

				if(!empty($row->anggaran_sppd)){
					$anggaran_sppd = $row->anggaran_sppd;
				}else{
					$anggaran_sppd = 0;
				}

				if(!empty($row->anggaran_spj)){
					$anggaran_spj = $row->anggaran_spj;
				}else{
					$anggaran_spj = 0;
				}
				$porsentase_fisik = floor(($fisik_realisasi/$row->fisik_rencana)*100);
				$porsentase_sppd  = floor(($anggaran_sppd/$row->anggaran_rencana)*100);
				$porsentase_spj   = floor(($anggaran_spj/$row->anggaran_rencana)*100);
				if(($porsentase_fisik <= $nilaiUnder) || ($porsentase_spj <= $nilaiUnder) || ($porsentase_sppd <= $nilaiUnder)){
					$data[]      = $row->id_user;
				}
			}
			return $data;
		}else{
			return 0;
		}

		// $pm      = ProgramModel::where('year',date('Y'))->get();
		// $program = '';
		// foreach ($pm as $val) {
		// 	$program[] = $val->id;
		// }
		// $km       = KegiatanModel::whereIn('id_program',$program)->get();
		// $kegiatan = '';
		// foreach ($km as $nilai) {
		// 	$kegiatan[] = $nilai->id;
		// }
		// $ikm  = ItemKegiatanModel::whereIn('id_kegiatan',$kegiatan)->orderBy('id_user','asc')->get();
		// $data = '';
		// $tot  = count($ikm);
		// foreach ($ikm as $key => $row) {
		// 	if(empty($row->fisik_realisasi)){
		// 		$fisik_realisasi = 0;
		// 	}else{
		// 		$fisik_realisasi = $row->fisik_realisasi;
		// 	}
		// 	$porsentase[] = floor(($fisik_realisasi/$row->fisik_rencana)*100);
		// }
		// return array_sum($porsentase);
	}
	public static function under_kegiatan($kpa)
	{
		$nilaiUnder = Self::underValue();

		$pm = ProgramModel::where('year',date('Y'))->get();
		$program = '';
		foreach ($pm as $val) {
			$program[] = $val->id;
		}
		$km = KegiatanModel::whereIn('id_program',$program)->get();
		$kegiatan = '';
		foreach ($km as $nilai) {
			$kegiatan[] = $nilai->id;
		}
		$ikm = ItemKegiatanModel::whereIn('id_kegiatan',$kegiatan)->where('id_user',$kpa)->get();
		$data = '';
		foreach ($ikm as $key => $row) {
			if(empty($row->fisik_realisasi)){
				$fisik_realisasi = 0;
			}else{
				$fisik_realisasi = $row->fisik_realisasi;
			}

			if(empty($row->anggaran_sppd)){
				$anggaran_sppd = 0;
			}else{
				$anggaran_sppd = $row->anggaran_sppd;
			}

			if(empty($row->anggaran_spj)){
				$anggaran_spj = 0;
			}else{
				$anggaran_spj = $row->anggaran_spj;
			}
			$porsentase_fisik = floor(($fisik_realisasi/$row->fisik_rencana)*100);
			$porsentase_sppd  = floor(($anggaran_sppd/$row->anggaran_rencana)*100);
			$porsentase_spj   = floor(($anggaran_spj/$row->anggaran_rencana)*100);
			if(($porsentase_fisik <= $nilaiUnder) || ($porsentase_spj <= $nilaiUnder) || ($porsentase_sppd <= $nilaiUnder)){
				$data[]      = self::findProgramByKegiatan($row->id_kegiatan);
			}
		}
		return $data;
	}

	public static function under_kegiatan_count($program,$kpa)
	{
		$nilaiUnder = Self::underValue();

		// $pm = ProgramModel::where('year',date('Y'))->get();
		// $program = '';
		// foreach ($pm as $val) {
		// 	$program[] = $val->id;
		// }
		$km = KegiatanModel::where('id_program',$program)->get();
		$kegiatan = '';
		foreach ($km as $nilai) {
			$kegiatan[] = $nilai->id;
		}
		$ikm = ItemKegiatanModel::whereIn('id_kegiatan',$kegiatan)->where('id_user',$kpa)->get();
		$data = '';
		foreach ($ikm as $key => $row) {
			if(empty($row->fisik_realisasi)){
				$fisik_realisasi = 0;
			}else{
				$fisik_realisasi = $row->fisik_realisasi;
			}

			if(empty($row->anggaran_sppd)){
				$anggaran_sppd = 0;
			}else{
				$anggaran_sppd = $row->anggaran_sppd;
			}

			if(empty($row->anggaran_spj)){
				$anggaran_spj = 0;
			}else{
				$anggaran_spj = $row->anggaran_spj;
			}
			$porsentase_fisik = floor(($fisik_realisasi/$row->fisik_rencana)*100);
			$porsentase_sppd  = floor(($anggaran_sppd/$row->anggaran_rencana)*100);
			$porsentase_spj   = floor(($anggaran_spj/$row->anggaran_rencana)*100);
			if(($porsentase_fisik <= $nilaiUnder) || ($porsentase_spj <= $nilaiUnder) || ($porsentase_sppd <= $nilaiUnder)){
				$data[]      = $row->id_kegiatan;
			}
		}
		return (!empty($data))?count(array_unique($data)):'';
	}

	public static function under_kegiatan_list($program,$kpa)
	{
		$nilaiUnder = Self::underValue();

		// $pm = ProgramModel::where('year',date('Y'))->get();
		// $program = '';
		// foreach ($pm as $val) {
		// 	$program[] = $val->id;
		// }
		$km = KegiatanModel::where('id_program',$program)->get();
		$kegiatan = '';
		foreach ($km as $nilai) {
			$kegiatan[] = $nilai->id;
		}
		$ikm = ItemKegiatanModel::whereIn('id_kegiatan',$kegiatan)->where('id_user',$kpa)->get();
		$data = '';
		foreach ($ikm as $key => $row) {
			if(empty($row->fisik_realisasi)){
				$fisik_realisasi = 0;
			}else{
				$fisik_realisasi = $row->fisik_realisasi;
			}

			if(empty($row->anggaran_sppd)){
				$anggaran_sppd = 0;
			}else{
				$anggaran_sppd = $row->anggaran_sppd;
			}

			if(empty($row->anggaran_spj)){
				$anggaran_spj = 0;
			}else{
				$anggaran_spj = $row->anggaran_spj;
			}
			$porsentase_fisik = floor(($fisik_realisasi/$row->fisik_rencana)*100);
			$porsentase_sppd  = floor(($anggaran_sppd/$row->anggaran_rencana)*100);
			$porsentase_spj   = floor(($anggaran_spj/$row->anggaran_rencana)*100);
			if(($porsentase_fisik <= $nilaiUnder) || ($porsentase_spj <= $nilaiUnder) || ($porsentase_sppd <= $nilaiUnder)){
				$data[]      = $row->id_kegiatan;
			}
		}
		return (!empty($data))?array_unique($data):'';
	}
	public static function findProgramByKegiatan($kegiatan)
	{
		return KegiatanModel::find($kegiatan)->id_program;
	}

	public static function en($en)
	{
		return Crypt::encrypt($en);
	}
	
	public static function de($de)
	{
		return Crypt::decrypt($de);
	}

	public static function underValue()
	{
		return AboutModel::find(1)->under;
	}

	public static function TotalKPAByProgram($idProgram)
	{
		return UserProgramModel::where('id_program',$idProgram)->count();
	}

	public static function countKegiatan($program,$kpa)
	{
		return UserKegiatanModel::where('id_program',$program)->where('id_user',$kpa)->count();
	}

	public static function countKegiatanUnder($program,$kpa,$nilai)
	{
		return ProsentaseModel::where('fisik_value','<=',$nilai)->orWhere('sppd_value','<=',$nilai)->orWhere('spj_value','<=',$nilai)->where('id_program',$program)->where('id_user',$kpa)->count();
	}

	public static function countRata($idUser,$idKegiatan)
	{
		$ikm = ItemKegiatanModel::where('id_user',$idUser)->where('id_kegiatan',$idKegiatan)->get();
		if(count($ikm)>0){
			foreach ($ikm as $key => $row) {
				if(empty($row->fisik_realisasi)){
					$realisasi = 0;
				}else{
					$realisasi = $row->fisik_realisasi;
				}
				$progress[] = floor(($realisasi/$row->fisik_rencana)*100);
			}
			$total = array_sum($progress);
			return ceil($total/count($ikm));
		}else{
			return 0;
		}
	}


	public static function insertCountRata($kpa,$program,$kegiatan)
	{
		$ikm = ItemKegiatanModel::where('id_user',$kpa)->where('id_kegiatan',$kegiatan)->get();
		if(count($ikm)>0){
			foreach ($ikm as $key => $row) {
				// FISIK
				$realisasi_progress[] = (!empty($row->fisik_realisasi))?floor(($row->fisik_realisasi/$row->fisik_rencana)*100):0;
				// SPPD
				$sppd_anggaran[]      = (!empty($row->anggaran_sppd))?floor(($row->anggaran_sppd/$row->anggaran_rencana)*100):0;
				// SPJ
				$spj_anggaran[]       = (!empty($row->anggaran_spj))?floor(($row->anggaran_spj/$row->anggaran_rencana)*100):0;
			}
			// FISIK
			$total_realisasi      = array_sum($realisasi_progress);
			$prosentase_realisasi = ceil($total_realisasi/count($ikm));
			// SPPD
			$total_sppd           = array_sum($sppd_anggaran);
			$prosentase_sppd      = ($total_sppd)?ceil($total_sppd/count($ikm)):0;
			// SPJ
			$total_spj            = array_sum($spj_anggaran);
			$prosentase_spj       = ($total_spj)?ceil($total_spj/count($ikm)):0;

			$PM         = ProsentaseModel::where('id_user',$kpa)
							->where('id_kegiatan',$kegiatan)
							->where('id_program',$program)->first();
			if(!empty($PM)){
				$PM->fisik_value = $prosentase_realisasi;
				$PM->sppd_value  = $prosentase_sppd;
				$PM->spj_value   = $prosentase_spj;
				$PM->save();
			}else{
				$pm              = new ProsentaseModel;
				$pm->id_user     = $kpa;
				$pm->id_kegiatan = $kegiatan;
				$pm->id_program  = $program;
				$pm->fisik_value = $prosentase_realisasi;
				$pm->sppd_value  = $prosentase_sppd;
				$pm->spj_value   = $prosentase_spj;
				$pm->save();
			}
		}else{
			ProsentaseModel::where('id_user',$kpa)->where('id_kegiatan',$kegiatan)->where('id_program',$program)->delete();
		}
	}

	public static function countRataSp2d($idUser,$idKegiatan)
	{
		$ikm = ItemKegiatanModel::where('id_user',$idUser)->where('id_kegiatan',$idKegiatan)->get();
		if(count($ikm)>0){
			foreach ($ikm as $key => $row) {
				if(empty($row->anggaran_sppd)){
					$sppd = 0;
				}else{
					$sppd = $row->anggaran_sppd;
				}
				$progress[] = floor(($sppd/$row->anggaran_rencana)*100);
			}
			$total = array_sum($progress);
			return ceil($total/count($ikm));
		}else{
			return 0;
		}
	}

	public static function countRataSpj($idUser,$idKegiatan)
	{
		$ikm = ItemKegiatanModel::where('id_user',$idUser)->where('id_kegiatan',$idKegiatan)->get();
		if(count($ikm)>0){
			foreach ($ikm as $key => $row) {
				if(empty($row->anggaran_spj)){
					$spj = 0;
				}else{
					$spj = $row->anggaran_spj;
				}
				$progress[] = floor(($spj/$row->anggaran_rencana)*100);
			}
			$total = array_sum($progress);
			return ceil($total/count($ikm));
		}else{
			return 0;
		}
	}

	public static function IsComplete()
	{
		$admin = Session::get('kpaUsers');
		$km = KPAModel::find($admin['id']);
		return $km->is_complete;
	}
	public static function KegiatanTotal($id)
	{
		return KegiatanModel::where('id_program',$id)->count();
	}
	public static function UserKegiatan($program,$kpa)
	{
		return UserKegiatanModel::where('id_program',$program)->where('id_user',$kpa)->count();
	}
	public static function TotalProgram($id)
	{
		// return DB::table('user_program')
  //                    ->select(DB::raw('count(*) as user_count'))
  //                    ->where('id_program',$id)
  //                    ->groupBy('id_user')
  //                    ->get();
		return UserProgramModel::where('id_program',$id)->groupBy('id_program')->count();
	}

	public static function TotalKPA($id)
	{
		return UserKegiatanModel::where('id_program',$id)->groupBy('id_user')->count();
	}

	public static function countKPA($program,$kegiatan)
	{
		return UserKegiatanModel::where('id_program',$program)->where('id_kegiatan',$kegiatan)->count();
	}

	public static function totalPaguKegiatan($id)
	{
		$km = UserKegiatanModel::where('id_kegiatan',$id)->get();
		$data[] = 0;
		if(count($km)>0){
			foreach ($km as $key => $value) {
				if(!empty($value->pagu_kegiatan_perubahan)){	
					$data[] = $value->pagu_kegiatan_perubahan;
				}else{
					$data[] = $value->pagu_kegiatan;
				}
			}
		}
		return array_sum($data);
	}
	public static function getKeterangan($kpa,$program,$kegiatan)
	{
		$ukm = UserKegiatanModel::where('id_user',$kpa)->where('id_program',$program)->where('id_kegiatan',$kegiatan)->first();
		return $ukm->keterangan;
	}

	public static function totalPagu($id)
	{
		$km = UserKegiatanModel::where('id_program',$id)->get();
		$data[] = 0;
		if(count($km)>0){
			foreach ($km as $key => $value) {
				if(!empty($value->pagu_kegiatan_perubahan)){
					$data[] = $value->pagu_kegiatan_perubahan;
				}else{
					$data[] = $value->pagu_kegiatan;
				}
			}
		}
		return array_sum($data);
	}
	public static function totalPaguKPA($program,$kpa)
	{
		$km = UserKegiatanModel::where('id_program',$program)->where('id_user',$kpa)->get();
		$data[] = 0;
		if(count($km)>0){
			foreach ($km as $key => $value) {
				if(!empty($value->pagu_kegiatan_perubahan)){
					$data[] = $value->pagu_kegiatan_perubahan;
				}else{
					$data[] = $value->pagu_kegiatan;
				}
			}
		}
		return array_sum($data);
	}
	public static function totalPaguKPAKegiatan($program,$kegiatan,$kpa)
	{
		$km = UserKegiatanModel::where('id_program',$program)->where('id_kegiatan',$kegiatan)->where('id_user',$kpa)->get();
		$data[] = 0;
		if(count($km)>0){
			foreach ($km as $key => $value) {
				if(!empty($value->pagu_kegiatan_perubahan)){
					$data[] = $value->pagu_kegiatan_perubahan;
				}else{
					$data[] = $value->pagu_kegiatan;
				}
			}
		}
		return array_sum($data);
	}
	public static function getKegiatanOfProgram($id)
	{
		$km = KegiatanModel::orderBy('id','desc')->where('id_program',$id)->get();
		if(count($km)>0){
			$html = '<ul class="itemkegiatan">';
			foreach ($km as $key => $value) {
				$html.= '<li><a href="">'.$value->name.'</a></li>';
			}
			$html.= '</ul>';

			return $html;
		}
	}

	public static function CountUserProgram($idKPA,$year)
	{
		
	}

	public static function getItemKegiatan($idKegiatan,$idTahap)
	{
		$ikm = ItemKegiatanModel::where('id_kegiatan',$idKegiatan)->where('id_detail_tahap',$idTahap)->orderBy('id','desc')->get();
		if(count($ikm)>0){
			foreach ($ikm as $key => $val) {
				$data['name'][]            = (!empty($val->name))?'<li>'.$val->name.'</li>':'';
				$data['vol'][]             = (!empty($val->vol))?'<li>'.$val->vol.'</li>':'';
				$data['satuan'][]          = (!empty($val->satuan))?'<li>'.$val->satuan.'</li>':'';
				$data['fisik_rencana'][]   = (!empty($val->fisik_rencana))?'<li>'.$val->fisik_rencana.'</li>':'';
				$data['fisik_realisasi'][] = (!empty($val->fisik_realisasi))?'<li>'.$val->fisik_realisasi.'</li>':'';
				$data['sppd_rencana'][]    = (!empty($val->sppd_rencana))?'<li>'.$val->sppd_rencana.'</li>':'';
				$data['sppd_realisasi'][]  = (!empty($val->sppd_realisasi))?'<li>'.$val->sppd_realisasi.'</li>':'';
				$data['spj_rencana'][]     = (!empty($val->spj_rencana))?'<li>'.$val->spj_rencana.'</li>':'';
				$data['spj_realisasi'][]   = (!empty($val->spj_realisasi))?'<li>'.$val->spj_realisasi.'</li>':'';
			}
			$item['name']            = implode('', $data['name']);
			$item['vol']             = implode('', $data['vol']);
			$item['satuan']          = implode('', $data['satuan']);
			$item['fisik_rencana']   = implode('', $data['fisik_rencana']);
			$item['fisik_realisasi'] = implode('', $data['fisik_realisasi']);
			$item['sppd_rencana']    = implode('', $data['sppd_rencana']);
			$item['sppd_realisasi']  = implode('', $data['sppd_realisasi']);
			$item['spj_rencana']     = implode('', $data['spj_rencana']);
			$item['spj_realisasi']   = implode('', $data['spj_realisasi']);
			return $item;
		}

	}
	
	public static function tahapBulan($tahap,$tahun)
	{
		$dtm = DetailTahapModel::where('id_tahap',$tahap)->where('year',$tahun)->get();
		foreach ($dtm as $key => $value) {
			$data[] = self::Month($value->month);
		}
		return implode(', ', $data);
	}
	public static function checkMonth($tahap,$bulan,$tahun)
	{
		return DetailTahapModel::where('id_tahap',$tahap)->where('month',$bulan)->where('year',$tahun)->count();
	}
	public static function Month($id)
	{
		switch ($id) {
			case 1:
				return 'Januari';
				break;
			case 2:
				return 'Februari';
				break;
			case 3:
				return 'Maret';
				break;
			case 4:
				return 'April';
				break;
			case 5:
				return 'Mei';
				break;
			case 6:
				return 'Juni';
				break;
			case 7:
				return 'Juli';
				break;
			case 8:
				return 'Agustus';
				break;
			case 9:
				return 'September';
				break;
			case 9:
				return 'September';
				break;
			case 10:
				return 'Oktober';
				break;
			case 11:
				return 'November';
				break;
			case 12:
				return 'Desember';
				break;
		}
	}
	// DIY

}