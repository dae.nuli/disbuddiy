-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 28, 2017 at 05:29 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diy`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `web_keywords` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `gplus` varchar(100) NOT NULL,
  `bbm` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `home_description` text NOT NULL,
  `blog_description` text NOT NULL,
  `how_to_buy_description` text NOT NULL,
  `order_description` text NOT NULL,
  `contact_description` text NOT NULL,
  `product_description` text NOT NULL,
  `under` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `name`, `email`, `web_keywords`, `address`, `facebook`, `twitter`, `instagram`, `gplus`, `bbm`, `phone`, `home_description`, `blog_description`, `how_to_buy_description`, `order_description`, `contact_description`, `product_description`, `under`, `created_at`, `updated_at`) VALUES
(1, 'Monev', 'giarsyani.nuli@gmail.com', 'hello', 'Jl. Kaliurang KM 14 Perumahan Pamungkas No. AA04 Yogyakarta', 'giarsyaninuli', 'giarsyaninuli', 'http://instagram.com/arinigrafika', '', '2494612', '081915804771', 'home is a', 'blog is a', 'how to buy is a menu', 'order is menu', 'menu is contanct', 'produk adalah', 80, NULL, '2015-11-02 08:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_controller` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `id_group`, `id_controller`, `deleted_at`, `created_at`, `updated_at`) VALUES
(65, 2, 8, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(66, 2, 9, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(67, 2, 10, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(68, 2, 13, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(69, 2, 14, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(70, 2, 15, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(71, 2, 17, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(72, 2, 21, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(73, 2, 22, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(74, 2, 23, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(75, 2, 24, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(76, 2, 25, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(77, 2, 26, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `complains`
--

CREATE TABLE `complains` (
  `id` int(10) UNSIGNED NOT NULL,
  `skpd_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `kegiatan_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `complains`
--

INSERT INTO `complains` (`id`, `skpd_id`, `program_id`, `kegiatan_id`, `name`, `email`, `phone_number`, `comment`, `created_at`, `updated_at`) VALUES
(1, 2, 5, 7, 'Nuli', 'Nuli@gmail.com', '081915804771', 'Hey can you explain in simple what this product does, because I wonder if this product can give as many required likes to your post', '2017-10-27 16:48:05', '2017-10-27 16:48:05'),
(2, 2, 5, 8, 'Dae', 'dae@gmail.com', '081915804771', 'You really need to incorporate an email notification or something when re-login is required. It frustrates me to have to login every day to check if re-login is required (bot stopped).\n\nThanks.', '2017-10-27 16:49:02', '2017-10-27 16:49:02'),
(3, 2, 5, 7, 'Mas', 'masto@gmail.com', '081195804771', 'I have installed and activated both and even added the # tags in which to like. I have set the speed to very fast and active, these settings have been set for around 24 hours now yet my account still hasn’t liked 1 photo.', '2017-10-27 16:49:39', '2017-10-27 16:49:39'),
(4, 1, 5, 7, 'Gaga', 'g@gmail.com', '29892', 'I would like to charge end user, do i need to select extended license on both main code and on addon as this?', '2017-10-27 16:55:06', '2017-10-27 16:55:06'),
(5, 2, 5, 7, 'rashul', 'hello@am.com', '293720', 'can i offer auto like service to my client with this app with your main app Nextpost ? i am looking to offer some service like people sign up server and app send auto likes for every new post on IG. please let me know if we can achieve this. thanks', '2017-10-27 16:56:32', '2017-10-27 16:56:32');

-- --------------------------------------------------------

--
-- Table structure for table `controllers`
--

CREATE TABLE `controllers` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `controllers`
--

INSERT INTO `controllers` (`id`, `id_parent`, `name`, `url`, `description`, `icon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Blog', NULL, 'Blog Parent', 'fa fa-file-o', NULL, '2015-03-24 08:02:15', '2015-03-24 08:02:15'),
(2, 1, 'Category', 'news_category', 'Blog Category', '', NULL, '2015-03-24 08:03:03', '2015-03-24 08:03:03'),
(3, 1, 'List', NULL, 'Blog List', '', NULL, '2015-03-24 08:05:39', '2015-03-24 08:05:39'),
(4, NULL, 'Products', NULL, 'Products', 'fa fa-list-alt', NULL, '2015-03-24 08:08:53', '2015-03-24 08:08:53'),
(5, 4, 'Category', 'product_category', 'Product Category', '', NULL, '2015-03-24 08:11:07', '2015-03-24 08:11:07'),
(6, 4, 'Sub Category', 'product_subcategory', 'Product Sub Category', '', NULL, '2015-03-24 08:12:13', '2015-03-24 08:12:13'),
(7, 4, 'List', 'products', 'Product List', '', NULL, '2015-03-24 08:12:36', '2015-03-24 08:12:36'),
(8, NULL, 'Orders', 'orders', 'Orders List', 'fa fa-shopping-cart', NULL, '2015-03-24 08:13:06', '2015-03-24 08:13:06'),
(9, NULL, 'Clients', 'clients', 'Clients List', 'fa fa-users', NULL, '2015-03-24 08:13:32', '2015-03-24 08:13:32'),
(10, NULL, 'Best Seller', 'best', 'Best Seller List', 'fa fa-star', NULL, '2015-03-24 08:14:07', '2015-03-24 08:14:07'),
(11, NULL, 'Testimony', 'testimony', 'Testimony List', 'fa fa-comments', NULL, '2015-03-24 08:14:49', '2015-03-24 08:14:49'),
(12, NULL, 'Gallery', 'gallery', 'Gallery List', 'fa fa-picture-o', NULL, '2015-03-24 08:15:20', '2015-03-24 08:15:20'),
(13, NULL, 'Management Staff', NULL, 'Management Staff Menu', 'fa fa-user', NULL, '2015-03-24 09:30:57', '2015-03-24 09:30:57'),
(14, 13, 'List', 'users', 'Staff List', '', NULL, '2015-03-24 09:31:27', '2015-03-24 09:39:55'),
(15, 13, 'Deleted Staff', 'deleted-staff', 'Deleted Staff List', '', NULL, '2015-03-24 09:31:53', '2015-03-24 09:31:53'),
(17, NULL, 'About', 'about', 'About Menu', 'fa fa-info-circle', NULL, '2015-03-24 08:18:46', '2015-03-24 08:53:07'),
(21, NULL, 'Reports', 'report', 'Reports Menu', 'fa fa-bar-chart-o', NULL, '2015-03-24 09:33:59', '2015-04-14 08:26:05'),
(22, NULL, 'Bank', 'bank', 'Bank is menu for input bank account', 'fa fa-usd', NULL, '2015-04-14 08:04:07', '2015-04-14 08:04:07'),
(23, NULL, 'How To Buy', 'how', 'is menu for description some step to buy product', 'fa fa-question-circle', NULL, '2015-04-14 08:05:37', '2015-04-14 08:05:37'),
(24, NULL, 'Banner', 'banner', 'is menu to set banner content', 'fa fa-desktop', NULL, '2015-04-14 08:06:17', '2015-04-14 08:06:17'),
(25, NULL, 'Statistics', 'statistics', 'is menu to view visitor activity', 'fa fa-signal', NULL, '2015-04-14 08:08:07', '2015-04-14 08:08:07'),
(26, NULL, 'Messages', 'messages', 'Is all about contact ', 'fa fa-envelope', NULL, '2015-04-18 02:14:15', '2015-04-18 02:14:15');

-- --------------------------------------------------------

--
-- Table structure for table `detail_tahap`
--

CREATE TABLE `detail_tahap` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_tahap` int(10) UNSIGNED NOT NULL,
  `month` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `detail_tahap`
--

INSERT INTO `detail_tahap` (`id`, `id_tahap`, `month`, `year`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2015, NULL, NULL, NULL),
(2, 1, 2, 2015, NULL, NULL, NULL),
(3, 1, 3, 2015, NULL, NULL, NULL),
(4, 2, 4, 2015, NULL, NULL, NULL),
(5, 2, 5, 2015, NULL, NULL, NULL),
(6, 2, 6, 2015, NULL, NULL, NULL),
(7, 2, 7, 2015, NULL, NULL, NULL),
(8, 3, 8, 2015, NULL, NULL, NULL),
(9, 3, 9, 2015, NULL, NULL, NULL),
(10, 3, 10, 2015, NULL, NULL, NULL),
(11, 3, 11, 2015, NULL, NULL, NULL),
(12, 3, 12, 2015, NULL, NULL, NULL),
(17, 1, 1, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(18, 1, 2, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(19, 1, 3, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(20, 1, 4, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(21, 1, 5, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(22, 2, 6, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(23, 2, 7, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(24, 2, 8, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(25, 2, 9, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(26, 2, 10, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(27, 3, 11, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(28, 3, 12, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(41, 1, 1, 2016, NULL, '2015-11-01 02:47:19', '2015-11-01 02:47:19'),
(42, 1, 2, 2016, NULL, '2015-11-01 02:47:19', '2015-11-01 02:47:19'),
(43, 1, 3, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(44, 1, 4, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(45, 1, 5, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(46, 1, 6, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(47, 1, 7, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(48, 1, 8, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(49, 1, 9, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(50, 1, 10, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(51, 1, 11, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(52, 1, 12, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:38');

-- --------------------------------------------------------

--
-- Table structure for table `item_kegiatan`
--

CREATE TABLE `item_kegiatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_kegiatan` int(10) UNSIGNED NOT NULL,
  `id_detail_tahap` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vol` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `satuan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fisik_rencana` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fisik_realisasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anggaran_rencana` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `anggaran_sppd` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `anggaran_spj` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_kegiatan`
--

INSERT INTO `item_kegiatan` (`id`, `id_user`, `id_kegiatan`, `id_detail_tahap`, `name`, `vol`, `satuan`, `fisik_rencana`, `fisik_realisasi`, `anggaran_rencana`, `anggaran_sppd`, `anggaran_spj`, `keterangan`, `deleted_at`, `created_at`, `updated_at`) VALUES
(16, 1, 7, 2, 'ddedededed', '9000', 'saaaa', '70', '40', '500000', '70000', '90000', 'kk', NULL, '2015-11-01 08:19:06', '2015-11-27 03:04:28'),
(17, 1, 4, 1, 'ss', '9', 's', '90', '90', '50000', '50000', '50000', 'ss', NULL, '2015-11-02 08:34:17', '2015-11-27 03:05:03'),
(19, 1, 4, 1, 'sss', '78', 's', '80', '70', '50000', '50000', '50000', 'cc', NULL, '2015-11-02 20:05:35', '2015-11-27 03:05:03'),
(20, 1, 1, 2, 'xxxxx', '2', 's', '70', '60', '800', '800', '800', 'jk', NULL, '2015-11-02 20:29:23', '2015-11-27 03:05:03'),
(21, 1, 1, 5, 'sss', '90', 's', '90', '90', '90000', '90000', '90000', 'kk', NULL, '2015-11-09 03:12:42', '2015-11-27 03:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_program` int(10) UNSIGNED NOT NULL,
  `code_kegiatan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pagu_kegiatan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `id_program`, `code_kegiatan`, `name`, `pagu_kegiatan`, `description`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, '045', 'hei', '100000', 'kete', 1, NULL, NULL, '2015-10-10 02:54:05', '2015-11-27 03:05:03'),
(2, 1, '001.003.002', 'kalong wewe', '900000', 'keter', NULL, NULL, '2015-11-27 03:42:02', '2015-10-15 06:39:24', '2015-11-27 03:42:02'),
(3, 1, '001.003.004.005', 'inez ningtyas', '80999', 'kekeke', NULL, NULL, '2015-11-27 03:42:02', '2015-10-15 06:39:53', '2015-11-27 03:42:02'),
(4, 2, '044', 'keg', '30000', 'ket', NULL, NULL, NULL, '2015-10-22 07:33:14', '2015-11-27 03:05:03'),
(5, 4, '4334', 'ffff', '40000', '', 1, NULL, NULL, '2015-10-24 19:39:31', '2015-11-27 03:06:00'),
(7, 5, '076', 'dae', '', 's', NULL, NULL, NULL, '2015-10-27 01:52:55', '2015-11-27 03:04:28'),
(8, 5, '079', 'batik tulis', '', '', NULL, NULL, NULL, '2015-10-27 20:19:08', '2015-11-27 03:04:28'),
(9, 6, '009', 'kalong wewe dua', '', 'ket dua', 1, NULL, '2015-11-27 03:41:43', '2015-11-27 03:37:09', '2015-11-27 03:41:43'),
(10, 6, '008', 'inez ningtyas dua', '', 'kekeke dua', 1, NULL, '2015-11-27 03:41:43', '2015-11-27 03:37:09', '2015-11-27 03:41:43'),
(11, 5, '453', 'dada', NULL, NULL, NULL, NULL, NULL, '2015-11-27 06:09:17', '2015-11-27 06:09:17');

-- --------------------------------------------------------

--
-- Table structure for table `kpa_users`
--

CREATE TABLE `kpa_users` (
  `id` int(11) NOT NULL,
  `code_kpa` varchar(255) NOT NULL,
  `instansi_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_staff` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `bidang` varchar(100) NOT NULL,
  `personil_name` varchar(255) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_complete` enum('2','1','0') DEFAULT '0',
  `activation` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kpa_users`
--

INSERT INTO `kpa_users` (`id`, `code_kpa`, `instansi_name`, `email`, `email_staff`, `username`, `phone`, `address`, `bidang`, `personil_name`, `password`, `is_active`, `is_complete`, `activation`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '001', 'PT Global Network', 'giarsyani.nuli1@gmail.com', 'giarsyani.nuli@gmail.com', 'giarsyani.nuli', '081915804771', 'Alamat palsu', '', 'Nuli Giarsyani', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', 'DAWCB6b5wBbbynUlp56V', NULL, '2015-10-08 17:55:09', '2015-10-28 03:33:22'),
(2, '0004', 'Hello World', 'giarsyani.nuli2@gmail.com', '', 'dae', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2015-10-13 23:38:42', '2015-10-15 08:42:34'),
(3, 'dsdsds', 'asasa', 'sdsdsds@dsds.com', '', 'sdsdsds', '', '', '', '', NULL, '0', '0', '', NULL, '2015-10-19 23:38:46', '2015-10-19 23:43:19'),
(4, '2009', 'Hao Hao', 'giarsyani.nuli@gmail.com', '', 'giarsyani.nuli', '', '', '', '', 'f3c6e6d4f26ba300a6d1e7925e239f4a62384fba', '1', '0', '', NULL, '2015-11-23 19:27:13', '2015-11-23 19:27:29');

-- --------------------------------------------------------

--
-- Table structure for table `laq_async_queue`
--

CREATE TABLE `laq_async_queue` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `retries` int(11) NOT NULL DEFAULT '0',
  `delay` int(11) NOT NULL DEFAULT '0',
  `payload` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `active` enum('1','0') DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_09_20_173653_create_table_program', 1),
('2015_09_20_173710_create_table_kegiatan', 1),
('2015_09_20_173724_create_table_item_kegiatan', 1),
('2015_09_20_173748_create_table_fisik', 1),
('2015_09_20_173757_create_table_anggaran', 1),
('2015_09_20_173805_create_table_spj', 1),
('2015_09_20_173813_create_table_sp2d', 1),
('2015_09_29_112452_create_table_tahap', 2),
('2015_09_29_112500_create_table_detail_tahap', 3),
('2014_06_03_193005_create_queue_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(10) UNSIGNED NOT NULL,
  `code_program` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `year` year(4) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `code_program`, `name`, `year`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '001.002.003', 'PRogram', 2014, 1, 1, '2015-11-27 03:42:01', '2015-10-10 02:53:06', '2015-11-27 03:42:01'),
(2, '069', 'program contohs', 2015, 1, 1, NULL, '2015-10-10 02:54:05', '2015-11-27 03:05:03'),
(3, '097', 'garuda indonesia', 2015, 1, 1, '2015-11-27 03:41:38', '2015-10-15 08:55:50', '2015-11-27 03:41:38'),
(4, '1111', 'resr', 2015, 1, NULL, NULL, '2015-10-24 19:39:31', '2015-11-27 03:06:00'),
(5, '087', 'sasambo', 2015, 1, NULL, NULL, '2015-10-27 01:52:39', '2015-11-27 03:04:28'),
(6, '212', 'PRogram ke dua', 2016, 1, 1, NULL, '2015-11-27 03:37:09', '2015-11-27 03:41:43');

-- --------------------------------------------------------

--
-- Table structure for table `prosentase`
--

CREATE TABLE `prosentase` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `fisik_value` int(50) UNSIGNED NOT NULL,
  `sppd_value` int(11) UNSIGNED NOT NULL,
  `spj_value` int(11) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prosentase`
--

INSERT INTO `prosentase` (`id`, `id_user`, `id_program`, `id_kegiatan`, `fisik_value`, `sppd_value`, `spj_value`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 7, 94, 95, 100, NULL, '2015-11-01 07:50:51', '2015-11-27 02:56:46'),
(5, 1, 2, 1, 81, 72, 73, NULL, '2015-11-01 08:19:06', '2015-11-27 03:05:03'),
(6, 1, 2, 4, 94, 100, 100, NULL, '2015-11-02 08:34:17', '2015-11-27 03:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `tahap`
--

CREATE TABLE `tahap` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tahap`
--

INSERT INTO `tahap` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Tahap 1', NULL, NULL, NULL),
(2, 'Tahap 2', NULL, NULL, NULL),
(3, 'Tahap 3', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `address`, `phone`, `is_active`, `level`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Nuli', 'testing@admin.com', '49489e8fdd4f6f20dc41ba3b2706d4d9', 'sdada', '081915804771', '1', 1, NULL, '2014-10-24 19:58:52', '2015-03-24 10:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `for_register` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `group_name`, `for_register`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '0', NULL, '2015-03-24 03:24:36'),
(2, 'Operator', '0', NULL, '2015-03-24 03:24:31'),
(3, 'User SKPD/KPA', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_kegiatan`
--

CREATE TABLE `user_kegiatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `pagu_kegiatan` varchar(100) NOT NULL,
  `pagu_kegiatan_perubahan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_kegiatan`
--

INSERT INTO `user_kegiatan` (`id`, `id_program`, `id_kegiatan`, `id_user`, `pagu_kegiatan`, `pagu_kegiatan_perubahan`, `keterangan`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 5, 7, 2, '800000000', '', '', NULL, '2015-10-28 23:22:02', '2015-11-27 03:04:28'),
(4, 5, 8, 2, '9000000', '80000', 'dae', NULL, '2015-10-29 00:06:08', '2015-11-27 03:04:28'),
(5, 5, 7, 1, '2000000', '', 'nul', NULL, '2015-10-29 00:08:55', '2015-11-27 03:04:28'),
(6, 2, 1, 1, '800000', '', 'kk', NULL, '2015-10-31 07:49:58', '2015-11-27 03:05:03'),
(7, 2, 4, 1, '90', '100000', 's', NULL, '2015-10-31 07:53:48', '2015-11-27 03:05:03'),
(8, 4, 5, 1, '800000', '', '', NULL, '2015-11-04 08:26:07', '2015-11-27 03:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_program`
--

CREATE TABLE `user_program` (
  `id` int(11) NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_program`
--

INSERT INTO `user_program` (`id`, `id_program`, `id_user`, `deleted_at`, `created_at`, `updated_at`) VALUES
(8, 2, 1, NULL, '2015-10-27 00:11:09', '2015-11-27 03:05:03'),
(10, 5, 1, NULL, '2015-10-27 01:53:20', '2015-11-27 03:04:28'),
(11, 5, 2, NULL, '2015-10-27 20:20:28', '2015-11-27 03:04:28'),
(12, 2, 2, NULL, '2015-10-27 21:10:25', '2015-11-27 03:05:03'),
(13, 2, 3, NULL, '2015-10-27 21:10:50', '2015-11-27 03:05:03'),
(16, 4, 1, NULL, '2015-11-04 08:39:51', '2015-11-27 03:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(32) NOT NULL,
  `id_article` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('1','2','3','4') NOT NULL COMMENT '1. products, 2. news, 3. Gallery, 4. general',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`),
  ADD KEY `id_controller` (`id_controller`);

--
-- Indexes for table `complains`
--
ALTER TABLE `complains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_parent` (`id_parent`);

--
-- Indexes for table `detail_tahap`
--
ALTER TABLE `detail_tahap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_kegiatan`
--
ALTER TABLE `item_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kpa_users`
--
ALTER TABLE `kpa_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laq_async_queue`
--
ALTER TABLE `laq_async_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prosentase`
--
ALTER TABLE `prosentase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tahap`
--
ALTER TABLE `tahap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_kegiatan`
--
ALTER TABLE `user_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_program`
--
ALTER TABLE `user_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `complains`
--
ALTER TABLE `complains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `controllers`
--
ALTER TABLE `controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `detail_tahap`
--
ALTER TABLE `detail_tahap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `item_kegiatan`
--
ALTER TABLE `item_kegiatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `kpa_users`
--
ALTER TABLE `kpa_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `laq_async_queue`
--
ALTER TABLE `laq_async_queue`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `prosentase`
--
ALTER TABLE `prosentase`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tahap`
--
ALTER TABLE `tahap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_kegiatan`
--
ALTER TABLE `user_kegiatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_program`
--
ALTER TABLE `user_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
