DROP TABLE IF EXISTS `about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `web_keywords` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `gplus` varchar(100) NOT NULL,
  `bbm` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `home_description` text NOT NULL,
  `blog_description` text NOT NULL,
  `how_to_buy_description` text NOT NULL,
  `order_description` text NOT NULL,
  `contact_description` text NOT NULL,
  `product_description` text NOT NULL,
  `under` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about`
--

LOCK TABLES `about` WRITE;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` VALUES (1,'Monev','giarsyani.nuli@gmail.com','hello','Jl. Kaliurang KM 14 Perumahan Pamungkas No. AA04 Yogyakarta','giarsyaninuli','giarsyaninuli','http://instagram.com/arinigrafika','','2494612','081915804771','home is a','blog is a','how to buy is a menu','order is menu','menu is contanct','produk adalah',80,NULL,'2015-11-02 15:52:34');
/*!40000 ALTER TABLE `about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(10) unsigned NOT NULL,
  `id_controller` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_group` (`id_group`),
  KEY `id_controller` (`id_controller`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access`
--

LOCK TABLES `access` WRITE;
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
INSERT INTO `access` VALUES (65,2,8,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(66,2,9,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(67,2,10,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(68,2,13,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(69,2,14,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(70,2,15,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(71,2,17,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(72,2,21,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(73,2,22,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(74,2,23,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(75,2,24,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(76,2,25,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27'),(77,2,26,NULL,'2015-04-18 09:14:27','2015-04-18 09:14:27');
/*!40000 ALTER TABLE `access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controllers`
--

DROP TABLE IF EXISTS `controllers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controllers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controllers`
--

LOCK TABLES `controllers` WRITE;
/*!40000 ALTER TABLE `controllers` DISABLE KEYS */;
INSERT INTO `controllers` VALUES (1,NULL,'Blog',NULL,'Blog Parent','fa fa-file-o',NULL,'2015-03-24 15:02:15','2015-03-24 15:02:15'),(2,1,'Category','news_category','Blog Category','',NULL,'2015-03-24 15:03:03','2015-03-24 15:03:03'),(3,1,'List',NULL,'Blog List','',NULL,'2015-03-24 15:05:39','2015-03-24 15:05:39'),(4,NULL,'Products',NULL,'Products','fa fa-list-alt',NULL,'2015-03-24 15:08:53','2015-03-24 15:08:53'),(5,4,'Category','product_category','Product Category','',NULL,'2015-03-24 15:11:07','2015-03-24 15:11:07'),(6,4,'Sub Category','product_subcategory','Product Sub Category','',NULL,'2015-03-24 15:12:13','2015-03-24 15:12:13'),(7,4,'List','products','Product List','',NULL,'2015-03-24 15:12:36','2015-03-24 15:12:36'),(8,NULL,'Orders','orders','Orders List','fa fa-shopping-cart',NULL,'2015-03-24 15:13:06','2015-03-24 15:13:06'),(9,NULL,'Clients','clients','Clients List','fa fa-users',NULL,'2015-03-24 15:13:32','2015-03-24 15:13:32'),(10,NULL,'Best Seller','best','Best Seller List','fa fa-star',NULL,'2015-03-24 15:14:07','2015-03-24 15:14:07'),(11,NULL,'Testimony','testimony','Testimony List','fa fa-comments',NULL,'2015-03-24 15:14:49','2015-03-24 15:14:49'),(12,NULL,'Gallery','gallery','Gallery List','fa fa-picture-o',NULL,'2015-03-24 15:15:20','2015-03-24 15:15:20'),(13,NULL,'Management Staff',NULL,'Management Staff Menu','fa fa-user',NULL,'2015-03-24 16:30:57','2015-03-24 16:30:57'),(14,13,'List','users','Staff List','',NULL,'2015-03-24 16:31:27','2015-03-24 16:39:55'),(15,13,'Deleted Staff','deleted-staff','Deleted Staff List','',NULL,'2015-03-24 16:31:53','2015-03-24 16:31:53'),(17,NULL,'About','about','About Menu','fa fa-info-circle',NULL,'2015-03-24 15:18:46','2015-03-24 15:53:07'),(21,NULL,'Reports','report','Reports Menu','fa fa-bar-chart-o',NULL,'2015-03-24 16:33:59','2015-04-14 15:26:05'),(22,NULL,'Bank','bank','Bank is menu for input bank account','fa fa-usd',NULL,'2015-04-14 15:04:07','2015-04-14 15:04:07'),(23,NULL,'How To Buy','how','is menu for description some step to buy product','fa fa-question-circle',NULL,'2015-04-14 15:05:37','2015-04-14 15:05:37'),(24,NULL,'Banner','banner','is menu to set banner content','fa fa-desktop',NULL,'2015-04-14 15:06:17','2015-04-14 15:06:17'),(25,NULL,'Statistics','statistics','is menu to view visitor activity','fa fa-signal',NULL,'2015-04-14 15:08:07','2015-04-14 15:08:07'),(26,NULL,'Messages','messages','Is all about contact ','fa fa-envelope',NULL,'2015-04-18 09:14:15','2015-04-18 09:14:15');
/*!40000 ALTER TABLE `controllers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail_tahap`
--

DROP TABLE IF EXISTS `detail_tahap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail_tahap` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tahap` int(10) unsigned NOT NULL,
  `month` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail_tahap`
--

LOCK TABLES `detail_tahap` WRITE;
/*!40000 ALTER TABLE `detail_tahap` DISABLE KEYS */;
INSERT INTO `detail_tahap` VALUES (1,1,1,2015,NULL,NULL,NULL),(2,1,2,2015,NULL,NULL,NULL),(3,1,3,2015,NULL,NULL,NULL),(4,2,4,2015,NULL,NULL,NULL),(5,2,5,2015,NULL,NULL,NULL),(6,2,6,2015,NULL,NULL,NULL),(7,2,7,2015,NULL,NULL,NULL),(8,3,8,2015,NULL,NULL,NULL),(9,3,9,2015,NULL,NULL,NULL),(10,3,10,2015,NULL,NULL,NULL),(11,3,11,2015,NULL,NULL,NULL),(12,3,12,2015,NULL,NULL,NULL),(17,1,1,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(18,1,2,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(19,1,3,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(20,1,4,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(21,1,5,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(22,2,6,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(23,2,7,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(24,2,8,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(25,2,9,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(26,2,10,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(27,3,11,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(28,3,12,2017,NULL,'2015-11-01 07:31:10','2015-11-01 07:31:10'),(41,1,1,2016,NULL,'2015-11-01 09:47:19','2015-11-01 09:47:19'),(42,1,2,2016,NULL,'2015-11-01 09:47:19','2015-11-01 09:47:19'),(43,1,3,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(44,1,4,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(45,1,5,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(46,1,6,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(47,1,7,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(48,1,8,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(49,1,9,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(50,1,10,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(51,1,11,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:20'),(52,1,12,2016,NULL,'2015-11-01 09:47:20','2015-11-01 09:47:38');
/*!40000 ALTER TABLE `detail_tahap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_kegiatan`
--

DROP TABLE IF EXISTS `item_kegiatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_kegiatan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `id_kegiatan` int(10) unsigned NOT NULL,
  `id_detail_tahap` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vol` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `satuan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fisik_rencana` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fisik_realisasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anggaran_rencana` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `anggaran_sppd` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `anggaran_spj` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_kegiatan`
--

LOCK TABLES `item_kegiatan` WRITE;
/*!40000 ALTER TABLE `item_kegiatan` DISABLE KEYS */;
INSERT INTO `item_kegiatan` VALUES (16,1,7,2,'ddedededed','9000','saaaa','70','40','500000','70000','90000','kk',NULL,'2015-11-01 15:19:06','2015-11-27 10:04:28'),(17,1,4,1,'ss','9','s','90','90','50000','50000','50000','ss',NULL,'2015-11-02 15:34:17','2015-11-27 10:05:03'),(19,1,4,1,'sss','78','s','80','70','50000','50000','50000','cc',NULL,'2015-11-03 03:05:35','2015-11-27 10:05:03'),(20,1,1,2,'xxxxx','2','s','70','60','800','800','800','jk',NULL,'2015-11-03 03:29:23','2015-11-27 10:05:03'),(21,1,1,5,'sss','90','s','90','90','90000','90000','90000','kk',NULL,'2015-11-09 10:12:42','2015-11-27 10:05:03');
/*!40000 ALTER TABLE `item_kegiatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kegiatan`
--

DROP TABLE IF EXISTS `kegiatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kegiatan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_program` int(10) unsigned NOT NULL,
  `code_kegiatan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pagu_kegiatan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kegiatan`
--

LOCK TABLES `kegiatan` WRITE;
/*!40000 ALTER TABLE `kegiatan` DISABLE KEYS */;
INSERT INTO `kegiatan` VALUES (1,2,'045','hei','100000','kete',1,NULL,NULL,'2015-10-10 09:54:05','2015-11-27 10:05:03'),(2,1,'001.003.002','kalong wewe','900000','keter',NULL,NULL,'2015-11-27 10:42:02','2015-10-15 13:39:24','2015-11-27 10:42:02'),(3,1,'001.003.004.005','inez ningtyas','80999','kekeke',NULL,NULL,'2015-11-27 10:42:02','2015-10-15 13:39:53','2015-11-27 10:42:02'),(4,2,'044','keg','30000','ket',NULL,NULL,NULL,'2015-10-22 14:33:14','2015-11-27 10:05:03'),(5,4,'4334','ffff','40000','',1,NULL,NULL,'2015-10-25 02:39:31','2015-11-27 10:06:00'),(7,5,'076','dae','','s',NULL,NULL,NULL,'2015-10-27 08:52:55','2015-11-27 10:04:28'),(8,5,'079','batik tulis','','',NULL,NULL,NULL,'2015-10-28 03:19:08','2015-11-27 10:04:28'),(9,6,'009','kalong wewe dua','','ket dua',1,NULL,'2015-11-27 10:41:43','2015-11-27 10:37:09','2015-11-27 10:41:43'),(10,6,'008','inez ningtyas dua','','kekeke dua',1,NULL,'2015-11-27 10:41:43','2015-11-27 10:37:09','2015-11-27 10:41:43'),(11,5,'453','dada',NULL,NULL,NULL,NULL,NULL,'2015-11-27 13:09:17','2015-11-27 13:09:17');
/*!40000 ALTER TABLE `kegiatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kpa_users`
--

DROP TABLE IF EXISTS `kpa_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kpa_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_kpa` varchar(255) NOT NULL,
  `instansi_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_staff` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `bidang` varchar(100) NOT NULL,
  `personil_name` varchar(255) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_complete` enum('2','1','0') DEFAULT '0',
  `activation` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kpa_users`
--

LOCK TABLES `kpa_users` WRITE;
/*!40000 ALTER TABLE `kpa_users` DISABLE KEYS */;
INSERT INTO `kpa_users` VALUES (1,'001','PT Global Network','giarsyani.nuli1@gmail.com','giarsyani.nuli@gmail.com','giarsyani.nuli','081915804771','Alamat palsu','','Nuli Giarsyani','f8221682b0d2e1baf27b46963870995fa88adc4e','1','2','DAWCB6b5wBbbynUlp56V',NULL,'2015-10-09 00:55:09','2015-10-28 10:33:22'),(2,'0004','nama','giarsyani.nuli2@gmail.com','','dae','','','','','f8221682b0d2e1baf27b46963870995fa88adc4e','1','2','',NULL,'2015-10-14 06:38:42','2015-10-15 15:42:34'),(3,'dsdsds','asasa','sdsdsds@dsds.com','','sdsdsds','','','','',NULL,'0','0','',NULL,'2015-10-20 06:38:46','2015-10-20 06:43:19'),(4,'2009','daaaa','giarsyani.nuli@gmail.com','','giarsyani.nuli','','','','','f3c6e6d4f26ba300a6d1e7925e239f4a62384fba','1','0','',NULL,'2015-11-24 02:27:13','2015-11-24 02:27:29');
/*!40000 ALTER TABLE `kpa_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laq_async_queue`
--

DROP TABLE IF EXISTS `laq_async_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laq_async_queue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  `retries` int(11) NOT NULL DEFAULT '0',
  `delay` int(11) NOT NULL DEFAULT '0',
  `payload` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laq_async_queue`
--

LOCK TABLES `laq_async_queue` WRITE;
/*!40000 ALTER TABLE `laq_async_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `laq_async_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `active` enum('1','0') DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2015_09_20_173653_create_table_program',1),('2015_09_20_173710_create_table_kegiatan',1),('2015_09_20_173724_create_table_item_kegiatan',1),('2015_09_20_173748_create_table_fisik',1),('2015_09_20_173757_create_table_anggaran',1),('2015_09_20_173805_create_table_spj',1),('2015_09_20_173813_create_table_sp2d',1),('2015_09_29_112452_create_table_tahap',2),('2015_09_29_112500_create_table_detail_tahap',3),('2014_06_03_193005_create_queue_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `program`
--

DROP TABLE IF EXISTS `program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code_program` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `year` year(4) NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `program`
--

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;
INSERT INTO `program` VALUES (1,'001.002.003','PRogram',2014,1,1,'2015-11-27 10:42:01','2015-10-10 09:53:06','2015-11-27 10:42:01'),(2,'069','program contohs',2015,1,1,NULL,'2015-10-10 09:54:05','2015-11-27 10:05:03'),(3,'097','garuda indonesia',2015,1,1,'2015-11-27 10:41:38','2015-10-15 15:55:50','2015-11-27 10:41:38'),(4,'1111','resr',2015,1,NULL,NULL,'2015-10-25 02:39:31','2015-11-27 10:06:00'),(5,'087','sasambo',2015,1,NULL,NULL,'2015-10-27 08:52:39','2015-11-27 10:04:28'),(6,'212','PRogram ke dua',2016,1,1,NULL,'2015-11-27 10:37:09','2015-11-27 10:41:43');
/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prosentase`
--

DROP TABLE IF EXISTS `prosentase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prosentase` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `fisik_value` int(50) unsigned NOT NULL,
  `sppd_value` int(11) unsigned NOT NULL,
  `spj_value` int(11) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prosentase`
--

LOCK TABLES `prosentase` WRITE;
/*!40000 ALTER TABLE `prosentase` DISABLE KEYS */;
INSERT INTO `prosentase` VALUES (1,1,5,7,94,95,100,NULL,'2015-11-01 14:50:51','2015-11-27 09:56:46'),(5,1,2,1,81,72,73,NULL,'2015-11-01 15:19:06','2015-11-27 10:05:03'),(6,1,2,4,94,100,100,NULL,'2015-11-02 15:34:17','2015-11-27 10:05:03');
/*!40000 ALTER TABLE `prosentase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tahap`
--

DROP TABLE IF EXISTS `tahap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tahap` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tahap`
--

LOCK TABLES `tahap` WRITE;
/*!40000 ALTER TABLE `tahap` DISABLE KEYS */;
INSERT INTO `tahap` VALUES (1,'Tahap 1',NULL,NULL,NULL),(2,'Tahap 2',NULL,NULL,NULL),(3,'Tahap 3',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tahap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `for_register` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,'Administrator','0',NULL,'2015-03-24 10:24:36'),(2,'Operator','0',NULL,'2015-03-24 10:24:31'),(3,'User SKPD/KPA','1',NULL,NULL);
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_kegiatan`
--

DROP TABLE IF EXISTS `user_kegiatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_kegiatan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_program` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `pagu_kegiatan` varchar(100) NOT NULL,
  `pagu_kegiatan_perubahan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_kegiatan`
--

LOCK TABLES `user_kegiatan` WRITE;
/*!40000 ALTER TABLE `user_kegiatan` DISABLE KEYS */;
INSERT INTO `user_kegiatan` VALUES (2,5,7,2,'800000000','','',NULL,'2015-10-29 06:22:02','2015-11-27 10:04:28'),(4,5,8,2,'9000000','80000','dae',NULL,'2015-10-29 07:06:08','2015-11-27 10:04:28'),(5,5,7,1,'2000000','','nul',NULL,'2015-10-29 07:08:55','2015-11-27 10:04:28'),(6,2,1,1,'800000','','kk',NULL,'2015-10-31 14:49:58','2015-11-27 10:05:03'),(7,2,4,1,'90','100000','s',NULL,'2015-10-31 14:53:48','2015-11-27 10:05:03'),(8,4,5,1,'800000','','',NULL,'2015-11-04 15:26:07','2015-11-27 10:06:00');
/*!40000 ALTER TABLE `user_kegiatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_program`
--

DROP TABLE IF EXISTS `user_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_program` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_program`
--

LOCK TABLES `user_program` WRITE;
/*!40000 ALTER TABLE `user_program` DISABLE KEYS */;
INSERT INTO `user_program` VALUES (8,2,1,NULL,'2015-10-27 07:11:09','2015-11-27 10:05:03'),(10,5,1,NULL,'2015-10-27 08:53:20','2015-11-27 10:04:28'),(11,5,2,NULL,'2015-10-28 03:20:28','2015-11-27 10:04:28'),(12,2,2,NULL,'2015-10-28 04:10:25','2015-11-27 10:05:03'),(13,2,3,NULL,'2015-10-28 04:10:50','2015-11-27 10:05:03'),(16,4,1,NULL,'2015-11-04 15:39:51','2015-11-27 10:06:00');
/*!40000 ALTER TABLE `user_program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Nuli','testing@admin.com','d58b87ef91477cdee2ccb45b2c49bc69','sdada','081915804771','1',1,NULL,'2014-10-25 02:58:52','2015-03-24 17:09:44');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitor`
--

DROP TABLE IF EXISTS `visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(32) NOT NULL,
  `id_article` int(10) unsigned DEFAULT NULL,
  `type` enum('1','2','3','4') NOT NULL COMMENT '1. products, 2. news, 3. Gallery, 4. general',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitor`
--

LOCK TABLES `visitor` WRITE;
/*!40000 ALTER TABLE `visitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `visitor` ENABLE KEYS */;
UNLOCK TABLES;
