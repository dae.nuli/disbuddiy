-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 17, 2018 at 06:37 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diy_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `web_keywords` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `gplus` varchar(100) NOT NULL,
  `bbm` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `home_description` text NOT NULL,
  `blog_description` text NOT NULL,
  `how_to_buy_description` text NOT NULL,
  `order_description` text NOT NULL,
  `contact_description` text NOT NULL,
  `product_description` text NOT NULL,
  `under` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `name`, `email`, `web_keywords`, `address`, `facebook`, `twitter`, `instagram`, `gplus`, `bbm`, `phone`, `home_description`, `blog_description`, `how_to_buy_description`, `order_description`, `contact_description`, `product_description`, `under`, `created_at`, `updated_at`) VALUES
(1, 'Monev', 'giarsyani.nuli@gmail.com', 'hello', 'Jl. Kaliurang KM 14 Perumahan Pamungkas No. AA04 Yogyakarta', 'giarsyaninuli', 'giarsyaninuli', 'http://instagram.com/arinigrafika', '', '2494612', '081915804771', 'home is a', 'blog is a', 'how to buy is a menu', 'order is menu', 'menu is contanct', 'produk adalah', 80, NULL, '2015-11-02 08:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_controller` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `id_group`, `id_controller`, `deleted_at`, `created_at`, `updated_at`) VALUES
(65, 2, 8, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(66, 2, 9, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(67, 2, 10, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(68, 2, 13, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(69, 2, 14, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(70, 2, 15, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(71, 2, 17, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(72, 2, 21, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(73, 2, 22, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(74, 2, 23, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(75, 2, 24, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(76, 2, 25, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(77, 2, 26, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `complains`
--

CREATE TABLE `complains` (
  `id` int(10) UNSIGNED NOT NULL,
  `skpd_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `kegiatan_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `complains`
--

INSERT INTO `complains` (`id`, `skpd_id`, `program_id`, `kegiatan_id`, `name`, `email`, `phone_number`, `subject`, `content`, `created_at`, `updated_at`) VALUES
(1, 2, 5, 7, 'Nuli', 'Nuli@gmail.com', '081915804771', '', 'Hey can you explain in simple what this product does, because I wonder if this product can give as many required likes to your post', '2017-10-27 16:48:05', '2017-10-27 16:48:05'),
(2, 2, 5, 8, 'Dae', 'dae@gmail.com', '081915804771', '', 'You really need to incorporate an email notification or something when re-login is required. It frustrates me to have to login every day to check if re-login is required (bot stopped).  Thanks.', '2017-10-27 16:49:02', '2017-10-27 16:49:02'),
(3, 2, 5, 7, 'Mas', 'masto@gmail.com', '081195804771', '', 'I have installed and activated both and even added the # tags in which to like. I have set the speed to very fast and active, these settings have been set for around 24 hours now yet my account still hasn’t liked 1 photo.', '2017-10-27 16:49:39', '2017-10-27 16:49:39'),
(4, 1, 5, 7, 'Gaga', 'g@gmail.com', '29892', '', 'I would like to charge end user, do i need to select extended license on both main code and on addon as this?', '2017-10-27 16:55:06', '2017-10-27 16:55:06'),
(5, 2, 5, 7, 'rashul', 'hello@am.com', '293720', '', 'can i offer auto like service to my client with this app with your main app Nextpost ? i am looking to offer some service like people sign up server and app send auto likes for every new post on IG. please let me know if we can achieve this. thanks', '2017-10-27 16:56:32', '2017-10-27 16:56:32'),
(7, 2, 5, 7, 'Hello', 'he@gmail.com', '081', NULL, 'heeeeee', '2017-11-01 21:52:41', '2017-11-01 21:52:41'),
(8, 2, NULL, NULL, 'Herman', 'her@gmail.com', '08191', 'Judul pengaduan', 'Isi pengaduan', '2017-11-01 23:22:43', '2017-11-01 23:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `controllers`
--

CREATE TABLE `controllers` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `controllers`
--

INSERT INTO `controllers` (`id`, `id_parent`, `name`, `url`, `description`, `icon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Blog', NULL, 'Blog Parent', 'fa fa-file-o', NULL, '2015-03-24 08:02:15', '2015-03-24 08:02:15'),
(2, 1, 'Category', 'news_category', 'Blog Category', '', NULL, '2015-03-24 08:03:03', '2015-03-24 08:03:03'),
(3, 1, 'List', NULL, 'Blog List', '', NULL, '2015-03-24 08:05:39', '2015-03-24 08:05:39'),
(4, NULL, 'Products', NULL, 'Products', 'fa fa-list-alt', NULL, '2015-03-24 08:08:53', '2015-03-24 08:08:53'),
(5, 4, 'Category', 'product_category', 'Product Category', '', NULL, '2015-03-24 08:11:07', '2015-03-24 08:11:07'),
(6, 4, 'Sub Category', 'product_subcategory', 'Product Sub Category', '', NULL, '2015-03-24 08:12:13', '2015-03-24 08:12:13'),
(7, 4, 'List', 'products', 'Product List', '', NULL, '2015-03-24 08:12:36', '2015-03-24 08:12:36'),
(8, NULL, 'Orders', 'orders', 'Orders List', 'fa fa-shopping-cart', NULL, '2015-03-24 08:13:06', '2015-03-24 08:13:06'),
(9, NULL, 'Clients', 'clients', 'Clients List', 'fa fa-users', NULL, '2015-03-24 08:13:32', '2015-03-24 08:13:32'),
(10, NULL, 'Best Seller', 'best', 'Best Seller List', 'fa fa-star', NULL, '2015-03-24 08:14:07', '2015-03-24 08:14:07'),
(11, NULL, 'Testimony', 'testimony', 'Testimony List', 'fa fa-comments', NULL, '2015-03-24 08:14:49', '2015-03-24 08:14:49'),
(12, NULL, 'Gallery', 'gallery', 'Gallery List', 'fa fa-picture-o', NULL, '2015-03-24 08:15:20', '2015-03-24 08:15:20'),
(13, NULL, 'Management Staff', NULL, 'Management Staff Menu', 'fa fa-user', NULL, '2015-03-24 09:30:57', '2015-03-24 09:30:57'),
(14, 13, 'List', 'users', 'Staff List', '', NULL, '2015-03-24 09:31:27', '2015-03-24 09:39:55'),
(15, 13, 'Deleted Staff', 'deleted-staff', 'Deleted Staff List', '', NULL, '2015-03-24 09:31:53', '2015-03-24 09:31:53'),
(17, NULL, 'About', 'about', 'About Menu', 'fa fa-info-circle', NULL, '2015-03-24 08:18:46', '2015-03-24 08:53:07'),
(21, NULL, 'Reports', 'report', 'Reports Menu', 'fa fa-bar-chart-o', NULL, '2015-03-24 09:33:59', '2015-04-14 08:26:05'),
(22, NULL, 'Bank', 'bank', 'Bank is menu for input bank account', 'fa fa-usd', NULL, '2015-04-14 08:04:07', '2015-04-14 08:04:07'),
(23, NULL, 'How To Buy', 'how', 'is menu for description some step to buy product', 'fa fa-question-circle', NULL, '2015-04-14 08:05:37', '2015-04-14 08:05:37'),
(24, NULL, 'Banner', 'banner', 'is menu to set banner content', 'fa fa-desktop', NULL, '2015-04-14 08:06:17', '2015-04-14 08:06:17'),
(25, NULL, 'Statistics', 'statistics', 'is menu to view visitor activity', 'fa fa-signal', NULL, '2015-04-14 08:08:07', '2015-04-14 08:08:07'),
(26, NULL, 'Messages', 'messages', 'Is all about contact ', 'fa fa-envelope', NULL, '2015-04-18 02:14:15', '2015-04-18 02:14:15');

-- --------------------------------------------------------

--
-- Table structure for table `detail_tahap`
--

CREATE TABLE `detail_tahap` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_tahap` int(10) UNSIGNED NOT NULL,
  `month` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `detail_tahap`
--

INSERT INTO `detail_tahap` (`id`, `id_tahap`, `month`, `year`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2015, NULL, NULL, NULL),
(2, 1, 2, 2015, NULL, NULL, NULL),
(3, 1, 3, 2015, NULL, NULL, NULL),
(4, 2, 4, 2015, NULL, NULL, NULL),
(5, 2, 5, 2015, NULL, NULL, NULL),
(6, 2, 6, 2015, NULL, NULL, NULL),
(7, 2, 7, 2015, NULL, NULL, NULL),
(8, 3, 8, 2015, NULL, NULL, NULL),
(9, 3, 9, 2015, NULL, NULL, NULL),
(10, 3, 10, 2015, NULL, NULL, NULL),
(11, 3, 11, 2015, NULL, NULL, NULL),
(12, 3, 12, 2015, NULL, NULL, NULL),
(17, 1, 1, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(18, 1, 2, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(19, 1, 3, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(20, 1, 4, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(21, 1, 5, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(22, 2, 6, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(23, 2, 7, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(24, 2, 8, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(25, 2, 9, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(26, 2, 10, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(27, 3, 11, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(28, 3, 12, 2017, NULL, '2015-11-01 00:31:10', '2015-11-01 00:31:10'),
(41, 1, 1, 2016, NULL, '2015-11-01 02:47:19', '2015-11-01 02:47:19'),
(42, 1, 2, 2016, NULL, '2015-11-01 02:47:19', '2015-11-01 02:47:19'),
(43, 1, 3, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(44, 1, 4, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(45, 1, 5, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(46, 1, 6, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(47, 1, 7, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(48, 1, 8, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(49, 1, 9, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(50, 1, 10, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(51, 1, 11, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:20'),
(52, 1, 12, 2016, NULL, '2015-11-01 02:47:20', '2015-11-01 02:47:38');

-- --------------------------------------------------------

--
-- Table structure for table `item_kegiatan`
--

CREATE TABLE `item_kegiatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_kegiatan` int(10) UNSIGNED NOT NULL,
  `id_detail_tahap` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vol` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `satuan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fisik_rencana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fisik_realisasi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anggaran_rencana` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anggaran_sppd` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anggaran_spj` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_kegiatan`
--

INSERT INTO `item_kegiatan` (`id`, `id_user`, `id_kegiatan`, `id_detail_tahap`, `name`, `vol`, `satuan`, `fisik_rencana`, `fisik_realisasi`, `anggaran_rencana`, `anggaran_sppd`, `anggaran_spj`, `keterangan`, `photo`, `deleted_at`, `created_at`, `updated_at`) VALUES
(24, 35, 38, 19, 'Pembangunan Jalan', '100', 'm2', '100', '100', '200000000', '10000000', '100000000', 'Sedang lelang', '582936.png', NULL, '2017-11-06 06:20:48', '2017-11-06 06:24:04'),
(25, 37, 33, 17, 'Pembuatan gapura', '1', 'unit', '100', NULL, '30000000', NULL, NULL, 'Sedang dilaksanakan', NULL, NULL, '2017-11-06 08:39:36', '2017-11-06 08:39:36'),
(26, 15, 29, 26, 'Gelar Mocopat', '18', 'kecamatan', '100', '95', '120000000', '120000000', '120000000', 'Gelar Mocopat tingkat Kecamatan sekabupaten Gunungkidul', '752390.jpg', NULL, '2017-11-06 11:05:55', '2017-11-06 11:07:39'),
(27, 20, 38, 27, 'pawai budaya', '1', 'kali', '30', NULL, '20000000', NULL, NULL, 'pawai budaya bogor', NULL, NULL, '2017-11-06 11:07:54', '2017-11-06 11:07:54'),
(28, 14, 41, 26, 'Sewa Kendaraan Operasional', '2', 'unit', '100', '100', '10000000', '10000000', '10000000', 'Sudah terlaksana', NULL, NULL, '2017-11-06 11:08:09', '2017-11-06 11:10:26'),
(29, 24, 38, 17, 'pengadaan komputer', '1', 'unit', '10', '10', '8500000', '8500000', '8250000', '8250000', NULL, NULL, '2017-11-06 11:08:15', '2017-11-06 11:09:21'),
(30, 12, 13, 26, 'Pembinaan Desa Budaya', '54', 'Desa', '100', '80', '300000000', '300000000', '290000000', 'Fasilitasi Upacara Adat', NULL, NULL, '2017-11-06 11:08:19', '2017-11-06 11:10:00'),
(31, 12, 29, 28, 'Macapat jaman now', '1', 'kegiatan', '100', NULL, '125000000', NULL, NULL, 'Sudah terlaksana dengan lancar.', '484595.jpg', NULL, '2017-11-06 11:08:25', '2017-11-06 11:08:25'),
(32, 16, 41, 26, 'Gelar Potensi Keluharan Budaya', '2', 'kali', '100', '100', '100000000', '100000000', '100000000', 'Dilaksanakan di Terban dan Kricak', NULL, NULL, '2017-11-06 11:08:27', '2017-11-06 11:09:13'),
(33, 27, 38, 17, 'festival perahu nelayan', '50', 'org', '100', NULL, '50000000', NULL, NULL, '-', NULL, NULL, '2017-11-06 11:08:34', '2017-11-06 11:08:34'),
(34, 12, 33, 24, 'Registrasi Cagar  Budaya', '1', 'Paket', '100', '100', '2500000', '2500000', '2450000', 'Mengetahui daftar Cagar Budaya', NULL, NULL, '2017-11-06 11:08:36', '2017-11-06 11:09:51'),
(35, 21, 38, 24, 'pembuatan gappura', '1', 'unit', '50', '45', '50000000', '25000000', '24000000', 'fondasi', NULL, NULL, '2017-11-06 11:08:47', '2017-11-06 11:09:53'),
(36, 12, 17, 27, 'Kuncung Bawuk', '56', 'Tayang', '100', '100', '123000000', '123000000', '12000000', 'sudah selesai terlaksana', NULL, NULL, '2017-11-06 11:09:32', '2017-11-06 11:10:37'),
(37, 22, 14, 26, 'Penyusunan Peta Perubahan Sosial dan Potensi Konflik', '1', 'Paket', '100', NULL, '50000000', NULL, NULL, '', NULL, NULL, '2017-11-06 11:10:27', '2017-11-06 11:10:27'),
(38, 12, 30, 26, 'wayang Pepadang', '1', 'kali', '100', NULL, '50000000', NULL, NULL, '', NULL, NULL, '2017-11-06 11:10:33', '2017-11-06 11:10:33'),
(39, 15, 29, 21, 'Gelar mocopat', '18', 'kecamatan', '100', '95', '120000000', '120000000', '120000000', 'gelar mocopat tingkat kecamatan', NULL, NULL, '2017-11-06 11:11:09', '2017-11-06 11:12:24'),
(40, 13, 41, 23, 'Pentas seni  di Desa Kantong', '59', 'grup', '100', NULL, '851000000', NULL, NULL, 'juni, juli, aagustus, september, november', NULL, NULL, '2017-11-06 11:11:17', '2017-11-06 11:11:17'),
(41, 18, 38, 27, 'Belanja Publikasi', '2', 'tayang', '100', NULL, '20000000', NULL, NULL, '-', NULL, NULL, '2017-11-06 11:11:56', '2017-11-06 11:11:56'),
(42, 12, 29, 27, 'Macapat kontemporer', '1', 'kegiatan', '98', NULL, '98000000', NULL, NULL, 'Terlaksana tanpa hambatan ', '444896.JPG', NULL, '2017-11-06 11:12:35', '2017-11-06 11:12:35'),
(43, 17, 35, 26, 'Rehab Bangunan Makam Raja-Raja di Imogiri', '1', 'Paket', '100', NULL, '100', NULL, NULL, '', NULL, '2017-11-06 11:13:46', '2017-11-06 11:13:35', '2017-11-06 11:13:46'),
(44, 15, 29, 28, 'sarasehan', '1', 'keg', '100', NULL, '2000000', NULL, NULL, 'sarasehan bahasa satra', NULL, NULL, '2017-11-06 11:14:55', '2017-11-06 11:14:55'),
(45, 29, 37, 19, 'Jogja Fashion Week', '1', 'kali', '30', NULL, '20000000', NULL, NULL, 'Persiapan pelaksanaan JFW 2017 ( Koordinasi dan konsultasi ke kementerian)', NULL, NULL, '2017-11-06 11:15:23', '2017-11-06 11:15:23'),
(46, 15, 29, 18, 'sarasehan', '4', 'kegiatan', '100', NULL, '1000', NULL, NULL, 'keterngan', NULL, NULL, '2017-11-06 11:16:05', '2017-11-06 11:16:05'),
(47, 12, 13, 17, 'Pembinaan Desa Budaya', '12', 'kali', '100', '90', '100000000', '100000000', '90000000', '-', NULL, '2017-11-08 10:10:53', '2017-11-06 11:18:59', '2017-11-08 10:10:53'),
(48, 14, 15, 26, 'Workshop Pembinaan Perfilman', '1', 'kali', '100', '100', '75000000', '71400000', '71400000', 'Sudah terlaksana 13 - 15 Oktober 2017', NULL, NULL, '2017-11-06 11:19:35', '2017-11-06 11:20:02'),
(49, 12, 13, 28, 'Upacara Adat', '12', 'kali', '100', '100', '1000000000', '100000000', '100000000', '-', NULL, NULL, '2017-11-06 11:19:38', '2017-11-06 11:21:10'),
(50, 23, 19, 18, 'aaa', '1', 'a', '111', NULL, '111', NULL, NULL, '111', NULL, NULL, '2017-11-06 11:21:17', '2017-11-06 11:21:17'),
(51, 12, 33, 17, 'Sadar lestari', '1', 'Kali', '12', '11', '15000000', '12000000', '13000000', 'Sosialisasi', NULL, NULL, '2017-11-06 11:21:47', '2017-11-06 11:23:08'),
(52, 25, 39, 26, 'aplikasi idmc', '1', '1', '80', NULL, '457000000', NULL, NULL, 'selesai di bulan Nopember 2017', NULL, NULL, '2017-11-06 11:22:04', '2017-11-06 11:22:04'),
(53, 12, 13, 23, 'adat tradisi', '100', 'kl', '1000', '100', '1000000000', '1000000000', '1000000000', '-', NULL, NULL, '2017-11-06 11:25:24', '2017-11-06 11:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_program` int(10) UNSIGNED NOT NULL,
  `code_kegiatan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pagu_kegiatan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `id_program`, `code_kegiatan`, `name`, `pagu_kegiatan`, `description`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(13, 8, '3.03.55.001', 'Pembinaan Desa Budaya', '7607327375', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 8, '3.03.55.003', 'Pembinaan dan Pengembangan Seni Rupa Daerah', '2896267650', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 8, '3.03.55.004', 'Pembinaan dan Pengembangan Perfilman', '4610884100', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 8, '3.03.55.005', 'Pengadaaan Sarana dan Prasarana Kesenian ke Masyarakat', '2477688000', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 8, '3.03.55.008', 'Promosi dan Publikasi Seni Budaya', '3023328200', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 8, '3.03.55.009', 'Penyelenggaraan Even Lembaga Penggiat Seni', '7336673675', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 8, '3.03.55.010', 'Gelar Budaya Jogja', '1205000000', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 8, '3.03.55.011', 'Jogja International Heritage Festival', '1000000000', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 8, '3.03.55.012', 'Festival Kebudayaan Yogyakarta', '4027753000', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 9, '3.03.56.001', 'Misi Kebudayaan ke Luar Negeri Dalam Rangka Diplomasi Budaya', '5441470000', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 9, '3.03.56.002', 'Membangun Kemitraan Dengan Instansi di Luar DIY', '4081655950', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 9, '3.03.56.003', 'Membangun Kemitraan dengan Lembaga Pelestari Budaya', '3710136750', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 9, '3.03.56.004', 'Selendang Sutera (Semarak Legenda Suku-suku Nusantara)', '2132030800', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 9, '3.03.56.005', 'Penghargaan Bagi Pelestari dan Penggiat Budaya', '850000000', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 9, '3.03.56.008', 'Perencanaan, Monitoring dan Evaluasi Program dan Kegiatan Keistimewaan Urusan Kebudayaan', '650000000', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 10, '3.03.57.001', 'Pembinaan dan Pengembangan Kesejarahan', '3407199200', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 10, '3.03.57.002', 'Pembinaan dan Pengembangan Bahasa dan Sastra', '2879963800', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 10, '3.03.57.003', 'Pelestarian, Pengembangan dan Aplikasi Nilai-nilai Luhur Budaya Luhur di Masyarakat', '2069312500', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 10, '3.03.57.004', 'Pelestarian Kepercayaan dan Tradisi', '2199195000', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 11, '3.03.58.001', 'Tata Kelola Warisan Budaya dan Cagar Budaya', '21042167200', NULL, NULL, NULL, NULL, NULL, NULL),
(34, 11, '3.03.58.002', 'Penguatan Lembaga Pengelola dan Pelestari Warisan Budaya', '6745000000', NULL, NULL, NULL, NULL, NULL, NULL),
(35, 11, '3.03.58.003', 'Pelestarian Warisan Budaya Benda dan Cagar Budaya', '18688986600', NULL, NULL, NULL, NULL, NULL, NULL),
(36, 11, '3.03.58.004', 'Nominasi Warisan Budaya Nasional dan Warisan Budaya Indonesia', '2196929200', NULL, NULL, NULL, NULL, NULL, NULL),
(37, 12, '3.03.59.001', 'Pembinaan Permuseuman', '12523000000', NULL, NULL, NULL, NULL, NULL, NULL),
(38, 13, '3.03.60.001', 'Peningkatan Sarana dan Prasarana Lembaga Pelaksana Urusan Keistimewaan', '9525302430', NULL, NULL, NULL, NULL, NULL, NULL),
(39, 9, '30356006', 'Pengembangan Ekosistem Kultural DIY Berbasis Digital', NULL, '', NULL, NULL, NULL, '2017-11-06 04:41:36', '2017-11-06 04:42:06'),
(40, 8, '30355012', 'Pengelolaan dan pengembangan taman budaya ', NULL, NULL, NULL, NULL, NULL, '2017-11-06 04:46:39', '2017-11-06 04:46:39'),
(41, 8, '30355013', 'Pengembangan Rintisan Desa dan Kantong Budaya', NULL, NULL, NULL, NULL, NULL, '2017-11-06 05:17:20', '2017-11-06 05:17:20');

-- --------------------------------------------------------

--
-- Table structure for table `kpa_users`
--

CREATE TABLE `kpa_users` (
  `id` int(11) NOT NULL,
  `code_kpa` varchar(255) NOT NULL,
  `instansi_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_staff` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `bidang` varchar(100) NOT NULL,
  `personil_name` varchar(255) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_complete` enum('2','1','0') DEFAULT '0',
  `activation` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kpa_users`
--

INSERT INTO `kpa_users` (`id`, `code_kpa`, `instansi_name`, `email`, `email_staff`, `username`, `phone`, `address`, `bidang`, `personil_name`, `password`, `is_active`, `is_complete`, `activation`, `deleted_at`, `created_at`, `updated_at`) VALUES
(12, '001', 'Dinas Kebudayaan DIY', 'disbuddiy@gmail.com', 'info@geekgarden.id', 'disbuddiy', '08787877811', 'Jalan Cendana', '', 'Nur Ikhwan Rahmanto', '31a5efd1ad39b5ec5141c42fe595365455ba0d17', '1', '2', 'AgJ26OnR9bP4HWYa5uuT', NULL, '2017-11-05 17:41:31', '2017-11-05 22:20:48'),
(13, '002', 'Dinas Kebudayaan Kabupaten Kulon Progo DIY', 'disbudkulonprogo@mail.com', '', 'disbudkulonprogo', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 17:51:23', '2017-11-06 09:49:23'),
(14, '003', 'Dinas Kebudayaan Kabupaten Bantul DIY', 'disbudbantul@mail.com', '', 'disbudbantul', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 17:53:44', '2017-11-06 09:49:17'),
(15, '004', 'Dinas Kebudayaan Kabupaten Gunung Kidul DIY', 'disbudgunungkidul@mail.com', '', 'disbudgunungkidul', '0274391086', 'Jalan Pangarsan Komplek Bbangsal Sewokoprojo, Wonosari, Gunungkidul', '', 'Drs Agus Kamtono, MM', 'f87b2bcbcba77a5246276a0fad4c6fd86f6ac3f8', '1', '2', '', NULL, '2017-11-05 17:54:32', '2017-11-06 10:56:57'),
(16, '005', 'Dinas Kebudayaan Kota Yogyakarta', 'disbudkotajogja@mail.com', '', 'disbudkotajogja', '081226872092', 'Jl. Kemasan No. 39 Purbayan Kotagede Yogyakarta', '', 'Achmad Hadi Ramadhana', '71036cc6cbd5a2949b804a660360d6478a93a57d', '1', '2', '', NULL, '2017-11-05 19:44:56', '2017-11-06 10:58:11'),
(17, '006', 'Dinas Pekerjaan Umum, Perumahan dan Energi Sumberdaya Mineral DIY', 'pupesdmdiy@mail.com', '', 'pupesdmdiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:45:54', '2017-11-06 09:48:15'),
(18, '007', 'Dinas Lingkungan Hidup DIY', 'lhdiy@mail.com', '', 'lhdiy', '085868771030', 'Jalan Tentara Rakyat Mataram No 53 Yogyakarta', '', 'Cahyani Alfiah', 'f80acdd7a85424106c79a842dce97e655727b7b4', '1', '2', '', NULL, '2017-11-05 19:46:38', '2017-11-06 10:42:21'),
(19, '008', 'Dinas Pekerjaan Umum Kabupaten Kulon Progo DIY', 'pukulonprogo@mail.com', '', 'pukulonprogo', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:47:39', '2017-11-06 09:48:07'),
(20, '009', 'Kantor Perwakilan Daerah Pemda DIY', 'kpdpdiy@mail.com', '', 'kpdpdiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:48:31', '2017-11-06 09:48:02'),
(21, '010', 'Dinas Pendidikan, Pemuda dan Olah Raga DIY', 'dispenporadiy@mail.com', '', 'dispenporadiy', '081578696234', 'Jalan Cendana No.9 Yogyakarta', '', 'Intan Galuh', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:49:42', '2017-11-06 10:55:56'),
(22, '011', 'Badan Kesatuan Bangsa dan Politik DIY', 'kesbangpoldiy@mail.com', '', 'kesbangpoldiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:51:32', '2017-11-06 09:47:56'),
(23, '012', 'Badan Perpustakaan dan Arsip Daerah DIY', 'bpaddiy@mail.com', '', 'bpaddiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:52:45', '2017-11-06 09:47:49'),
(24, '013', 'Badan Perencanaan Pembangunan Daerah DIY', 'bappedadiy@mail.com', '', 'bappedadiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:54:01', '2017-11-06 09:47:37'),
(25, '014', 'Dinas Komunikasi dan Informatika DIY', 'diskominfodiy@mail.com', '', 'diskominfodiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:55:18', '2017-11-06 09:47:22'),
(26, '015', 'Dinas Kehutanan dan Perkebunan DIY', 'dkpdiy@mail.com', '', 'dkpdiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:56:28', '2017-11-06 09:47:17'),
(27, '016', 'Dinas Kelautan dan Perikanan DIY', 'dislautkandiy@mail.com', '', 'dislautkandiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:57:49', '2017-11-06 08:22:15'),
(28, '017', 'Badan Ketahanan Pangan dan Penyuluhan DIY', 'bkppdiy@mail.com', '', 'bkppdiy', '0274540798', 'Jl. Gondosuli No. 6 Yogyakarta', '', 'Yosephine R. A.', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 19:59:01', '2017-11-06 10:55:54'),
(29, '018', 'Dinas Pariwisata DIY', 'pariwisatadiy@mail.com', '', 'pariwisatadiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 20:00:01', '2017-11-06 08:22:07'),
(30, '019', 'Museum Sonobudoyo DIY', 'sonobudoyodiy@mail.com', '', 'sonobudoyodiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 20:00:40', '2017-11-06 08:22:04'),
(31, '020', 'Taman Budaya Yogyakarta DIY', 'tamanbudayadiy@mail.com', '', 'tamanbudayadiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 20:01:54', '2017-11-06 08:22:00'),
(32, '021', 'Balai Pelestarian Warisan dan Cagar Budaya DIY', 'bpwcbdiy@mail.com', '', 'bpwcbdiy', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-05 20:03:06', '2017-11-06 08:14:20'),
(33, '022', 'Dinas Kebudayaan Kabupaten Sleman DIY', 'disbudsleman@mail.com', 'joediethfanspersibselalu@gmail.com', 'disbudsleman', '085643079550', 'Plosokuning II Minomartani Ngaglik Sleman Yogyakarta', '', 'Armanda Judith Enrian', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', 'xOKOjq4Kx2RL6LIOjF9w', NULL, '2017-11-05 20:09:56', '2017-11-06 10:55:44'),
(35, '1212121', 'Dinas contoh', 'hrd@geekgarden.id', 'imandebo@yahoo.com', 'hrd', '23892382', 'dakjda', '', 'iman', '31a5efd1ad39b5ec5141c42fe595365455ba0d17', '1', '2', 'XjoXwg0EU2IK2vLIIEmU', NULL, '2017-11-06 05:35:04', '2017-11-06 05:36:40'),
(36, '2121212', 'BPPT', 'giarsyani.nuli@gmail.com', '', 'giarsyani.nuli', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-06 07:43:59', '2017-11-06 07:44:02'),
(37, '120101', 'Badan conto', 'badancontoh@mail.com', '', 'badancontoh', '', '', '', '', 'f8221682b0d2e1baf27b46963870995fa88adc4e', '1', '2', '', NULL, '2017-11-06 08:12:01', '2017-11-06 08:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `laq_async_queue`
--

CREATE TABLE `laq_async_queue` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `retries` int(11) NOT NULL DEFAULT '0',
  `delay` int(11) NOT NULL DEFAULT '0',
  `payload` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `laq_async_queue`
--

INSERT INTO `laq_async_queue` (`id`, `status`, `retries`, `delay`, `payload`, `created_at`, `updated_at`) VALUES
(10, 2, 1, 0, '{"job":"mailer@handleQueuedMessage","data":{"view":"backend.login.message","data":{"username":"giarsyani.nuli","password":"ObokYE6"},"callback":"C:38:\\"Illuminate\\\\Support\\\\SerializableClosure\\":1783:{a:2:{i:0;s:107:\\"function ($message) use($km) {\\n    $message->to($km->email, $km->instansi_name)->subject(\'New Account\');\\n};\\";i:1;a:1:{s:2:\\"km\\";O:8:\\"KPAModel\\":21:{s:13:\\"\\u0000*\\u0000softDelete\\";b:1;s:8:\\"\\u0000*\\u0000table\\";s:9:\\"kpa_users\\";s:13:\\"\\u0000*\\u0000connection\\";N;s:13:\\"\\u0000*\\u0000primaryKey\\";s:2:\\"id\\";s:10:\\"\\u0000*\\u0000perPage\\";i:15;s:12:\\"incrementing\\";b:1;s:10:\\"timestamps\\";b:1;s:13:\\"\\u0000*\\u0000attributes\\";a:17:{s:2:\\"id\\";i:8;s:8:\\"code_kpa\\";s:4:\\"1212\\";s:13:\\"instansi_name\\";s:40:\\"Badan pengkajian dan penerapan teknologi\\";s:5:\\"email\\";s:24:\\"giarsyani.nuli@gmail.com\\";s:11:\\"email_staff\\";s:0:\\"\\";s:8:\\"username\\";s:14:\\"giarsyani.nuli\\";s:5:\\"phone\\";s:0:\\"\\";s:7:\\"address\\";s:0:\\"\\";s:6:\\"bidang\\";s:0:\\"\\";s:13:\\"personil_name\\";s:0:\\"\\";s:8:\\"password\\";s:40:\\"bb33593019b75ac02f71dad4cbf9ea3349abff30\\";s:9:\\"is_active\\";i:1;s:11:\\"is_complete\\";s:1:\\"0\\";s:10:\\"activation\\";s:0:\\"\\";s:10:\\"deleted_at\\";N;s:10:\\"created_at\\";s:19:\\"2017-11-05 10:22:31\\";s:10:\\"updated_at\\";s:19:\\"2017-11-05 10:22:41\\";}s:11:\\"\\u0000*\\u0000original\\";a:17:{s:2:\\"id\\";i:8;s:8:\\"code_kpa\\";s:4:\\"1212\\";s:13:\\"instansi_name\\";s:40:\\"Badan pengkajian dan penerapan teknologi\\";s:5:\\"email\\";s:24:\\"giarsyani.nuli@gmail.com\\";s:11:\\"email_staff\\";s:0:\\"\\";s:8:\\"username\\";s:14:\\"giarsyani.nuli\\";s:5:\\"phone\\";s:0:\\"\\";s:7:\\"address\\";s:0:\\"\\";s:6:\\"bidang\\";s:0:\\"\\";s:13:\\"personil_name\\";s:0:\\"\\";s:8:\\"password\\";s:40:\\"bb33593019b75ac02f71dad4cbf9ea3349abff30\\";s:9:\\"is_active\\";i:1;s:11:\\"is_complete\\";s:1:\\"0\\";s:10:\\"activation\\";s:0:\\"\\";s:10:\\"deleted_at\\";N;s:10:\\"created_at\\";s:19:\\"2017-11-05 10:22:31\\";s:10:\\"updated_at\\";s:19:\\"2017-11-05 10:22:41\\";}s:12:\\"\\u0000*\\u0000relations\\";a:0:{}s:9:\\"\\u0000*\\u0000hidden\\";a:0:{}s:10:\\"\\u0000*\\u0000visible\\";a:0:{}s:10:\\"\\u0000*\\u0000appends\\";a:0:{}s:11:\\"\\u0000*\\u0000fillable\\";a:0:{}s:10:\\"\\u0000*\\u0000guarded\\";a:1:{i:0;s:1:\\"*\\";}s:8:\\"\\u0000*\\u0000dates\\";a:0:{}s:10:\\"\\u0000*\\u0000touches\\";a:0:{}s:14:\\"\\u0000*\\u0000observables\\";a:0:{}s:7:\\"\\u0000*\\u0000with\\";a:0:{}s:13:\\"\\u0000*\\u0000morphClass\\";N;s:6:\\"exists\\";b:1;}}}}"}}', '2017-11-05 10:22:41', '2017-11-05 10:22:41');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `active` enum('1','0') DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_09_20_173653_create_table_program', 1),
('2015_09_20_173710_create_table_kegiatan', 1),
('2015_09_20_173724_create_table_item_kegiatan', 1),
('2015_09_20_173748_create_table_fisik', 1),
('2015_09_20_173757_create_table_anggaran', 1),
('2015_09_20_173805_create_table_spj', 1),
('2015_09_20_173813_create_table_sp2d', 1),
('2015_09_29_112452_create_table_tahap', 2),
('2015_09_29_112500_create_table_detail_tahap', 3),
('2014_06_03_193005_create_queue_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(10) UNSIGNED NOT NULL,
  `code_program` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `year` year(4) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `code_program`, `name`, `year`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(8, '3.03.55', 'PROGRAM PENGEMBANGAN KESENIAN DAN BUDAYA DAERAH', 2017, NULL, NULL, NULL, NULL, NULL),
(9, '3.03.56', 'PROGRAM PROMOSI DAN KEMITRAAN BUDAYA DIY DI DALAM DAN LUAR NEGERI', 2017, NULL, NULL, NULL, NULL, NULL),
(10, '3.03.57', 'PROGRAM PENGELOLAAN NILAI DAN SEJARAH', 2017, NULL, NULL, NULL, NULL, NULL),
(11, '3.03.58', 'PROGRAM PENGELOLAAN CAGAR BUDAYA DAN WARISAN BUDAYA', 2017, NULL, NULL, NULL, NULL, NULL),
(12, '3.03.59', 'PROGRAM PEMBINAAN DAN PENGEMBANGAN MUSEUM', 2017, NULL, NULL, NULL, NULL, NULL),
(13, '3.03.60', 'PROGRAM PENINGKATAN SARANA DAN PRASARANA APARATUR', 2017, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prosentase`
--

CREATE TABLE `prosentase` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `fisik_value` int(50) UNSIGNED NOT NULL,
  `sppd_value` int(11) UNSIGNED NOT NULL,
  `spj_value` int(11) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prosentase`
--

INSERT INTO `prosentase` (`id`, `id_user`, `id_program`, `id_kegiatan`, `fisik_value`, `sppd_value`, `spj_value`, `deleted_at`, `created_at`, `updated_at`) VALUES
(7, 35, 13, 38, 100, 5, 50, NULL, '2017-11-06 06:20:48', '2017-11-06 06:24:04'),
(8, 37, 11, 33, 0, 0, 0, NULL, '2017-11-06 08:39:36', '2017-11-06 08:39:36'),
(9, 15, 10, 29, 48, 50, 50, NULL, '2017-11-06 11:05:55', '2017-11-06 11:16:05'),
(10, 20, 13, 38, 0, 0, 0, NULL, '2017-11-06 11:07:54', '2017-11-06 11:07:54'),
(11, 14, 8, 41, 100, 100, 100, NULL, '2017-11-06 11:08:09', '2017-11-06 11:10:26'),
(12, 24, 13, 38, 100, 100, 97, NULL, '2017-11-06 11:08:15', '2017-11-06 11:08:59'),
(13, 12, 8, 13, 64, 70, 69, NULL, '2017-11-06 11:08:19', '2017-11-08 10:10:53'),
(14, 12, 10, 29, 0, 0, 0, NULL, '2017-11-06 11:08:25', '2017-11-06 11:08:25'),
(15, 16, 8, 41, 100, 100, 100, NULL, '2017-11-06 11:08:27', '2017-11-06 11:09:13'),
(16, 27, 13, 38, 0, 0, 0, NULL, '2017-11-06 11:08:34', '2017-11-06 11:08:34'),
(17, 12, 11, 33, 96, 90, 92, NULL, '2017-11-06 11:08:36', '2017-11-06 11:22:52'),
(18, 21, 13, 38, 90, 50, 48, NULL, '2017-11-06 11:08:47', '2017-11-06 11:09:53'),
(19, 12, 8, 17, 100, 100, 9, NULL, '2017-11-06 11:09:32', '2017-11-06 11:10:37'),
(20, 22, 8, 14, 0, 0, 0, NULL, '2017-11-06 11:10:27', '2017-11-06 11:10:27'),
(21, 12, 10, 30, 0, 0, 0, NULL, '2017-11-06 11:10:33', '2017-11-06 11:10:33'),
(22, 13, 8, 41, 0, 0, 0, NULL, '2017-11-06 11:11:17', '2017-11-06 11:11:17'),
(23, 18, 13, 38, 0, 0, 0, NULL, '2017-11-06 11:11:56', '2017-11-06 11:11:56'),
(24, 17, 11, 35, 0, 0, 0, '2017-11-06 11:13:46', '2017-11-06 11:13:35', '2017-11-06 11:13:46'),
(25, 29, 12, 37, 0, 0, 0, NULL, '2017-11-06 11:15:23', '2017-11-06 11:15:23'),
(26, 14, 8, 15, 100, 95, 95, NULL, '2017-11-06 11:19:35', '2017-11-06 11:20:02'),
(27, 23, 8, 19, 0, 0, 0, NULL, '2017-11-06 11:21:17', '2017-11-06 11:21:17'),
(28, 25, 9, 39, 0, 0, 0, NULL, '2017-11-06 11:22:04', '2017-11-06 11:22:04');

-- --------------------------------------------------------

--
-- Table structure for table `tahap`
--

CREATE TABLE `tahap` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tahap`
--

INSERT INTO `tahap` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Tahap 1', NULL, NULL, NULL),
(2, 'Tahap 2', NULL, NULL, NULL),
(3, 'Tahap 3', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `address`, `phone`, `is_active`, `level`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'admin@admin.com', '49489e8fdd4f6f20dc41ba3b2706d4d9', 'sdada', '081915804771', '1', 1, NULL, '2014-10-24 19:58:52', '2017-11-05 17:30:15');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `for_register` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `group_name`, `for_register`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '0', NULL, '2015-03-24 03:24:36'),
(2, 'Operator', '0', NULL, '2015-03-24 03:24:31'),
(3, 'User SKPD/KPA', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_kegiatan`
--

CREATE TABLE `user_kegiatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `pagu_kegiatan` varchar(100) NOT NULL,
  `pagu_kegiatan_perubahan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_kegiatan`
--

INSERT INTO `user_kegiatan` (`id`, `id_program`, `id_kegiatan`, `id_user`, `pagu_kegiatan`, `pagu_kegiatan_perubahan`, `keterangan`, `deleted_at`, `created_at`, `updated_at`) VALUES
(11, 13, 38, 12, '9595229630', '9852498586', 'penambahan 257.268.956', NULL, '2017-11-05 20:24:34', '2017-11-05 20:24:34'),
(12, 12, 37, 13, '31799000', '31799000', 'tidak ada perubahan', NULL, '2017-11-05 20:26:27', '2017-11-05 20:26:27'),
(13, 11, 33, 12, '20803525550', '20053690550', 'Penambahan 143000000 dan efisiensi kegiatan 892,825,000', NULL, '2017-11-05 20:29:19', '2017-11-05 20:29:19'),
(14, 11, 35, 12, '18614878800', '18243011820', 'Penambahan 239.000.000 dan sisa lelang 610.866.980', NULL, '2017-11-05 20:31:20', '2017-11-05 20:31:20'),
(15, 11, 34, 12, '6797063000', '6797063000', 'tidak ada perubahan', NULL, '2017-11-05 20:32:21', '2017-11-05 20:32:21'),
(16, 11, 36, 12, '2446929200', '2742179200', 'Penambahan 345.000.000 dan efisiensi kegiatan 49.750.000', NULL, '2017-11-05 20:33:56', '2017-11-05 20:33:56'),
(17, 11, 35, 13, '598258000', '598258000', 'tidak ada perubahan', NULL, '2017-11-05 20:41:26', '2017-11-05 20:41:26'),
(18, 11, 35, 14, '3656567000', '1786567000', 'Efisiensi kegiatan 1.870.000.000', NULL, '2017-11-05 20:43:13', '2017-11-05 20:43:13'),
(19, 11, 35, 15, '982559000', '611909000', 'efisiensi kegiatan Rp.370.650.000', NULL, '2017-11-05 20:45:15', '2017-11-05 20:45:15'),
(20, 11, 35, 33, '1693174100', '1580244100', 'Sisa lelang 112.930.000', NULL, '2017-11-05 20:48:02', '2017-11-05 20:48:02'),
(21, 11, 35, 16, '2398230100', '2398230100', 'Tetap', NULL, '2017-11-05 20:50:01', '2017-11-05 20:50:01'),
(22, 11, 35, 17, '10183537718', '10183537718', 'tetap', NULL, '2017-11-05 20:51:00', '2017-11-05 20:51:00'),
(23, 11, 35, 26, '3500000000', '3500000000', 'Tetap', NULL, '2017-11-05 20:51:52', '2017-11-05 20:51:52'),
(24, 10, 28, 12, '3406807600', '4053807600', 'Penambahan Rp.647.000.000', NULL, '2017-11-05 20:56:27', '2017-11-05 20:56:27'),
(25, 10, 29, 12, '3006039000', '2564694000', 'Sisa lelang Rp 200.000.000 dan efisiensi Rp. 241.345.000', NULL, '2017-11-05 21:00:52', '2017-11-05 21:00:52'),
(26, 10, 30, 12, '2233333500', '2324233500', 'Penambahan Rp.90.900.000', NULL, '2017-11-05 21:02:41', '2017-11-05 21:02:41'),
(27, 10, 31, 12, '2199195000', '2199195000', 'tetap', NULL, '2017-11-05 21:03:26', '2017-11-05 21:03:26'),
(28, 10, 29, 13, '750000000', '750000000', 'Tetap', NULL, '2017-11-05 21:06:17', '2017-11-05 21:06:17'),
(29, 10, 31, 13, '1503000000', '1503000000', 'Tetap', NULL, '2017-11-05 21:06:56', '2017-11-05 21:06:56'),
(30, 10, 31, 14, '3074483600', '3074483600', 'Tetap', NULL, '2017-11-05 21:11:21', '2017-11-05 21:11:21'),
(31, 10, 29, 15, '220000000', '242000000', 'Penambahan 22.000.000', NULL, '2017-11-05 21:12:46', '2017-11-05 21:12:46'),
(32, 10, 30, 15, '150000000', '240000000', 'Penambahan 90.000.000', NULL, '2017-11-05 21:13:56', '2017-11-05 21:13:56'),
(33, 10, 29, 33, '513003500', '513003500', 'Tetap', NULL, '2017-11-05 21:15:35', '2017-11-05 21:15:35'),
(34, 10, 30, 33, '90781700', '90781700', 'Tetap', NULL, '2017-11-05 21:16:31', '2017-11-05 21:16:31'),
(35, 10, 28, 33, '979691000', '979691000', 'Tetap', NULL, '2017-11-05 21:17:14', '2017-11-05 21:17:14'),
(36, 9, 22, 12, '5156070000', '4829970000', 'Efisiensi Kegiatan Rp.326.100.000', NULL, '2017-11-05 21:21:28', '2017-11-05 21:21:28'),
(37, 9, 23, 12, '4949188550', '4949188550', 'tetap', NULL, '2017-11-05 21:22:07', '2017-11-05 21:22:07'),
(38, 9, 24, 12, '3927874350', '3986974350', 'Penambahan Rp.109.100.000 dan Efisiensi kegiatan Rp.50.000.000', NULL, '2017-11-05 21:23:46', '2017-11-05 21:23:46'),
(39, 9, 25, 12, '2132030800', '2132030800', 'Tetap', NULL, '2017-11-05 21:24:35', '2017-11-05 21:24:35'),
(40, 9, 26, 12, '850000000', '684967000', 'Sisa lelang Rp.45.033.000 dan Efisiensi Kegiatan Rp.120.000.000', NULL, '2017-11-05 21:25:54', '2017-11-05 21:25:54'),
(41, 9, 27, 12, '650000000', '695000000', 'Penambahan Rp.45.000.000', NULL, '2017-11-05 21:27:57', '2017-11-05 21:27:57'),
(42, 8, 13, 12, '7169163375', '6384365375', 'Efisiensi kegiatan Rp.784.798.000', NULL, '2017-11-05 21:30:44', '2017-11-05 21:30:44'),
(43, 8, 14, 12, '2895876050', '2687576050', 'Efisiensi kegiatan Rp.208.300.000', NULL, '2017-11-05 21:32:06', '2017-11-05 21:32:06'),
(44, 8, 15, 12, '4609905100', '4562405100', 'Efisiensi kegiatan Rp.47.500.000', NULL, '2017-11-05 21:33:09', '2017-11-05 21:33:09'),
(45, 8, 16, 12, '1957627500', '1957627500', 'Tetap', NULL, '2017-11-05 21:34:20', '2017-11-05 21:34:20'),
(46, 8, 17, 12, '3068501000', '3068501000', 'Tetap', NULL, '2017-11-05 21:35:01', '2017-11-05 21:35:01'),
(47, 8, 18, 12, '7329455675', '7262555675', 'Sisa lelang Rp.4.480.000 dan efisiensi kegiatan Rp.62.420.000', NULL, '2017-11-05 21:37:12', '2017-11-05 21:37:12'),
(48, 8, 19, 12, '1201000000', '1193500000', 'Efisiensi kegiatan Rp.7.500.000', NULL, '2017-11-05 21:38:20', '2017-11-05 21:38:20'),
(49, 8, 20, 12, '1000000000', '1111200000', 'Penambahan 111.200.000', NULL, '2017-11-05 21:39:42', '2017-11-05 21:39:42'),
(50, 8, 21, 12, '4025253000', '3922075500', 'Sisa lelang dll Rp.103.117.500', NULL, '2017-11-05 21:41:06', '2017-11-05 21:41:06'),
(51, 9, 22, 13, '1340514000', '1340514000', 'tetap', NULL, '2017-11-05 22:25:46', '2017-11-05 22:25:46'),
(52, 9, 23, 13, '2827741600', '2827741600', 'tetap', NULL, '2017-11-05 22:27:51', '2017-11-05 22:27:51'),
(53, 9, 26, 13, '200000000', '200000000', 'tetap', NULL, '2017-11-05 22:30:40', '2017-11-05 22:30:40'),
(54, 9, 22, 14, '1602161791', '1496606095', 'Efisiensi kegiatan Rp.105.555.696', NULL, '2017-11-06 04:29:34', '2017-11-06 04:29:34'),
(55, 9, 23, 14, '358210000', '349270000', 'Efisiensi Rp. 8.940.000', NULL, '2017-11-06 04:30:39', '2017-11-06 04:30:39'),
(56, 9, 26, 14, '200000000', '200000000', 'Tetap', NULL, '2017-11-06 04:31:19', '2017-11-06 04:31:19'),
(57, 9, 22, 15, '1177108000', '1177108000', 'Tetap', NULL, '2017-11-06 04:32:34', '2017-11-06 04:32:34'),
(58, 9, 23, 15, '950000000', '950000000', 'tetap', NULL, '2017-11-06 04:33:59', '2017-11-06 04:33:59'),
(59, 9, 22, 33, '1042584000', '1042584000', 'tetap', NULL, '2017-11-06 04:35:06', '2017-11-06 04:35:06'),
(60, 9, 23, 33, '857928500', '857928500', 'tetap', NULL, '2017-11-06 04:36:02', '2017-11-06 04:36:02'),
(61, 9, 26, 33, '200000000', '200000000', 'Tetap', NULL, '2017-11-06 04:36:36', '2017-11-06 04:36:36'),
(62, 9, 22, 16, '1073465000', '1073465000', 'Penyesuaian TUK', NULL, '2017-11-06 04:37:37', '2017-11-06 04:37:37'),
(63, 9, 23, 16, '267988000', '267988000', 'Tetap', NULL, '2017-11-06 04:38:13', '2017-11-06 04:38:13'),
(64, 9, 26, 16, '200000000', '200000000', 'Tetap', NULL, '2017-11-06 04:38:55', '2017-11-06 04:38:55'),
(65, 9, 39, 25, '6000000000', '6000000000', 'Tetap', NULL, '2017-11-06 04:43:12', '2017-11-06 04:43:12'),
(66, 8, 40, 31, '58332844307', '71226825707', 'Penambahan 13.000.000.000 dan efisiensi kegiatan 106.018.600', NULL, '2017-11-06 05:15:46', '2017-11-06 05:15:46'),
(67, 8, 41, 13, '1893000000', '1893000000', 'Tetap', NULL, '2017-11-06 05:18:43', '2017-11-06 05:18:43'),
(68, 8, 14, 13, '333935000', '333935000', 'Perubahan TUK', NULL, '2017-11-06 05:19:22', '2017-11-06 05:19:22'),
(69, 8, 17, 13, '370133000', '370133000', 'Tetap', NULL, '2017-11-06 05:20:03', '2017-11-06 05:20:03'),
(70, 8, 18, 13, '2533913000', '2533913000', 'Perubahan TUK', NULL, '2017-11-06 05:20:57', '2017-11-06 05:20:57'),
(71, 8, 19, 13, '1798886500', '1798886500', 'Perubahan TUK', NULL, '2017-11-06 05:21:42', '2017-11-06 05:21:42'),
(72, 8, 21, 13, '862125000', '862125000', 'Tetap', NULL, '2017-11-06 05:22:27', '2017-11-06 05:22:27'),
(73, 8, 41, 14, '1536080000', '1536560000', 'Efisiensi kegiatan Rp.9.520.000', NULL, '2017-11-06 05:23:59', '2017-11-06 05:23:59'),
(74, 8, 15, 14, '75000000', '75000000', 'Tetap', NULL, '2017-11-06 05:24:40', '2017-11-06 05:24:40'),
(75, 8, 40, 14, '315000000', '315000000', 'Tetap', NULL, '2017-11-06 05:26:05', '2017-11-06 05:26:05'),
(76, 8, 41, 15, '1056452000', '1056452000', 'Tetap', NULL, '2017-11-06 05:27:21', '2017-11-06 05:27:21'),
(77, 8, 18, 15, '1770277925', '1770277925', 'Tetap', NULL, '2017-11-06 05:28:08', '2017-11-06 05:28:08'),
(78, 8, 41, 16, '952315400', '952315400', 'Penyesuaian TUK', NULL, '2017-11-06 05:29:17', '2017-11-06 05:29:31'),
(79, 8, 19, 16, '306754200', '306754200', 'Tetap', NULL, '2017-11-06 05:30:08', '2017-11-06 05:30:08'),
(80, 8, 41, 33, '1670094800', '1670094800', 'Tetap', NULL, '2017-11-06 05:31:05', '2017-11-06 05:31:05'),
(81, 8, 18, 33, '3320909600', '3320909600', 'Tetap', NULL, '2017-11-06 05:31:53', '2017-11-06 05:31:53'),
(82, 13, 38, 35, '10000000000', '10000000000', 'tetap', NULL, '2017-11-06 05:37:13', '2017-11-06 05:37:13'),
(83, 11, 33, 37, '1000000000', '1500000000', 'Penambahan', NULL, '2017-11-06 08:23:03', '2017-11-06 08:23:03'),
(84, 11, 35, 37, '2000000000', '2000000000', 'Tetap', NULL, '2017-11-06 08:23:25', '2017-11-06 08:23:25'),
(85, 8, 19, 23, '20000000', '', '', NULL, '2017-11-06 11:00:40', '2017-11-06 11:00:40'),
(86, 13, 38, 21, '50000000', '50000000', '', NULL, '2017-11-06 11:02:05', '2017-11-06 11:02:05'),
(87, 13, 38, 27, '50000000', '50000000', '', NULL, '2017-11-06 11:02:19', '2017-11-06 11:02:19'),
(88, 13, 38, 19, '50000000', '50000000', '', NULL, '2017-11-06 11:02:48', '2017-11-06 11:02:48'),
(89, 13, 38, 18, '50000000', '50000000', '', NULL, '2017-11-06 11:03:18', '2017-11-06 11:03:18'),
(90, 13, 38, 20, '50000000', '50000000', '', NULL, '2017-11-06 11:03:30', '2017-11-06 11:03:30'),
(91, 13, 38, 24, '50000000', '50000000', '', NULL, '2017-11-06 11:03:41', '2017-11-06 11:03:41'),
(92, 8, 14, 22, '50000000', '50000000', '', NULL, '2017-11-06 11:04:35', '2017-11-06 11:04:35'),
(93, 12, 37, 29, '60000000', '60000000', '', NULL, '2017-11-06 11:11:40', '2017-11-06 11:11:40'),
(94, 12, 37, 31, '90000000', '90000000', '', NULL, '2017-11-06 11:18:31', '2017-11-06 11:18:31'),
(95, 12, 37, 28, '20000000', '20000000', '', NULL, '2017-11-06 11:21:03', '2017-11-06 11:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_program`
--

CREATE TABLE `user_program` (
  `id` int(11) NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_program`
--

INSERT INTO `user_program` (`id`, `id_program`, `id_user`, `deleted_at`, `created_at`, `updated_at`) VALUES
(21, 8, 12, NULL, '2017-11-05 20:05:14', '2017-11-05 20:05:14'),
(22, 8, 31, NULL, '2017-11-05 20:06:32', '2017-11-05 20:06:32'),
(23, 8, 13, NULL, '2017-11-05 20:06:42', '2017-11-05 20:06:42'),
(24, 8, 14, NULL, '2017-11-05 20:07:07', '2017-11-05 20:07:07'),
(25, 8, 15, NULL, '2017-11-05 20:07:22', '2017-11-05 20:07:22'),
(26, 8, 16, NULL, '2017-11-05 20:08:19', '2017-11-05 20:08:19'),
(27, 8, 33, NULL, '2017-11-05 20:10:24', '2017-11-05 20:10:24'),
(28, 13, 12, NULL, '2017-11-05 20:11:24', '2017-11-05 20:11:24'),
(29, 12, 13, NULL, '2017-11-05 20:12:57', '2017-11-05 20:12:57'),
(30, 11, 12, NULL, '2017-11-05 20:13:42', '2017-11-05 20:13:42'),
(31, 11, 13, NULL, '2017-11-05 20:14:04', '2017-11-05 20:14:04'),
(32, 11, 14, NULL, '2017-11-05 20:14:18', '2017-11-05 20:14:18'),
(33, 11, 15, NULL, '2017-11-05 20:14:58', '2017-11-05 20:14:58'),
(34, 11, 33, NULL, '2017-11-05 20:15:14', '2017-11-05 20:15:14'),
(35, 11, 16, NULL, '2017-11-05 20:15:34', '2017-11-05 20:15:34'),
(36, 11, 17, NULL, '2017-11-05 20:15:53', '2017-11-05 20:15:53'),
(37, 11, 26, NULL, '2017-11-05 20:16:16', '2017-11-05 20:16:16'),
(38, 10, 12, NULL, '2017-11-05 20:17:14', '2017-11-05 20:17:14'),
(39, 10, 13, NULL, '2017-11-05 20:17:29', '2017-11-05 20:17:29'),
(40, 10, 14, NULL, '2017-11-05 20:17:43', '2017-11-05 20:17:43'),
(41, 10, 15, NULL, '2017-11-05 20:17:52', '2017-11-05 20:17:52'),
(42, 10, 16, '2017-11-05 20:18:49', '2017-11-05 20:18:03', '2017-11-05 20:18:49'),
(43, 10, 33, NULL, '2017-11-05 20:18:13', '2017-11-05 20:18:13'),
(44, 9, 12, NULL, '2017-11-05 20:19:25', '2017-11-05 20:19:25'),
(45, 9, 13, NULL, '2017-11-05 20:19:39', '2017-11-05 20:19:39'),
(46, 9, 14, NULL, '2017-11-05 20:19:54', '2017-11-05 20:19:54'),
(47, 9, 15, NULL, '2017-11-05 20:20:07', '2017-11-05 20:20:07'),
(48, 9, 33, NULL, '2017-11-05 20:20:23', '2017-11-05 20:20:23'),
(49, 9, 16, NULL, '2017-11-05 20:20:38', '2017-11-05 20:20:38'),
(50, 9, 25, NULL, '2017-11-05 20:20:58', '2017-11-05 20:20:58'),
(51, 13, 35, NULL, '2017-11-06 05:36:12', '2017-11-06 05:36:12'),
(52, 11, 37, NULL, '2017-11-06 08:12:26', '2017-11-06 08:12:26'),
(53, 13, 24, NULL, '2017-11-06 10:50:57', '2017-11-06 10:50:57'),
(54, 13, 21, NULL, '2017-11-06 10:51:50', '2017-11-06 10:51:50'),
(55, 13, 27, NULL, '2017-11-06 10:52:05', '2017-11-06 10:52:05'),
(56, 13, 18, NULL, '2017-11-06 10:52:23', '2017-11-06 10:52:23'),
(57, 13, 20, NULL, '2017-11-06 10:52:33', '2017-11-06 10:52:33'),
(58, 8, 22, NULL, '2017-11-06 10:56:36', '2017-11-06 10:56:36'),
(59, 8, 23, NULL, '2017-11-06 10:59:18', '2017-11-06 10:59:18'),
(60, 13, 19, NULL, '2017-11-06 11:01:37', '2017-11-06 11:01:37'),
(61, 12, 29, NULL, '2017-11-06 11:11:22', '2017-11-06 11:11:22'),
(62, 12, 31, NULL, '2017-11-06 11:18:03', '2017-11-06 11:18:03'),
(63, 12, 28, NULL, '2017-11-06 11:20:43', '2017-11-06 11:20:43');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(32) NOT NULL,
  `id_article` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('1','2','3','4') NOT NULL COMMENT '1. products, 2. news, 3. Gallery, 4. general',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`),
  ADD KEY `id_controller` (`id_controller`);

--
-- Indexes for table `complains`
--
ALTER TABLE `complains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_parent` (`id_parent`);

--
-- Indexes for table `detail_tahap`
--
ALTER TABLE `detail_tahap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_kegiatan`
--
ALTER TABLE `item_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kpa_users`
--
ALTER TABLE `kpa_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laq_async_queue`
--
ALTER TABLE `laq_async_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prosentase`
--
ALTER TABLE `prosentase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tahap`
--
ALTER TABLE `tahap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_kegiatan`
--
ALTER TABLE `user_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_program`
--
ALTER TABLE `user_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `complains`
--
ALTER TABLE `complains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `controllers`
--
ALTER TABLE `controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `detail_tahap`
--
ALTER TABLE `detail_tahap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `item_kegiatan`
--
ALTER TABLE `item_kegiatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `kpa_users`
--
ALTER TABLE `kpa_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `laq_async_queue`
--
ALTER TABLE `laq_async_queue`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `prosentase`
--
ALTER TABLE `prosentase`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tahap`
--
ALTER TABLE `tahap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_kegiatan`
--
ALTER TABLE `user_kegiatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `user_program`
--
ALTER TABLE `user_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
