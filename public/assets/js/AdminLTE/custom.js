
$(function() {

    $(document).on('keyup','.autocomma', function(event) {
        // skip for arrow keys
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
            return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });

	$(document).on('click','.delete',function(){
		return confirm('Are you sure want to delete ?');
	});

    $(document).on('click','.reset',function(){
        return confirm('Are you sure want to reset ?');
    });

    // $(document).on('click','.restore',function(){
    //     return confirm('Are you sure want to restore ?');
    // });

    $(document).on('click','.actived',function(){
        return confirm('Are you sure want to active ?');
    });

    $(document).on('click','.noactived',function(){
        return confirm('Are you sure want to non active ?');
    });

    // $(document).on('click','.published',function(){
    //     return confirm('Are you sure want to publish ?');
    // });

    // $(document).on('click','.drafted',function(){
    //     return confirm('Are you sure want to draft ?');
    // });

	// $('.filter').on('click',function (e){
 //        e.preventDefault();
	//         if($(this).data('method')==='filter'){
	// 	        $('#compose-modal').modal('show');
	//         }
 //    });

    // $('.info-client').on('click',function (e){
    //     var idClient = $(this).data('idclient');
    //     $('#open-modal').load('/admin/orders/modal-client-info/'+idClient);
    // });

    // $('.edit-payment').on('click',function (e){
    //     // e.preventDefault();
    //     var id_orders = $(this).data('idorder');
    //     var id_payment = $(this).data('idpayment');
    //     $('#open-modal').load('/admin/orders/modal-edit-payment/'+id_orders+'/'+id_payment);
    // });

    // $('.add-payment').on('click',function (e){
    //     // e.preventDefault();
    //     var id_orders = $(this).data('idorder');
    //     $('#open-modal').load('/admin/orders/modal-add-payment/'+id_orders);
    // });

    // $('.detail-client').on('click',function (e){
    //     // e.preventDefault();
    //     var id_client = $(this).data('idclient');
    //     $('#open-modal').load('/admin/clients/modal-detail-client/'+id_client);
    // });
    $('.print-program-kpa').on('click',function (e){
        // e.preventDefault();
        var kpa   = $(this).data('kpa');
        var tahun = $(this).data('tahun');
        $('#open-modal').load('/admin/home/modal-print-by-user/'+kpa+'/'+tahun);
    });

    $('.detail-user').on('click',function (e){
        // e.preventDefault();
        var id_user = $(this).data('iduser');
        $('#open-modal').load('/admin/users/modal-detail-user/'+id_user);
    });

    $('.deleted-staff').on('click',function (e){
        $('#open-modal').load('/admin/users/modal-deleted-user');
    });

    $(document).on('change','.category_product',function(){
        $('.subcategory').attr('data-selected','');
    });

    $('.detail-kpa').on('click',function (e){
        // e.preventDefault();
        var id_user = $(this).data('iduser');
        $('#open-modal').load('/admin/kpa/modal-detail-kpa/'+id_user);
    });
    
    $('.dashboard-kegiatan').on('click',function (e){
        var id_kegiatan = $(this).data('idkegiatan');
        $('#open-modal').load('/admin/home/modal-dashboard-kegiatan/'+id_kegiatan);
    });
    
    $('.detail-total-kpa').on('click',function (e){
        var id_program = $(this).data('idprogram');
        $('#open-modal').load('/admin/home/modal-detail-jumlah-kpa/'+id_program);
    });

    $('.tambah-sub').on('click',function (e){
        waiting();
        var kegiatan = $(this).data('kegiatan');
        $('#open-modal').load('/panel/program/modal-add-sub/'+kegiatan);
    });
    
    $('.edit-item').on('click',function (e){
        waiting();
        var sub = $(this).data('sub');
        $('#open-modal').load('/panel/program/modal-edit-sub/'+sub);
    });

    $('.detail-kpa-program').on('click',function (e){
        // e.preventDefault();
        var id_user = $(this).data('iduser');
        $('#open-modal').load('/admin/program/modal-detail-kpa/'+id_user);
    });

    $('.choose-user').on('click',function (e){
        $('#open-modal').load('/admin/program/modal-detail-user');
    });

    $('.tambah-kpa').on('click',function (e){
        waiting();
        var idProgram = $(this).data('program');
        $('#open-modal').load('/admin/program/modal-add-user/'+idProgram);
    });
    $('.sync').on('click',function (e){
        waiting();
        var idProgram = $(this).data('program');
        $('#open-modal').load('/admin/program/modal-sync-user/'+idProgram);
    });

    $('.edit-pagu-kegiatan').on('click',function (e){
        waiting();
        var id      = $(this).data('id');
        var program = $(this).data('program');
        var kpa     = $(this).data('kpa');
        $('#open-modal').load('/admin/program/modal-sync-edit-kegiatan/'+id+'/'+program+'/'+kpa);
    });
    
    $('.add-kegiatan').on('click',function (e){
        waiting();
        var idProgram = $(this).data('program');
        $('#open-modal').load('/admin/program/modal-add-kegiatan/'+idProgram);
    });

    $('.add-kegiatan-kpa').on('click',function (e){
        waiting();
        var program = $(this).data('program');
        var kpa     = $(this).data('kpa');
        $('#open-modal').load('/admin/program/modal-add-kegiatan-kpa/'+program+'/'+kpa);
    });

    $('.print-program').on('click',function (e){
        waiting();
        var program = $(this).data('program');
        $('#open-modal').load('/panel/program/modal-print/'+program);
    });
    
    $('.import-program').on('click',function (e){
        waiting();
        $('#open-modal').load('/admin/program/modal-import-program');
    });

    $(document).on('click','.waiting',function(){
        $('.waiting').addClass('disabled');
    });
    $(document).on('click','.close-waiting,.close-button',function(){
        $('.waiting').removeClass('disabled');
    });
    $(document).on('click','.please-waiting',function(){
        waiting();
    });
});

function waiting(){
    $.blockUI({ 
        message: '<h1 style="font-weight:300">Please Wait</h1>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#fff', 
            '-webkit-border-radius': '0', 
            '-moz-border-radius': '0',  
            color: '#000',
            'text-transform': 'uppercase'
        },
        baseZ:9999
    });
}